timedilator-2
=============

Minimum-overhead TUI tracing/frame profiler for low-latency application development.

Views data recorded by the [nanoprof](https://codeberg.org/Kiith/nanoprof) instrumentation library.

**WARNING**: this project is in a very early stage, there will be breaking changes in both the UI
and the underlying profiling data format (as well as the `nanoprof` library).

![](https://codeberg.org/Kiith/timedilator-2/raw/branch/master/timedilator-2.gif)

About
-----

Timedilator-2 is similar to tracing (aka frame) profilers used in game development such as
[Optick](https://github.com/bombomby/optick) or [RAD
Telemetry](http://www.radgametools.com/telemetry.htm), but it was written for a specific work
environment, developing low-latency network applications. These often run on slow harware with no
display output and only an SSH connection to work with. This resulted in the following requirements:

1. User interface must work without graphical output, in terminal to allow profiling 
   directly on a device in real-time over an SSH connection.
2. Tracing profilers generate a lot of data each second, which has to either be recorded or sent
   somewhere (perhaps over the very network we're developing for). Profiling data should be as
   minimal, as 'compressed' as possible.
3. Latencies relevant for these applications is even shorter than in games - while a frame may take
   16 *milliseconds* to render, we may have a *microsecond* or less to process to process a packet.
   Profiling overhead should be as minimal as possible, even if it makes it less convenient to add
   instrumentation.

The resulting profiler is useful for very-low-latency applications, and should also be decent for
game development, but if you don't like TUI you may want to look at
[alternatives](https://codeberg.org/Kiith/nanoprof#similar-projects).

Features
--------

* Nanosecond precision (depending on platform CPU clock precision)
* Manual instrumentation using the [nanoprof](https://codeberg.org/Kiith/nanoprof) library
* Low CPU overhead
* Visual profiling with a TUI - we use Unicode glyphs and RGB colors to make usage this as
  'visual' as possible
* Help information directly on the screen at all times
* Statistics about profiled code scopes such as duration percentiles, frequencies, etc.
* *Should* work on any terminal with RGB color support - only tested on Linux and OSX so far
* Profile multiple threads simultaneously
* Can open profiled data (.nanoprof files) in real time as they are being written, or finished 
  after measurement.

Getting started
---------------

### C/C++

See nanoprof's [getting started](https://codeberg.org/Kiith/nanoprof#getting-started) tutorial.

TODO: support for more languages

License 
-------

Licensed under the [MIT](http://opensource.org/licenses/MIT) license, see file `LICENSE`.
