use std::cmp;
use tui::style::Color;

/// Common color constants.
pub const BLACK:  Color = Color::Rgb(0,0,0);
pub const RED:    Color = Color::Rgb(255,0,0);
pub const WHITE:  Color = Color::Rgb(255,255,255);
pub const YELLOW: Color = Color::Rgb(255,255,0);

/// Get a gray color of specified intensity.
pub const fn gray(level: u8) -> Color {
    Color::Rgb(level, level, level)
}

/// Wrapper around Color for sorting.
#[derive(Copy, Clone)]
pub struct Wrapper(pub Color);

/// Ordering for RGB colors, for sorting.
impl Ord for Wrapper {
    fn cmp(&self, other: &Self) -> cmp::Ordering {
        if let Color::Rgb(r1,g1,b1) = self.0 {
            if let Color::Rgb(r2,g2,b2) = other.0 {
                (r1,g1,b1).cmp(&(r2,g2,b2))
            } else {
                unreachable!("TODO implement Ord for non-RGB colors");
            }
        } else {
            unreachable!("TODO implement Ord for non-RGB colors");
        }
    }
}

/// Ordering for RGB colors, for sorting.
impl PartialOrd for Wrapper {
    fn partial_cmp(&self, other: &Self) -> Option<cmp::Ordering> {
        Some(self.cmp(other))
    }
}

/// Equality for `Wrapper`, for sorting.
impl Eq for Wrapper { }

/// Equality for `ColorWrapper`, for sorting.
impl PartialEq for Wrapper {
    fn eq(&self, other: &Self) -> bool {
        if let Color::Rgb(r1,g1,b1) = self.0 {
            if let Color::Rgb(r2,g2,b2) = other.0 {
                (r1,g1,b1) == (r2,g2,b2)
            } else {
                unreachable!("TODO implement Eq for non-RGB colors");
            }
        } else {
            unreachable!("TODO implement Eq for non-RGB colors");
        }
    }
}

/// Generate colors to use for scopes. Must provide good enough contrast between any two colors.
pub fn generate_scope_colors() -> Vec<Color> {
    let mut result = vec![];
    // 'reds'
    for g in 0 .. 4 {
        for b in 0 .. 4 {
            if g == b {
                continue;
            }
            result.push(Color::Rgb(255, g * 63, b * 63));
        }
    }
    // 'greens'
    for r in 0 .. 4 {
        for b in 0 .. 4 {
            if r == b {
                continue;
            }
            result.push(Color::Rgb(r * 63, 255, b * 63));
        }
    }
    // 'blues'
    for r in 0 .. 4 {
        for g in 0 .. 4 {
            if r == g {
                continue;
            }
            result.push(Color::Rgb(r * 63, g * 63, 255));
        }
    }
    result
}

