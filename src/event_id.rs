use std::convert::TryFrom;

/// Nest level of a scope/track.
#[derive(Copy, Clone, PartialEq, Eq)]
pub struct NestLevel(pub i8);

impl NestLevel {
    /// Minimum supported nest level - this is actually used for internal profiling, MIN+1 is the 'shallowest' level.
    pub const MIN: Self = Self(i8::MIN);
    /// Maximum, 'deepest' supported nest level.
    pub const MAX: Self = Self(i8::MAX);
    /// Nest level used for internal profiling. Is ignored for purposes of nesting 'depth'.
    pub const INTERNAL_PROFILING: Self = Self::MIN;
}

/// Base integer type used to implement timepoint/data event IDs.
pub type IDBase = u32;

/// Timepoint event ID.
///
/// While nanoprof uses 16bit values (at least at the moment), we use 32bit so we can have special
/// TUI-only timepoint ID's, e.g. for scopes created by merging multiple smaller scopes.
#[derive(Copy, Clone, PartialEq, Eq)]
pub struct TimepointID(pub IDBase);

impl TimepointID {
    /// Maximum timepoint ID that can be found in a nanoprof stream.
    pub const MAX:          Self = Self(0x3FFF);
    /// Maximum timepoint ID that can be defined by the nanoprof user.
    pub const MAX_USER:     Self = Self(0x3BFF);
    /// Timepoint IDs format in nanoprof streams uses 14bit values - this includes internal timepoints.
    pub const MAX_INTERNAL: Self = Self(0x3FFF);

    // Internal 14-bit value ranges

    /// Minimum timepoint ID that is used exclusively to end, not start, a scope (at nest level -128).
    pub const MIN_END:      Self = Self(Self::MAX_USER.0 + 1);
    /// Maximum timepoint ID that is used exclusively to end, not start, a scope (at nest level 127).
    pub const MAX_END:      Self = Self(Self::MIN_END.0 + 255);
    /// Maximum possible gap timepoint ID, when timepoint's value is in 2^48*hectopicoseconds.
    pub const GAP_48:       Self = Self(Self::MAX_INTERNAL.0 - 1);
    // Using last 49 values lower than MAX_INTERNAL for gaps, makes it easier to compress with
    // bitpacker and some transformations.
    /// Minimum gap timepoint ID, when timepoint's value is in hectopicoseconds.
    pub const GAP_0:        Self = Self(Self::GAP_48.0 - 48);

    // Internal timedilator-only 32-bit value ranges

    /// Maximum timepoint ID used by merged scopes (see `is_merged()`)
    pub const MERGED_MAX:   Self = Self(IDBase::MAX - 1);
    /// Minimum timepoint ID used by merged scopes (see `is_merged()`)
    pub const MERGED_MIN:   Self = Self(Self::MERGED_MAX.0 - 65535);
    /// Create a timepoint ID representing a gap timepoint with time value as a product of `2^^shift_level`.
    ///
    /// Gap timepoints are purely for internal use, they don't exist in nanoprof stream.
    pub const fn gap(shift_level: u8) -> Self {
        Self(Self::GAP_0.0 + shift_level as IDBase)
    }

    /// Check if this timepoint ID is a gap timepoint ID.
    pub const fn is_gap(self) -> bool {
        self.0 >= Self::GAP_0.0 && self.0 <= Self::GAP_48.0
    }

    /// Get `shift_level` of a gap timepoint. Can only be called on a gap timepoint ID.
    pub fn gap_level(self) -> u64 {
        assert!(self.is_gap(), "only applicable to gap timepoints");
        u64::from(self.0 - Self::GAP_0.0)
    }

    /// Create a timepoint ID for a merged scope with specified coverage (65535:full, 0:empty).
    pub const fn merged(coverage: u16) -> Self {
        Self( Self::MERGED_MIN.0 + coverage as IDBase )
    }

    /// Returns true if this timepoint ID is that of a merged scope.
    ///
    /// Merged scopes are created by `Stream::track_scope_view` when there are no scopes with
    /// durations long enough to be visibly rendered, but there are still many short-duration
    /// scopes and we want to render these somehow. So, we 'merge' these short scopes, even
    /// including gaps between them, and calculate a 'coverage' value specifying how much of the
    /// resulting merged scope is covered by actual scopes, and how much is taken by gaps. This
    /// coverage is a 16-bit value, with 0 representing all gaps, and 65535 representing all scopes.
    ///
    /// Coverage is encoded in the timepoint ID, that's why there are 65536 merged timepoint IDs.
    pub const fn is_merged(self) -> bool {
        self.0 >= Self::MERGED_MIN.0 && self.0 <= Self::MERGED_MAX.0
    }

    /// Returns true if this timepoint ID is that of an internal (nanoprof self-profiling) event.
    pub const fn is_internal(self) -> bool {
        self.0 > Self::MAX_USER.0 && self.0 <= Self::MAX_INTERNAL.0
    }

    /// Returns true if this is an 'end' timepoint ID, i.e. ID of an event that ends previous scope
    /// at the same nest level, but does not start any new scope.
    pub const fn is_end(self) -> bool {
        self.0 >= Self::MIN_END.0 && self.0 <= Self::MAX_END.0
    }

    /// Construct and 'end' timepoint to end a scope on specified nest level.
    pub fn end(level: NestLevel) -> Self {
        Self(Self::MIN_END.0 + IDBase::try_from(i32::from(level.0) - (i32::from(i8::MIN))).unwrap())
    }
}


/// Data event ID.
///
/// While nanoprof uses 16bit values (at least at the moment), we use 32bit so we can have special
/// TUI-only data ID's.
#[derive(Copy, Clone, PartialEq, Eq)]
pub struct DataID(pub IDBase);

#[allow(dead_code)]
impl DataID {
    /// Maximum data event ID that can be found in a nanoprof stream.
    pub const MAX:          Self = Self(0x3FFF);
    /// Maximum data event ID that can be defined by the nanoprof user.
    pub const MAX_USER:     Self = Self(0x3BFF);
    /// Keeping 14-bit IDs to be consistent with timepoints - may change in future.
    pub const MAX_INTERNAL: Self = Self(0x3FFF);
}


