use std::cmp;
use std::convert::{TryFrom, TryInto};
use std::fmt;
use std::ops;
use std::mem;

use crate::stream_reader::{
    DataEvent
};
use crate::event_id::{
    NestLevel,
    TimepointID,
};
use crate::integer_storage::{
    U16ArrayStats,
    U16Array,
    TimepointIDs,
    TimepointDeltas
};
use crate::stat_utils::{
    bin,
    dec,
};

/// Data event stored in a Track, after specified timepoint and at specified time.
#[derive(Clone)]
pub struct TrackDataEvent {
    /// Index of the last timepoint in the track with time before/equal to this data event.
    _timepoint_index: usize,
    /// Time the data event was measured at (time of the last timepoint in *any* track before this data event).
    pub time:        StreamTime,
    /// Data event itself.
    pub data:        DataEvent,
}

#[derive(Copy, Clone, PartialEq, Eq, Debug)]
/// Time/duration in hectopicoseconds.
///
/// max 64bit value corresponds to about 58.5 years.
pub struct StreamTime{
    hpsecs: u64
}

impl Ord for StreamTime {
    fn cmp(&self, other: &Self) -> cmp::Ordering {
        self.hpsecs.cmp(&other.hpsecs)
    }
}
impl PartialOrd for StreamTime {
    fn partial_cmp(&self, other: &Self) -> Option<cmp::Ordering> {
        Some(self.hpsecs.cmp(&other.hpsecs))
    }
}

impl ops::Add for StreamTime {
    type Output = Self;

    fn add(self, rhs: Self) -> Self {
        Self{hpsecs: self.hpsecs + rhs.hpsecs}
    }
}

impl ops::Sub for StreamTime {
    type Output = Self;

    fn sub(self, rhs: Self) -> Self {
        Self{hpsecs: self.hpsecs - rhs.hpsecs}
    }
}

impl ops::SubAssign for StreamTime {
    fn sub_assign(&mut self, rhs: Self) {
        self.hpsecs -= rhs.hpsecs;
    }
}

impl ops::AddAssign for StreamTime {
    fn add_assign(&mut self, rhs: Self) {
        self.hpsecs += rhs.hpsecs;
    }
}

impl ops::Mul<u64> for StreamTime {
    type Output = Self;

    fn mul(self, rhs: u64) -> Self {
        assert!(self.hpsecs == 0 || (u64::MAX / self.hpsecs > rhs), "{} * {}", self.hpsecs, rhs);
        Self{hpsecs: self.hpsecs * rhs}
    }
}

/// Dividing `StreamTime` by integer gives a `StreamTime`.
impl ops::Div<u64> for StreamTime {
    type Output = Self;

    fn div(self, rhs: u64) -> Self {
        Self{hpsecs: self.hpsecs / rhs}
    }
}

/// Dividing `StreamTime` by `StreamTime` gives an integer (how much larger A is compared to B).
impl ops::Div<Self> for StreamTime {
    type Output = u64;

    fn div(self, rhs: Self) -> Self::Output {
        self.hpsecs / rhs.hpsecs
    }
}

impl StreamTime {
    /// Infinitely distant future.
    pub const INF: Self = Self{hpsecs: u64::MAX};
    pub const MIN: Self = Self{hpsecs: 0};

    /// Construct from a time value in nanoseconds.
    pub const fn from_nanoseconds(nanoseconds: u64) -> Self {
        Self{hpsecs: nanoseconds * 10}
    }

    /// Construct from a time value in picoseconds.
    pub const fn from_picoseconds(picoseconds: u64) -> Self {
        Self{hpsecs: (picoseconds / 100)}
    }

    /// Construct from a time value in hectopicoseconds.
    pub const fn from_hpsecs(hectopicoseconds: u64) -> Self {
        Self{hpsecs: hectopicoseconds}
    }

    /// Construct from a raw `StreamTime` time value (used e.g. with stats/histograms).
    pub const fn from_raw(raw: u64) -> Self {
        Self::from_hpsecs(raw)
    }

    /// True if the value is infinite.
    pub const fn is_inf(self) -> bool {
        self.hpsecs == u64::MAX
    }

    /// Get time value in picoseconds.
    pub const fn as_picoseconds(self) -> u128 {
        self.hpsecs as u128 * 100
    }

    /// Get time value in nanoseconds.
    pub const fn as_nanoseconds(self) -> u64 {
        self.hpsecs / 10
    }

    /// Get raw internal time value.
    pub const fn raw(self) -> u64 {
        self.hpsecs
    }

    /// True if this is a zero time value.
    pub const fn is_zero(self) -> bool {
        self.hpsecs == 0
    }
}

/// ID of a nanoprof stream.
#[derive(Copy, Clone)]
pub struct StreamID(pub u16);

/// A time interval between two timepoints, also called 'zone' in many similar tools.
///
/// The scope is identified (and therefore named) by ID of the timepoint that started it. Its nest
/// level is also determined by this ID.
#[derive(Copy, Clone, PartialEq, Eq)]
pub struct Scope {
    /// Start of the time interval.
    pub start:            StreamTime,
    /// Duraton of the time interval.
    pub duration:         StreamTime,
    /// ID of the timepoint starting this scope.
    pub id:               TimepointID,
    /// Index of the first, if any, data event in this scope, in `data_buffer` filled by `Stream::track_scope_view`
    pub data_event_first: usize,
    /// Number of data events in this scope, immediately following `data_event_first`. Can be 0.
    pub data_event_count: usize
}

impl Scope {
    /// Get the end of the time interval represented by this scope.
    pub fn end(&self) -> StreamTime {
        if !self.duration.is_inf() {
            self.start + self.duration
        } else {
            StreamTime::INF
        }
    }
}

/// Get the duration and covered area of a merged scope that would be created by merging 
/// `(start,duration,covered_duration)` into `merged`, with a possible gap in between.
fn merged_coverage_and_duration(merged: MergedScope, start: StreamTime, duration: StreamTime, covered_duration: StreamTime) -> (StreamTime, StreamTime) {
    let merged_end = merged.start + merged.duration;
    assert!(start >= merged_end, "scopes should be added in increasing monotone fashion");
    // Gap between end of merged scope and this new scope/block
    let gap            = start - merged_end;
    let total_duration = merged.duration + duration + gap;
    let total_covered  = merged.covered + covered_duration;
    (total_duration, total_covered)
}

// Memory usage history WRT track storage:
//
// Scopes:            100.0% (893M)
// 4+4B timepoints:    81.2%
// 4+2B timepoints:    64.4%
// 2+2B timepoints:    46.1% (412M)
// 14+10b timepoints: ~62.0% 

/// Time delta storage type used in timepoints stored in a track.
///
/// The value is *usually* in hectopicoseconds, but this may change for special timepoint IDs
/// (currently used for gap timepoint IDs which specify the number of bits to left-shift the value
/// to get time in hectopicoseconds)
#[derive(Copy, Clone)]
struct TimepointDelta(u16);
impl TimepointDelta {
    /// Number of bits used by a `TimepointDelta` value.
    const BITS:      u8          = 16;
    /// Mask to mask out the bits we can store in a `TimepointDelta`, e.g. a `StreamTime`.
    const BITS_MASK: u64            = 0xFFFF;

    /// Construct from given `StreamTime` value.
    fn from_streamtime(time: StreamTime) -> Self {
        Self(time.hpsecs.try_into().unwrap())
    }

    /// Get time delta represented by a gap timepoint (these are used to extend time delta when a
    /// single timepoint cannot represent full delta value).
    fn gap_time_delta(self, id: TimepointID) -> StreamTime {
        StreamTime{hpsecs: u64::from(self.0) << id.gap_level()}
    }
}

/// 16-bit `TimepointID` used in timepoints stored in a track.
///
/// Can represent all nanoprof timepoint IDs and gap timepoint IDs, but not display-specific IDs
/// like merged timepoint IDs.
#[derive(Clone, Copy)]
struct TimepointID16(u16);
impl TimepointID16 {
    /// Convert to a proper `TimepointID`.
    const fn to_timepoint_id(self) -> TimepointID {
        TimepointID(self.0 as u32)
    }
}


/// Information about timepoints in a 'block' of e.g. 128 consecutive timepoints.
///
/// Also used for metablocks - blocks of blocks.
///
/// `Stream::BLOCK_SIZE_EXP` determines how many timepoints are in a block, and how many blocks in a
/// metablock.
#[derive(Clone, Copy)]
struct TimepointBlock {
    /// Start time (before delta) of the first timepoint in the block.
    start_time:        StreamTime,
    /// Total area of the block not covered by scopes - used for fast merged scopes generation.
    total_not_covered: StreamTime,
    /// ID of the last timepont in the block. Used to determine if we can skip a block with no
    /// extra processing (if the last block is a time delta, we'd need to iterate backward to find
    /// the 'true' last timepoint to determine the scope starting before the end of the block).
    ///
    /// Used as an optimization to avoid cache thrashing (we could get this value directly from
    /// track.timepoint_ids).
    end_timepoint_id: TimepointID,
}

/// Debug stats collected about a Track.
pub struct TrackStats {
    /// Stats about timepoint delta storag.
    timepoint_deltas:             U16ArrayStats,
    /// Stats about timepoint ID storag.
    timepoint_ids:                U16ArrayStats,
    /// Memory reserved for timepoint blocks.
    memory_blocks_reserved:       usize,
    /// Memory used by timepoint blocks.
    memory_blocks_allocated:      usize,
    /// Memory reserved for data events.
    memory_data_events_reserved:  usize,
    /// Memory used by data events.
    memory_data_events_allocated: usize,
    /// Number of 'gap' timepoints - overhead of our 16bit timepoint delta implementation.
    gap_timepoints:               usize,
}

impl TrackStats {
    const fn new() -> Self {
        Self {
            timepoint_deltas:             U16ArrayStats::new(),
            timepoint_ids:                U16ArrayStats::new(),
            memory_blocks_reserved:       0,
            memory_blocks_allocated:      0,
            memory_data_events_reserved:  0,
            memory_data_events_allocated: 0,
            gap_timepoints:               0,
        }
    }

    /// Get total size of memory allocated by this track in bytes.
    pub const fn total_memory_used(&self) -> usize {
        self.timepoint_deltas.memory_total_used +
        self.timepoint_ids.memory_total_used +
        self.memory_blocks_allocated +
        self.memory_data_events_allocated
    }
}
impl fmt::Display for TrackStats {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "timepoint deltas:\n\
                   {}\n\
                   timepoint IDs:\n\
                   {}\n\
                   \n\
                   tp-blocks:   used {} reserved {}\n\
                   data events: used {} reserved {}\n\
                   gap timepoints: {}",
                   self.timepoint_deltas,
                   self.timepoint_ids,
                   bin(self.memory_blocks_allocated),      bin(self.memory_blocks_reserved),
                   bin(self.memory_data_events_allocated), bin(self.memory_data_events_reserved),
                   dec(self.gap_timepoints))
    }
}

/// A single 'track' of the stream - stores all timepoints on a single nest level.
#[derive(Clone)]
struct Track {
    /// Timepoint deltas from oldest to newest.
    ///
    /// There is one delta here per ID in `timepoint_ids`.
    timepoint_deltas: TimepointDeltas,
    /// Timepoint IDs from oldest to newest.
    ///
    /// This includes any scope-end added for parent tracks so we don't need to look at parent
    /// timepoints to determine where to end a timepoint on the current-track.
    timepoint_ids:    TimepointIDs,

    /// Data events recorded in scopes formed by timepoints on this track.
    data_events: Vec<TrackDataEvent>,
    /// Information about blocks of consecutive timepoints in `timepoint_ids`/`timepoint_deltas`
    ///
    /// Each 'level' consists of fewer, larger blocks - in `block_levels[0]`, each block has
    /// `2^^BASE_BLOCK_SIZE_EXP` timepoints, in `block_levels[n]` where `n>=1`, each block has the
    /// same number of timepoints as `2^^(META_BLOCK_SIZE_EXP*n)` of the base level blocks.
    /// Each block on a higher level encompasses `2^^META_BLOCK_SIZE_EXP` blocks on the level below.
    ///
    /// Allows to skip a whole block worth of timepoints when getting scopes to view, which
    /// improves zoomed out display performance.
    block_levels: [Vec<TimepointBlock>; Self::BLOCK_LEVELS],
    /// Time of the last timepoint in `timepoint_deltas`. Used to determine time delta when we add the
    /// next timepoint (as those are added with their time value, rather than delta).
    last_time:    StreamTime,
    /// ID of the last timepoint in `timepoint_ids`, for fast access.
    last_id: TimepointID,
    /// Counters of gaps with every possible `shift_level` in the track. Used for debugging.
    gap_counters: [u64; 49],

    /// Index of the first timepoint the next `collect_stats()` call should process.
    stats_next_timepoint: usize,
    /// ID of the last timepoint processed by a `collect_stats()` call.
    stats_prev_id:        TimepointID,
    /// If the last timepoint processed by `collect_stats()` was a gap, this is cumulative sum of
    /// that gap and any contiguous preceding gap timepoints.
    stats_total_gap:      StreamTime,
    /// Cumulative sum of all timepoint deltas processed by `collect_stats()` calls so far.
    ///
    /// Note that gaps are only added to this when the following timepoint (the delta of which is
    /// extended by the gap/s) is processed.
    stats_current_time:   StreamTime
}
impl Default for Track {
    /// Get a default-initialized Track with no timepoint.
    fn default() -> Self {
        Self {
            timepoint_deltas: TimepointDeltas::new(false),
            timepoint_ids:    TimepointIDs::new(true),
            data_events:      vec![],
            block_levels:     Default::default(),
            last_time:        StreamTime{hpsecs: 0},
            last_id:          TimepointID::end(NestLevel(0)),
            gap_counters:     [0; 49],

            stats_next_timepoint: 0,
            stats_prev_id:        TimepointID::end(NestLevel(0)),
            stats_total_gap:      StreamTime{hpsecs: 0},
            stats_current_time:   StreamTime{hpsecs: 0}
        }
    }
}
impl Track {
    /// Number of nesting levels of timepoint blocks in the track.
    ///
    /// Note: currently this cannot be safely changed without also rewriting `Stream::track_scope_view`
    /// - that code directly depends on this value. It could be rewritten in recursive style to
    /// support any `BLOCK_LEVELS` value.
    const BLOCK_LEVELS: usize = 3;

    /// Get total number of gap timepoints, for debugging.
    fn gap_count(&self) -> u64 {
        self.gap_counters.iter().sum()
    }

    /// Get index of the last block at specified level that starts *before* `start_time`.
    fn start_block_index(&self, block_level: usize, start_time: StreamTime) -> usize {
        let lower_bound_index = match self.block_levels[block_level].binary_search_by_key(&start_time, |s| {s.start_time}) {
            // Ok if same start time found, Err if not found - either way, return the index.
            Ok(i) | Err(i)  => i,
        };
        if lower_bound_index == 0 {
            0
        } else {
            lower_bound_index - 1
        }
    }

    /// Get the min,max bounds of the subrange of data events with times between `start_time` and `end_time`.
    /// 
    /// The max bound is *exclusive* - the first index not included in the bounds. If the last data
    /// event is exactly at `end_time`, it is not included in the bounds to avoid it being included
    /// in two scopes bounds - at the end of the first and the beginning of the second.
    fn data_event_bounds(&self, start_time: StreamTime, end_time: StreamTime) -> (usize, usize) {
        let lower_bound_index = self.data_events.partition_point(|d| d.time < start_time);
        let upper_bound_index = self.data_events.partition_point(|d| d.time < end_time);
        // Since data events are 'points' in time, not spans (unlike blocks/scopes), there's no need 
        // to add one more preceding/succeeding item.
        (lower_bound_index, upper_bound_index)
    }

    /// Find the nearest non-gap timepoint before index `start_idx` and return its ID, if any.
    fn find_previous_nongap_timepoint(&mut self, start_idx: usize) -> Option<TimepointID> {
        for i in 1..=start_idx {
            let candidate = self.timepoint_id_at(start_idx - i).to_timepoint_id();
            if !candidate.is_gap() {
                return Some(candidate)
            }
        }
        None
    }

    /// True if there are no timepoints in the track.
    fn is_empty(&self) -> bool {
        self.timepoint_deltas.is_empty()
    }

    /// Get the number of timepoints in the track.
    fn timepoint_count(&self) -> usize {
        self.timepoint_deltas.len()
    }

    /// Get timepoint index bounds (inclusive, exclusive) of the L0 (base) timepoint block with specified index.
    fn base_block_bounds(&self, block_idx: usize) -> (usize, usize) {
        (block_idx << Stream::BASE_BLOCK_SIZE_EXP,
         cmp::min((block_idx + 1) << Stream::BASE_BLOCK_SIZE_EXP, self.timepoint_count()))
    }

    /// Add a timepoint with specified ID and delta to the track.
    fn add_timepoint(&mut self, id: TimepointID, delta: TimepointDelta) {
        self.timepoint_deltas.push(delta.0);
        self.timepoint_ids.push(id.0.try_into().unwrap());
    }

    fn timepoint_id_at(&mut self, index: usize) -> TimepointID16 {
        TimepointID16(self.timepoint_ids.at(index))
    }

    fn timepoint_id_last(&mut self) -> Option<TimepointID16> {
        self.timepoint_ids.last().map(TimepointID16)
    }

    fn timepoint_delta_at(&mut self, index: usize) -> TimepointDelta {
        TimepointDelta(self.timepoint_deltas.at(index))
    }

    /// Add stats about this track to aggregate stats about all tracks.
    fn add_stats(&mut self, stats: &mut TrackStats) {
        self.timepoint_deltas.add_stats(&mut stats.timepoint_deltas);
        self.timepoint_ids.add_stats(&mut stats.timepoint_ids);
        stats.memory_blocks_reserved       += self.block_levels.iter().map(Vec::capacity).sum::<usize>() * mem::size_of::<TimepointBlock>();
        stats.memory_blocks_allocated      += self.block_levels.iter().map(Vec::len).sum::<usize>() * mem::size_of::<TimepointBlock>();
        stats.memory_data_events_reserved  += self.data_events.capacity() * mem::size_of::<TrackDataEvent>();
        stats.memory_data_events_allocated += self.data_events.capacity() * mem::size_of::<TrackDataEvent>();
        for i in 0..self.timepoint_count() {
            stats.gap_timepoints += if self.timepoint_id_at(i).to_timepoint_id().is_gap() { 1 } else { 0 }
        }
    }
}

#[derive(Clone, Copy)]
/// Statistics collected about the `track_scope_view` function and possibly other stream viewing related code.
pub struct StreamViewStats {
    /// Number of `track_scope_view calls`.
    scope_view_calls:     u64,
    /// Number of that were skipped without processing blocks within, for each block level.
    blocks_skipped:       [u64; Track::BLOCK_LEVELS],
    /// Number of blocks that were fully processed, for each block level.
    blocks_processed:     [u64; Track::BLOCK_LEVELS],
    /// Number of timepoints that were processed.
    timepoints_processed: u64,
}
impl StreamViewStats {
    pub const fn new() -> Self {
        Self {
            scope_view_calls:     0,
            blocks_skipped:       [0; Track::BLOCK_LEVELS],
            blocks_processed:     [0; Track::BLOCK_LEVELS],
            timepoints_processed: 0,
        }
    }

    /// Reset the stat counters.
    fn clear(&mut self) {
        *self = Self::new();
    }
}
impl fmt::Display for StreamViewStats {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{},{}/{},{}/{},{}/{},{}", 
               self.scope_view_calls,
               self.blocks_skipped[2], self.blocks_processed[2],
               self.blocks_skipped[1], self.blocks_processed[1],
               self.blocks_skipped[0], self.blocks_processed[0],
               self.timepoints_processed)
    }
}

/// Params for a block skip, on where to skip.
///
/// Used in `Stream::track_scope_view`
///
/// We defer a skip if a block ends when expecting a timepoint to end current scope.
#[derive(Clone, Copy)]
struct SkipParams {
    /// Value to change `ScopeViewBuilderState::prev_id` to .
    prev_id:        TimepointID,
    /// Value to change `ScopeViewBuilderState::current_time` to.
    current_time:   StreamTime,
    /// Level of the block to skip (0 for smallest blocks, 1 for metablocks of those smallest blocks, etc.)
    block_level: usize,
    /// Sum of parts of the block that are not covered by scopes.
    not_covered:    StreamTime
}

/// Data about an under-construction merged scope. We're summing total duration to avoid imprecision of repeated divisions.
#[derive(Clone, Copy)]
struct MergedScope {
    /// Start of the merged scope.
    start:    StreamTime,
    /// Sum of duratons of all scopes being merged.
    covered:  StreamTime,
    /// Total duration of the merged scope including gaps between the scopes being merged.
    duration: StreamTime,
}

/// State of `Stream::track_scope_view`, tracking current time, scope state, etc.
struct ScopeViewBuilderState<'a> {
    /// ID/idx of the previous non-gap timepoint (can be an 'end' timepoint).
    prev_id:           TimepointID,
    /// Total gap up to current timepoint is accumulated here, to add to the timepoint's delta.
    total_gap:         StreamTime,
    /// Start time of the current timepoint.
    current_time:      StreamTime,
    /// Reference to the buffer we're adding scopes into.
    buffer:            &'a mut Vec<Scope>,
    /// Start time (left bound) of the scope view.
    start_time:        StreamTime,
    /// End time (right bound) of the scope view.
    end_time:          StreamTime,
    /// Resolution - smallest possible visible scope duration.
    time_resolution:   StreamTime,
    /// Skip operation to apply right when we finish the current scope (or even immediately if no scope under construction).
    pending_skip:      Option<SkipParams>,
    /// Merged scope currently under construction, if any.
    merged_scope:      Option<MergedScope>,
    /// For each block level, a flag true if we skipped the previous block.
    ///
    /// Used to avoid deferring skip of the next block even if the previous one does not end with
    /// an 'end' timepoint.
    previous_skipped:  [bool; Track::BLOCK_LEVELS],
}
impl ScopeViewBuilderState<'_> {
    /// If coverage of a merged scope would be lower than this after merging more scopes, cut off 
    /// and emit that scope at its current coverage instead.
    const COVERAGE_CUTOFF_THRESHOLD: u64 = 2048;
    /// Avoid multiplication overflow
    const MAX_SAFE_MERGED_DURATION: StreamTime = StreamTime{hpsecs: u64::MAX / Self::COVERAGE_CUTOFF_THRESHOLD };

    /// Try to skip current block at specified block level.
    ///
    /// * `track`:       Track we're viewing.
    /// * `block_idx`:   Index of the current block within its level.
    /// * `block_level`: Block level we're trying to skip a block on
    ///
    /// Returns true if we can immediately skip the block, false if a skip is impossible or if it's
    /// possible but deferredd (in this case `pending_skip` is set).
    fn try_skip_block(&mut self, track: &mut Track, block_idx: usize, block_level: usize) -> bool {
        let previous_skipped = self.previous_skipped[block_level];
        self.previous_skipped[block_level] = false;
        if !self.can_skip_block(track, block_idx, block_level) {
            return false;
        } 

        // If we skipped the previous block on this level, no need to defer the next skip (such
        // deferral is done if previous block ends with a scope-starting timepoint so we can finish
        // that scope - but if both previous and current block are small enough to be skipped, we
        // don't need to finish anything and can skip both blocks).
        if previous_skipped {
            assert!(self.apply_and_clear_skip() == Some(block_level));
            return true;
        }

        // If previous id is end, we can skip immediately even if gap is non-zero, since that gap is
        // part of time between previous id's timepoint and the next (possibly non-end) timepoint,
        // i.e. the gap is not a part of a scope, so we won't miss any part of a scope.
        if self.prev_id.is_end() {
            self.current_time += self.total_gap;
            assert!(self.apply_and_clear_skip() == Some(block_level));
            return true;
        }
        // Skip is deferred and will occur as soon as the current scope is finished.
        false
    }

    /// Check if we can skip specified block, and if so, set `pending_skip` to that skip operation.
    ///
    /// * `track`       Track we're viewing.
    /// * `block_idx`   Index of the block at its block level.
    /// * `block_level` Block level containing the block we want to skip.
    ///
    /// Returns false if we can't skip this block, true if we can and we've set `pending_skip` -
    /// but this does not mean we can apply the skip immediately, see `try_skip_block`.
    fn can_skip_block(&mut self, track: &mut Track, block_idx: usize, block_level: usize) -> bool {
        let blocks: &Vec<TimepointBlock> = &track.block_levels[block_level];
        // Skipping the last block is complicated to decide so just don't do that.
        if block_idx + 1 >= blocks.len() {
            return false;
        }
        // If there is already a pending skip (from a higher block level), don't skip now, just
        // wait for timepoint processing to execute the pending skip.
        if self.pending_skip.is_some() {
            return false
        }

        let block: TimepointBlock = blocks[block_idx];
        let block_start_time      = block.start_time;
        let next_block_start_time = blocks[block_idx + 1].start_time;
        // Last non-gap timepoint ID in the block.
        let block_end_timepoint_id = {
            let block_end_timepoint_id = block.end_timepoint_id;
            if block_end_timepoint_id.is_gap() {
                // Get the last non-gap timepoint ID so we know what to start drawing scopes in next
                // block from. We don't need to get the delta since current_time will be set to
                // the start of new block, from which only new time deltas apply.
                let next_block_start_idx = (block_idx + 1) << (Stream::META_BLOCK_SIZE_EXP * block_level + Stream::BASE_BLOCK_SIZE_EXP);
                track.find_previous_nongap_timepoint(next_block_start_idx)
                     .unwrap_or_else(|| TimepointID::end(NestLevel(0)))
            }
            else {
                block_end_timepoint_id
            }
        };

        let block_duration        = next_block_start_time - block_start_time;
        assert!(block.total_not_covered <= block_duration,
            "block area not covered by scopes must be smaller than full block area");
        let block_covered = block_duration - block.total_not_covered;

        // For blocks with short duration, this will skip the block if it has low coverage.
        // For blocks with long duration, this will skip the block if its coverage wouldn't hide
        // a significant amount of visible scopes.
        let skippable_coverage = block_covered * 10 < block_duration && block_covered < self.time_resolution * 2;
        // Blocks large enough to be visible, and not so sparse that their scopes would be barely visible.
        if block_duration < self.time_resolution || skippable_coverage || next_block_start_time < self.start_time {
            // Initialize a skip. Will be applied immediately if possible, otherwise after current
            // scope is finished.
            self.pending_skip = Some(SkipParams{
                prev_id:      block_end_timepoint_id,
                current_time: next_block_start_time,
                block_level,
                not_covered:  block.total_not_covered
            });
            true
        } else {
            false
        }
    }

    /// Add a time interval with specified duration & coverered (by scopes) duration to curent
    /// merged scope.
    ///
    /// If we aren't building a merged scope, starts a new one. If the merged scope would reach
    /// conditions for it to be finished (i.e. would be too diffuse/sparse with new addition),
    /// we finish the merged scope.
    fn add_to_merged_scope(&mut self, duration: StreamTime, covered_duration: StreamTime) {
        match self.merged_scope {
            None => {
                self.merged_scope = Some(MergedScope{ 
                    start:   self.current_time,
                    covered: covered_duration,
                    duration
                });
            },
            Some(ref mut merged) => {
                let (total_duration, total_covered) =
                    merged_coverage_and_duration(*merged, self.current_time, duration, covered_duration);
                // Avoid multiplication overflow.
                let safe_duration = total_duration < Self::MAX_SAFE_MERGED_DURATION;
                // If the resulting scope would have enough coverage, continue it, otherwise finish it.
                if safe_duration && total_covered * u64::from(u16::MAX) >= total_duration * Self::COVERAGE_CUTOFF_THRESHOLD
                {
                    merged.duration = total_duration;
                    merged.covered  = total_covered;
                } else {
                    self.finish_merged_scope();
                    // we don't add the 'new' interval to any scope here - it's too sparse to be
                    // over COVERAGE_CUTOFF_THRESHOLD since it would have decreased current
                    // merged scope's coverage to go below that threshold.
                }
            }
        }
    }

    /// Apply and clear `self.pending_skip` if not None.
    fn apply_and_clear_skip(&mut self) -> Option<usize> {
        if let Some(p) = self.pending_skip {
            assert!(p.current_time >= self.current_time, "cannot skip backwards in time");
            let remaining_block_duration = p.current_time - self.current_time;
            // The part of block passed before applying the skip is covered by definition, otherwise
            // we'd apply the skip immediately instead of processing more timepoints.
            assert!(p.not_covered <= remaining_block_duration,
                    "apply_and_clear_skip: remaining duration must include all uncovered spans (not_covered {}, remaining_block_duration {}, block_level {})",
                    p.not_covered.hpsecs, remaining_block_duration.hpsecs, p.block_level);
            let covered_duration = remaining_block_duration - p.not_covered;

            self.add_to_merged_scope(remaining_block_duration, covered_duration);
            self.previous_skipped[p.block_level] = true;
            // Must update prev_id/current_time/total_gap as we skipped some timepoints.
            self.prev_id      = p.prev_id;
            self.current_time = p.current_time;
            self.total_gap    = StreamTime{hpsecs: 0};
            let result        = Some(p.block_level);
            self.pending_skip = None;
            result
        } else {
            None
        }
    }

    /// Finish processing a timepoint with specified ID and time delta (including gaps).
    ///
    /// Returns true if the timepoint is still within viewed time window, false if we've reached
    /// beyond the time window (and don't need to process more timepoints).
    fn finish_timepoint(&mut self, id: TimepointID, delta: StreamTime) -> bool {
        self.prev_id       = id;
        self.total_gap     = StreamTime{hpsecs: 0};
        self.current_time += delta;
        self.current_time < self.end_time
    }

    /// Finish the merged scope currently being built, if any - e.g. when we need to start drawing a real scope.
    fn finish_merged_scope(&mut self) {
        if let Some(merged) = self.merged_scope {
            if merged.duration >= self.time_resolution  {
                let coverage = if !merged.duration.is_zero() {
                    (merged.covered * u64::from(u16::MAX) / merged.duration).try_into().unwrap()
                } else {
                    u16::MAX
                };
                self.buffer.push(Scope {
                    start:            merged.start,
                    duration:         merged.duration,
                    id:               TimepointID::merged(coverage),
                    // Merged scopes ignore data events inside (they wouldn't be readable anyway).
                    data_event_first: 0,
                    data_event_count: 0
                });
            }
            self.merged_scope = None;
        }
    }

    /// Finish and add to buffer a scope starting at previous timepoint with specified duration.
    ///
    /// Returns true if the scope is large enough to be potentially visible based on `self.time_resolution`, false otherwise.
    fn finish_scope(&mut self, duration: StreamTime) -> bool {
        if duration < self.time_resolution {
            // Scope is too short to be visible, add it to a merged scope.
            if !duration.is_zero() {
                self.add_to_merged_scope(duration, duration);
            }
            false
        } else {
            // Finish current merged scope, if any, and add the finished real scope.
            self.finish_merged_scope();

            self.buffer.push(Scope{
                start:            self.current_time,
                duration,
                id:               self.prev_id,
                // These two are set after `finish_scope` ends.
                data_event_first: 0,
                data_event_count: 0
            });
            true
        }
    }
}

/// Stream - all profiling data recorded by a single nanoprof instance (usually a thread of profiled process).
pub struct Stream {
    /// ID of the stream.
    id:                StreamID,
    /// Track for internal nanoprof profiling.
    internal_track:    Track,
    /// Tracks with user-recorded timepoints, from shallowest to deepest nest levels.
    nest_level_tracks: Vec<Track>,
    /// Value of the earliest timepoint in the stream, if any.
    first_update_time: Option<StreamTime>,
    /// Value of the latest timepoint in the stream, if any.
    last_update_time:  Option<StreamTime>,
    /// Nest level of the last added scope. Helps optimize code cutting off preceding scopes at deeper nest levels.
    last_nest_level:   NestLevel,
    /// Statistics of code used for viewing scopes (currently `track_scope_view`).
    view_stats:        StreamViewStats
}

// This struct is definitely not going to be used at compile-time.
#[allow(clippy::missing_const_for_fn)]
impl Stream {
    /// Log2 of number of level L-1 blocks in each level L block where L > 0.
    const META_BLOCK_SIZE_EXP:      usize = 3;
    /// Log2 of number of timepoints in each level 0 block.

    const BASE_BLOCK_SIZE_EXP:      usize = 7; // 4 gives very slightly better perf at cost of much more memory usage
    /// Data events in empty scopes (with zero duration between timepoint) end up being visible
    /// in all scopes starting with that empty scopes' time - so if we have a 1000 empty scopes
    /// followed by a non-empty scope, we may add 1000 scopes' worth of data events to a
    /// `track_scope_view` output.
    ///
    /// This sets a hard limit on how many such 
    const MAX_VISIBLE_SCOPE_DATA_EVENTS: usize = 100;

    /// Construct a new stream with specified ID, with no timepoints in any track.
    pub fn new(id: StreamID) -> Self {
        Self {
            id,
            internal_track:    Track::default(),
            nest_level_tracks: std::iter::repeat(Track::default()).take(255).collect::<Vec<_>>(),
            first_update_time: None,
            last_update_time:  None,
            last_nest_level:   NestLevel(NestLevel::INTERNAL_PROFILING.0 + 1),
            view_stats:        StreamViewStats::new()
        }
    }

    /// Get view stat counters and reset them to 0.
    pub fn get_and_reset_view_stats(&mut self) -> StreamViewStats {
        let result = self.view_stats;
        self.view_stats.clear();
        result
    }

    /// Get ID of this stream.
    pub fn get_id(&self) -> StreamID {
        self.id
    }

    /// Return true if there are no timepoints recorded in this track/for this nest level.
    pub fn is_track_empty(&self, level: NestLevel) -> bool {
        self.track_at_level(level).is_empty()
    }

    /// Get all data events in specified scope on specified nest level.
    pub fn get_scope_data_events( 
        &mut self,
        scope: Scope,
        level: NestLevel ) -> &[TrackDataEvent]
    {
        let track: &Track = self.track_at_level(level);
        let (start_data_index, end_data_index) = track.data_event_bounds(scope.start, scope.end());
        &track.data_events[start_data_index..end_data_index]
    }

    /// Get a copy of scopes potentially renderable on `level`at `time_resolution` between `start_time` and `end_time`.
    ///
    /// The scopes are copied into `buffer`. Data events in those scopes are copied into `data_buffer`.
    ///
    /// Scopes too small to be rendered may be aggregated and added as 'merged scopes' - data events
    /// for these scopes are not copied.
    pub fn track_scope_view(
        &mut self,
        buffer:          &mut Vec<Scope>,
        data_buffer:     &mut Vec<(TrackDataEvent, bool)>,
        level:           NestLevel,
        start_time:      StreamTime,
        end_time:        StreamTime,
        time_resolution: StreamTime)
    {
        self.view_stats.scope_view_calls += 1;
        let track: &mut Track                      = self.track_at_level_mut(level);
        let first_top_block_index                  = track.start_block_index(Track::BLOCK_LEVELS - 1, start_time);
        // Index of the first timepoint in the first top-level block that starts before viewed area.
        let first_top_block_start_timepoint_index = first_top_block_index << (Self::META_BLOCK_SIZE_EXP * (Track::BLOCK_LEVELS - 1) + Self::BASE_BLOCK_SIZE_EXP );

        // Most track_scope_view logic in in this struct.
        let mut state = ScopeViewBuilderState {
            prev_id:           track.find_previous_nongap_timepoint(first_top_block_start_timepoint_index)
                                    .unwrap_or_else(|| TimepointID::end(level)),
            total_gap:         StreamTime{hpsecs: 0},
            // Time of the last timepoint, ignoring gaps within scopes (which are added up to time_delta)
            current_time:      track.block_levels[Track::BLOCK_LEVELS-1][first_top_block_index].start_time,
            buffer,
            start_time,
            end_time,
            time_resolution,
            pending_skip:      None,
            merged_scope:      None,
            previous_skipped:  [false; Track::BLOCK_LEVELS]
        };

        // Stat counters, added to counters in `self.view_stats` in the end.
        let mut blocks_skipped       = [0; Track::BLOCK_LEVELS];
        let mut blocks_processed     = [0; Track::BLOCK_LEVELS];
        let mut timepoints_processed = 0;

        // Iterate top-level (level 2) blocks.
        'outer: for mmb in first_top_block_index..track.block_levels[2].len() {
            // Skip blocks too short to be visible.
            if state.try_skip_block(track, mmb, 2) {
                blocks_skipped[2] += 1;
                continue;
            }

            let l2_block_start_block_idx = mmb << Self::META_BLOCK_SIZE_EXP;
            let l2_block_end_block_idx   = cmp::min((mmb + 1) << Self::META_BLOCK_SIZE_EXP, track.block_levels[1].len());
            // Iterate level 1 blocks.
            'next_l1_block: for mb in l2_block_start_block_idx..l2_block_end_block_idx {
                // Skip blocks too short to be visible.
                if state.try_skip_block(track, mb, 1) {
                    blocks_skipped[1] += 1;
                    continue;
                }

                let l1_block_start_block_idx = mb << Self::META_BLOCK_SIZE_EXP;
                let l1_block_end_block_idx   = cmp::min((mb + 1) << Self::META_BLOCK_SIZE_EXP, track.block_levels[0].len());
                // Iterate level 0 blocks.
                'next_block: for b in l1_block_start_block_idx..l1_block_end_block_idx {
                    // Skip blocks too short to be visible.
                    if state.try_skip_block(track, b, 0) {
                        blocks_skipped[0] += 1;
                        // prev_id/current_time does not change within gaps
                        continue;
                    }

                    let (block_start_timepoint_idx, block_end_timepoint_idx) = track.base_block_bounds(b);
                    // Iterate timepoints in the L0 block.
                    for t in block_start_timepoint_idx..block_end_timepoint_idx {
                        timepoints_processed += 1;
                        let id    = track.timepoint_id_at(t).to_timepoint_id();
                        let delta = track.timepoint_delta_at(t);
                        // If the current timepoint is a gap, it adds up to the next non-gap
                        // timepoint's delta.
                        if id.is_gap() {
                            state.total_gap += delta.gap_time_delta(id);
                            continue;
                        }

                        // Non-gap - finisheds a scope, whether visible or not.
                        let total_delta = StreamTime{hpsecs: state.total_gap.hpsecs + u64::from(delta.0)};

                        // Must be in the visible area to be added to view.
                        let intersects_visible_area = state.current_time + total_delta >= start_time;
                        // On an 'end' timepoint, do nothing: last timepoint ended a scope, current
                        // one may start a scope but that will be handled with the next timepoint.
                        if intersects_visible_area && !state.prev_id.is_end() {
                            // proper scope large enough to be visible - add it
                            if state.finish_scope(total_delta) {
                                // The scope we just added.
                                let last_scope: &mut Scope = state.buffer.last_mut().unwrap();

                                // All data events in this scope view are in this range.
                                let (mut start_data_index, end_data_index) =
                                    track.data_event_bounds(last_scope.start, last_scope.end());

                                // HACK to avoid spamming the view when many zero-sized scopes
                                // effectively merge their data events
                                if end_data_index - start_data_index > Self::MAX_VISIBLE_SCOPE_DATA_EVENTS {
                                    start_data_index = end_data_index - Self::MAX_VISIBLE_SCOPE_DATA_EVENTS;
                                }

                                // data_event_first points to the first event we're about to add
                                // to data_buffer.
                                last_scope.data_event_first = data_buffer.len();
                                // While we have events events are within scope time bounds,
                                // add them to data buffer.
                                for data in &track.data_events[start_data_index..end_data_index] {
                                    data_buffer.push((data.clone(), false));
                                }
                                last_scope.data_event_count = data_buffer.len() - last_scope.data_event_first;
                            }
                        } 


                        if !state.finish_timepoint(id, total_delta) {
                            // done, reached a timepoint out of visible range
                            break 'outer;
                        }

                        // If we need to skip to the next block on any level, do it.
                        if let Some(block_level) = state.apply_and_clear_skip() {
                            blocks_skipped[block_level] += 1;
                            match block_level {
                                2 => continue 'outer,
                                1 => continue 'next_l1_block,
                                0 => continue 'next_block,
                                _ => panic!("unsupported block level")
                            }
                        }
                    }
                    blocks_processed[0] += 1;
                }
                blocks_processed[1] += 1;
            }
            blocks_processed[2] += 1;
        }

        // If there's an open scope at the end, add it as infinite.
        if !state.prev_id.is_end() {
            state.finish_scope(StreamTime::INF);
        }
        // If we're building a merged scope, finish it.
        state.finish_merged_scope();
        // Save stat counters.
        for i in 0 .. Track::BLOCK_LEVELS {
            self.view_stats.blocks_skipped[i]   += blocks_skipped[i];
            self.view_stats.blocks_processed[i] += blocks_processed[i];
        }
        self.view_stats.timepoints_processed += timepoints_processed;
    }

    /// Collect and return debug stats about all tracks in this stream.
    pub fn collect_debug_stats(&mut self) -> TrackStats {
        let mut stats = TrackStats::new();

        self.internal_track.add_stats(&mut stats);
        for track in &mut self.nest_level_tracks {
            track.add_stats(&mut stats);
        }

        stats
    }

    /// Collect statistics about scopes added at specified nest level since the last call for that nest level,
    /// excluding the last (ongoing) scope.
    ///
    /// Each scope is passed to `process_scope` which implements the statistics collection.
    pub fn collect_stats<F>(&mut self, level: NestLevel, mut process_scope: F) 
        where F: FnMut(Scope)
    {
        let track: &mut Track = self.track_at_level_mut(level);
        if track.is_empty() {
            return;
        }

        // Iterate timepoints since the last call, generate scopes, and collect stats about them.
        for t in track.stats_next_timepoint..track.timepoint_count() {
            let id = track.timepoint_id_at(t).to_timepoint_id();
            let delta = track.timepoint_delta_at(t);

            if id.is_gap() {
                track.stats_total_gap += delta.gap_time_delta(id);
                continue;
            } 

            let total_delta = StreamTime{hpsecs: track.stats_total_gap.hpsecs + u64::from(delta.0)};
            if !track.stats_prev_id.is_end() {
                process_scope(Scope{
                    start:            track.stats_current_time,
                    duration:         total_delta,
                    id:               track.stats_prev_id,
                    data_event_first: 0,
                    data_event_count: 0
                });
            }

            track.stats_prev_id   = id;
            track.stats_total_gap = StreamTime{hpsecs: 0};
            track.stats_current_time += total_delta;
        }
        track.stats_next_timepoint = track.timepoint_count();
    }

    /// Get the total number of timepoints so far.
    pub fn timepoint_count(&self) -> u64 {
        let mut result = self.internal_track.timepoint_count() as u64;
        for track in &self.nest_level_tracks {
            result += track.timepoint_count() as u64;
        }
        result
    }

    /// Get the number of gaps in the entire stream.
    pub fn gap_count(&self) -> u64 {
        self.nest_level_tracks.iter().map(Track::gap_count).sum::<u64>() + self.internal_track.gap_count()
    }

    /// Add a data event recorded after the last timepoint added to the stream (on any track).
    ///
    /// * `last_timepoint_nest_level`: Nest level of the last timepoint.
    ///   The data event is added to the highest track/level *up to* `last_timepoint_nest_level`
    ///   that currently ends with an open scope (i.e. doesn't end by an 'end' timepoint) - it 
    ///   belongs to that scope.
    ///   This ensures the data event respects scoping / belongs to the current ongoing scope at
    ///   the moment of recording.
    /// * `data`: The data event to add.
    pub fn add_data_event_after_last_timepoint(&mut self, last_timepoint_nest_level: NestLevel, data: DataEvent) {
        // The data event must be in the currently ongoing scope, if any.
        // We're only in a scope if we're not after an 'end' event.
        for l in (NestLevel::INTERNAL_PROFILING.0+1..=last_timepoint_nest_level.0).rev() {

            let time = self.last_update_time.expect("must be called after add_timepoint(), so this must be Some");
            let track: &mut Track = self.track_at_level_mut(NestLevel(l));
            // We're after an end timepoint on this level, so we are not in a scope on this level.
            // Go down a level to check if we're at that level.
            if track.last_id.is_end() {
                // We're not in a scope at last timepoint's nest level, so we must be in a
                // lower-level (shallower) scope.
                continue;
            }

            // Data event belongs to this track.
            assert!(!track.last_id.is_gap(), "last timepoint cannot be gap outside of `add_timepoint()`");
            // We're after the last added non-end timepoint, on current nest level - we're in the
            // scope started by that timepoint, so add the data event to this level's track
            track.data_events.push(TrackDataEvent{
                _timepoint_index: track.timepoint_count() - 1,
                // this may be different from the track's last timepoint; we're using time from
                // the *stream's* last timepoint - this actually improves time precision a bit.
                time,
                data
            });
            return;
        }

        // Fell through all levels - we're not in any scope at all.
        log::info!(target:"stream", "data event outside of all scopes - this is not handled yet" );
    }

    /// Add a timepoint to the stream.
    ///
    /// * `id`: ID of the timepoint - may be a normal or 'scope end' timepoint.
    /// * `time`: Absolute time of the timepoint (after its time delta). Delta will be reconstructed internally.
    /// * `nest_level`: Nest level (track) of the timepoint.
    pub fn add_timepoint(&mut self, id: TimepointID, time: StreamTime, nest_level: NestLevel) {
        assert!(time >= self.last_update_time.unwrap_or(StreamTime::MIN),
            "we only support adding timepoints in a monotone fashion, from earlier to later");

        // Update the update times.
        self.last_update_time = Some(time);
        if self.first_update_time.is_none() {
            self.first_update_time = Some(time);
        }

        // For current nest level, add the timepoint (and any gap timepoints preceding it).
        self.add_timepoint_to_track(id, time, nest_level);
        // Timepoints on the internal profiling level don't affect scopes on other levels.
        if nest_level.0 != NestLevel::INTERNAL_PROFILING.0 {
            // Add an 'end' timepoint to end any incomplete deeper (nested) scopes - end of
            // 'parent' scope ends 'child' scope.
            if nest_level.0 < self.last_nest_level.0 {
                // No need to go above last_nest_level, since previous timepoint at that higher level
                // would have already inserted ending events for open timepoints at higher levels.
                for l in nest_level.0 + 1 ..=self.last_nest_level.0 {
                    let level = NestLevel(l);
                    // If there is any timepoint at this level, and the last timepoint is non-end, add an end after it.
                    if let Some(last_id_16) = self.track_at_level_mut(level).timepoint_id_last() {
                        let last_id = last_id_16.to_timepoint_id();
                        if !last_id.is_end()  {
                            assert!(!last_id.is_gap(), 
                                    "a track cannot end with a gap as any `add_timepoint()` call adding a gap must add a normal timepoint as well");
                            self.add_timepoint_to_track(TimepointID::end(level), time, level);
                        }
                    }
                }
            }
            self.last_nest_level = nest_level;
        } 
    }

    /// Get the last timepoint recorded in the stream, if any.
    pub fn most_recent_update_time(&self) -> Option<StreamTime> {
        self.last_update_time
    }

    /// Get the first timepoint recorded in the stream, if any.
    pub fn earliest_update_time(&self) -> Option<StreamTime> {
        self.first_update_time
    }

    /// Get the total duration from the first to the last recorded event in this stream.
    pub fn total_profiling_duration(&self) -> StreamTime {
        if let Some(first) = self.first_update_time {
            if let Some(last) = self.last_update_time {
                return last - first;
            }
        }
        StreamTime::MIN
    }

    /// Get the track at specified nest level.
    fn track_at_level(&self, level: NestLevel) -> &Track {
        if level.0 == NestLevel::INTERNAL_PROFILING.0 {
            &self.internal_track
        } else {
            // Last '-1' accounts for the internal profiling nest level, which is not in `nest_level_tracks`
            &self.nest_level_tracks[(usize::try_from((level.0 as isize) - (i8::MIN as isize) - 1).unwrap())]
        }
    }

    /// Get mutable access to track at specified nest level.
    fn track_at_level_mut(&mut self, level: NestLevel) -> &mut Track {
        if level.0 == NestLevel::INTERNAL_PROFILING.0 {
            &mut self.internal_track
        } else {
            &mut self.nest_level_tracks[(usize::try_from((level.0 as isize) - (i8::MIN as isize) - 1).unwrap())]
        }
    }

    /// Add a timepoint to track at specified nest level.
    fn add_timepoint_to_track(&mut self, id: TimepointID, time: StreamTime, nest_level: NestLevel) {
        let track: &mut Track = self.track_at_level_mut(nest_level);
        // Time delta of the newly added timepoint.
        let delta            = time - track.last_time;
        let mut current_time = track.last_time;
        track.last_time      = time;

        // Part of the delta we can represent in the timepoint itself.
        let timepoint_delta = StreamTime{hpsecs: delta.hpsecs & TimepointDelta::BITS_MASK};
        // Any extra bits we couldn't fit into the timepoint.
        let mut gap_bits    = delta.hpsecs >> TimepointDelta::BITS;
        let mut shift_level = TimepointDelta::BITS;

        // Updates blocks on all nest levels before adding a timepoint.
        let update_blocks = |track: &mut Track, current_time: StreamTime, id: TimepointID, delta: TimepointDelta| {

            // Update blocks on each block level.
            for level in 0 .. Track::BLOCK_LEVELS {
                let mask = (1 << (Self::META_BLOCK_SIZE_EXP * level + Self::BASE_BLOCK_SIZE_EXP)) - 1;

                // If we've reached enough timepoints to start a new block, start it.
                if track.timepoint_count() & mask == 0 {
                    let not_covered_time = if track.last_id.is_end() {
                        if id.is_gap() { delta.gap_time_delta(id) } else { timepoint_delta }
                    } else {
                        StreamTime{hpsecs: 0}
                    };
                    // `current_time` is *before* the timepoint, `time` is *after* it
                    track.block_levels[level].push(TimepointBlock{ 
                        start_time:        current_time,
                        total_not_covered: not_covered_time,
                        end_timepoint_id:  id
                    });
                } else {
                    // Update block coverage and end timepoint ID.
                    if track.last_id.is_end() {
                        let time = if id.is_gap() { delta.gap_time_delta(id) } else { timepoint_delta };
                        track.block_levels[level].last_mut().unwrap().total_not_covered += time;
                    }
                    track.block_levels[level].last_mut().unwrap().end_timepoint_id = id;
                }
            }
        };

        // Truncation is intended here.
        #[allow(clippy::cast_possible_truncation)]
        // If we couldn't fit the entire time delta, add gap timepoint/s before the timepoint to extend its delta.
        while gap_bits > 0 {
            let gap_delta = TimepointDelta((gap_bits & TimepointDelta::BITS_MASK) as u16);
            let gap_id    = TimepointID::gap(shift_level);

            // There may be 'TimepointDelta::BITS' consecutive zero bits, in which case we don't
            // need to add a gap, so check this.
            if gap_delta.0 > 0 {
                let gap_delta_time = gap_delta.gap_time_delta(gap_id);
                assert!(gap_delta_time <= delta,
                     "gap may not take more time than the delta it's supposed to cover");
                // If we've added enough timepoints to start a meta/block, add it.
                update_blocks(track, current_time, gap_id, gap_delta);
                current_time += gap_delta_time;
                track.gap_counters[shift_level as usize] += 1;
                track.add_timepoint(gap_id, gap_delta);
            }

            gap_bits >>= TimepointDelta::BITS;
            shift_level += TimepointDelta::BITS;
        }

        // Add the timepoint itself.
        let new_delta = TimepointDelta::from_streamtime(timepoint_delta);
        // If we've added enough timepoints to start a meta/block, add it.
        update_blocks(track, current_time, id, new_delta);
        current_time += timepoint_delta;
        assert!(current_time == time, "all time deltas must add up to the time we wanted to reach");
        track.add_timepoint(id, new_delta);
        track.last_id = id;
    }
}
