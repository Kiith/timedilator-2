use std::cmp;
use std::convert::{TryFrom, TryInto};
use std::time;
use std::char;
use std::sync::Arc;
use std::sync::Mutex;

use tui::{
    buffer::Buffer,
    buffer::Cell,
    layout::{Rect, Constraint, Direction, Layout, Alignment},
    style::{Color, Style, Modifier},
    widgets::{Block, Widget, StatefulWidget, Paragraph},
    text::Text
};

use crate::event_id::{
    NestLevel,
    TimepointID,
};
use crate::stream::{
    Stream,
    StreamID,
    StreamViewStats,
    Scope,
    StreamTime,
    TrackDataEvent
};
use crate::stream_reader::{
    ThreadStreamReader,
    IDRegistry,
    ScopeStats
};
use crate::color;
use crate::color::{
    BLACK,
    RED,
    WHITE,
    YELLOW,
    gray
};

/// Create a label (Paragraph) with specified text, color and alignment.
fn label<'a, T>(text: T, color: Color, align: Alignment) -> Paragraph<'a> 
  where T: Into<Text<'a>>
{
    Paragraph::new(text).style(Style::default().fg(color)).alignment(align)
}

/// Create a left-aligned label (Paragraph) with specified text and color.
fn label_left<'a, T>(text: T, color: Color) -> Paragraph<'a> 
  where T: Into<Text<'a>> 
{
    label(text, color, Alignment::Left)
}

/// Create a right-aligned label (Paragraph) with specified text and color.
fn label_right<'a, T>(text: T, color: Color) -> Paragraph<'a> 
  where T: Into<Text<'a>> 
{
    label(text, color, Alignment::Right)
}

const SUBCELLS_PER_CELL: usize = 8;

/// Of an array of 8 octant color, get the 1st and 2nd most common color.
///
/// If all colors are the same, both values will be the same.
fn top2_octant_colors(mut octant_colors: [color::Wrapper; 8]) -> (Color, Color) {
    let sorted_colors = {
        octant_colors.sort_unstable();
        &octant_colors
    };

    let mut current_count  = 1;
    let mut color_fg_count = 0;
    let mut color_bg_count = 0;
    let mut current_color  = sorted_colors[0].0;
    let mut color_fg       = BLACK;
    let mut color_bg       = BLACK;

    let mut update_max = |current_count: usize, current_color: Color| {
        if current_count > color_fg_count {
            color_bg_count = color_fg_count;
            color_bg       = color_fg;
            color_fg_count = current_count;
            color_fg       = current_color;
        } else if current_count > color_bg_count {
            color_bg_count = current_count;
            color_bg       = current_color;
        }
    };

    // Iterate over a sorted array of octant colors, counting instances of the current color,
    // and if the color changes, check if it's the 1st or 2nd most common.
    for c in &sorted_colors[1..] {
        if c.0 == current_color {
            current_count += 1;
        } else {
            update_max(current_count, current_color);
            current_count = 1;
            current_color = c.0;
        }
    }
    update_max(current_count, current_color);

    (color_fg, color_bg)
}

/// Get a glyph best representing a scope starting at octant `start` of a cell, ending at octant `end` (both inclusive)
///
/// Octant values can be 0-7.
///
/// This cannot return an 'empty' symbol (' ') since both start and end are inclusive.
const fn subcells_symbol(start: u8, end: u8) -> (&'static str, bool) {
    match (start, end) {
        (0,     0)     => (tui::symbols::block::ONE_EIGHTH,     false),
        (0 | 1, 1)     => (tui::symbols::block::ONE_QUARTER,    false),
        (0 | 1, 2)     => (tui::symbols::block::THREE_EIGHTHS,  false),
        (0 | 1, 3)     => (tui::symbols::block::HALF,           false),
        (0 | 1, 4)     => (tui::symbols::block::FIVE_EIGHTHS,   false),
        (0 | 1, 5)     => (tui::symbols::block::THREE_QUARTERS, false),
        (0 | 1, 6)     => (tui::symbols::block::SEVEN_EIGHTHS,  false),
        (0,     7)     => (tui::symbols::block::FULL,           false),
        (1,     7)     => (tui::symbols::block::ONE_EIGHTH,     true),
        (2,     2 | 3) => ("⡇",                                 false),
        (2 | 3, 4 | 5) => ("┃",                                 false),
        (2,     6 | 7) => (tui::symbols::block::ONE_QUARTER,    true),
        (3 | 4, 3 | 4) => ("│",                                 false),
        (3,     6 | 7) => (tui::symbols::block::THREE_EIGHTHS,  true),
        (4 | 5, 5)     => ("⢸",                                 false),
        (4,     6 | 7) => (tui::symbols::block::HALF,           true),
        (5,     6 | 7) => (tui::symbols::block::FIVE_EIGHTHS,   true),
        (6,     6 | 7) => (tui::symbols::block::THREE_QUARTERS, true),
        (7,     7)     => (tui::symbols::block::SEVEN_EIGHTHS,  true),
        _              => ("?", false)
    }
}

/// Get the symbol that best approximates a cell with specified octant colors. The symbol has to
/// have specified background/foreground colors, and whether to swap these colors to get a better
/// match.
///
/// * `color_fg`: most common color in `octants`
/// * `color_bg`: 2nd most common color in `octants`
/// * `octants`: colors of each octant of drawn cell
///
/// Returns (`symbol`, `invert`) - if `invert` is true, background and foreground colors should be inverted.
fn subcells_symbol_match_octants<'a>(color_fg: Color, color_bg: Color, octants: &[color::Wrapper; 8]) -> (&'a str, bool) {
    let mut current_symbol         = "?";
    let mut current_invert         = 0;
    let mut current_mismatch_score = 8;

    // Color 'maps' representing each glyph, to compare against
    let matchers = [
        ([1, 0, 0, 0, 0, 0, 0, 0], tui::symbols::block::ONE_EIGHTH,     1),
        ([1, 1, 0, 0, 0, 0, 0, 0], tui::symbols::block::ONE_QUARTER,    1),
        ([1, 1, 1, 0, 0, 0, 0, 0], tui::symbols::block::THREE_EIGHTHS,  1),
        ([1, 1, 1, 1, 0, 0, 0, 0], tui::symbols::block::HALF,           1),
        ([1, 1, 1, 1, 1, 0, 0, 0], tui::symbols::block::FIVE_EIGHTHS,   1),
        ([1, 1, 1, 1, 1, 1, 0, 0], tui::symbols::block::THREE_QUARTERS, 1),
        ([1, 1, 1, 1, 1, 1, 1, 0], tui::symbols::block::SEVEN_EIGHTHS,  1),
        ([1, 1, 1, 1, 1, 1, 1, 1], tui::symbols::block::FULL,           1),
        // ([0, 1, 1, 0, 0, 0, 0, 0], "⡇",                                 0),
        // ([0, 0, 0, 0, 0, 1, 1, 0], "⢸",                                 0),
        ([0, 0, 0, 1, 1, 0, 0, 0], "│",                                 0),
        ([0, 0, 1, 1, 1, 1, 0, 0], "┃",                                 0),
    ];

    // Try out all matchers, and some that allow it even in inverted form.
    // Find the matcher that most closely approximates `octants` using `color_fg` and `color_bg`
    'outer: for (matcher, symbol, can_invert) in matchers {
        'inv: for inv in 0 ..=can_invert {
            let mut mismatches = 0;
            for o in 0 .. 8 {
                let selector = matcher[o] ^ inv;
                let color = octants[o].0;
                let match_color = if selector == 1 {color_fg} else {color_bg};

                // Do not allow black (no scope) color to be overdrawn by any color (even color_fg),
                // and otherwise don't allow the foreground color to be overdrawn by the background
                // - even if we would have a more exact match. This makes mismatches/rendering
                // artifacts on cells at different tracks more consistent, making
                // 'child scope bigger than parent' artifacts less common.
                if color_fg != BLACK && color == color_fg && match_color != color_fg {
                    continue 'inv;
                }
                if color != BLACK && match_color == BLACK {
                    continue 'inv;
                }

                if color != match_color {
                    mismatches += 1;
                    // Worse than current best match; skip.
                    if mismatches >= current_mismatch_score {
                        continue 'inv;
                    }
                }
            }
            // If we have a better match, save it.
            if mismatches <= current_mismatch_score {
                current_symbol         = symbol;
                current_invert         = inv;
                current_mismatch_score = mismatches;
            }

            // Perfect match - stop.
            if mismatches == 0 {
                break 'outer;
            }
        }
    }
    (current_symbol, current_invert != 0) 
}

struct DrawCellCounters {
    gap:        u64,
    full:       u64,
    partial_1:  u64,
    partial_2:  u64,
    rasterized: u64,
}
impl DrawCellCounters {
    const fn new() -> Self {
        Self{
            gap:        0,
            full:       0,
            partial_1:  0,
            partial_2:  0,
            rasterized: 0
        }
    }

    fn clear(&mut self) {
        self.gap        = 0;
        self.full       = 0;
        self.partial_1  = 0;
        self.partial_2  = 0;
        self.rasterized = 0;
    }

    fn as_string(&self) -> String {
        format!("G{},F{},P{},{},R{}", self.gap, self.full, self.partial_1, self.partial_2, self.rasterized)
    }
}

/// State of a stream view widget. Owns the stream reading threads.
pub struct StreamViewState {
    /// All stream reading threads.
    pub stream_readers:     Vec<ThreadStreamReader>,
    /// ID of the currently viewed stream.
    current_stream_id:      StreamID,
    /// Time corresponding to the center of the widget. Center is defined as right edge - time_window_cells / 2
    time_window_center:     StreamTime,
    /// Time of the first timepoint in any stream - start time of profiled data.
    earliest_update_time:   StreamTime,
    /// If true, the view automatically tracks the latest data.
    auto_pan:               bool,
    /// Time interval represented by a single terminal cell; resolution of the view.
    time_per_cell:          StreamTime,
    /// Scope colors are assigned from this palette.
    colors:                 Vec<Color>,
    /// Drawn scopes are vertically offset by this many cells, used for vertical panning.
    vertical_pan:           i32,
    /// Width of the currently viewed time window in cells. This is currently equal to widget width.
    time_window_cells:      u16,
    /// Set to the cell the user left-clicked this frame, if any.
    select_coords:          Option<(u16, u16)>,
    /// Set to the cell the user right-clicked this frame, if any.
    focus_coords:           Option<(u16, u16)>,
    /// Currently selected scope, if any.
    selected_scope:         Option<Scope>,
    /// Stream containing currently selected scope, if any.
    selected_scope_stream:  Option<StreamID>,
    /// ID of the currently focused stream, if any (for multi-stream view).
    focus_stream_id:        Option<StreamID>,
    /// True if this state is used to draw multiple ('All') synchronized streams, by just alternating current stream ID.
    synchronized:           bool,
    /// Statistical counters about scope cell rendering.
    cell_counters:          DrawCellCounters,
    /// Show debug information.
    debug_mode:             bool
}

// This struct is definitely not going to be used at compile-time.
#[allow(clippy::missing_const_for_fn)]
impl StreamViewState {
    /// Resolution levels / `time_per_cell` values the user can switch between.
    /// 
    /// Using a few fixed resolution values helps with comparisons / reproducibility.
    const RESOLUTION_LEVELS: [StreamTime; 28] = [
        StreamTime::from_picoseconds(1000), //ns
        StreamTime::from_picoseconds(2 * 1000),
        StreamTime::from_picoseconds(5 * 1000),
        StreamTime::from_picoseconds(10 * 1000),
        StreamTime::from_picoseconds(20 * 1000),
        StreamTime::from_picoseconds(50 * 1000),
        StreamTime::from_picoseconds(100 * 1000),
        StreamTime::from_picoseconds(200 * 1000),
        StreamTime::from_picoseconds(500 * 1000),

        StreamTime::from_picoseconds(1000 * 1000), //us
        StreamTime::from_picoseconds(2 * 1000 * 1000),
        StreamTime::from_picoseconds(5 * 1000 * 1000),
        StreamTime::from_picoseconds(10 * 1000 * 1000),
        StreamTime::from_picoseconds(20 * 1000 * 1000),
        StreamTime::from_picoseconds(50 * 1000 * 1000),
        StreamTime::from_picoseconds(100 * 1000 * 1000),
        StreamTime::from_picoseconds(200 * 1000 * 1000),
        StreamTime::from_picoseconds(500 * 1000 * 1000),

        StreamTime::from_picoseconds(1000 * 1000 * 1000), //ms
        StreamTime::from_picoseconds(2 * 1000 * 1000 * 1000),
        StreamTime::from_picoseconds(5 * 1000 * 1000 * 1000),
        StreamTime::from_picoseconds(10 * 1000 * 1000 * 1000),
        StreamTime::from_picoseconds(20 * 1000 * 1000 * 1000),
        StreamTime::from_picoseconds(50 * 1000 * 1000 * 1000),
        StreamTime::from_picoseconds(100 * 1000 * 1000 * 1000),
        StreamTime::from_picoseconds(200 * 1000 * 1000 * 1000),
        StreamTime::from_picoseconds(500 * 1000 * 1000 * 1000),

        StreamTime::from_picoseconds(1000 * 1000 * 1000 * 1000), //s
    ];

    /// Construct a default stream view state, looking at stream with ID 0.
    pub fn new(stream_paths: &[String], reader_thread_count: usize) -> Self {
        let mut stream_readers = vec![];
        let mut next_stream = 0;
        for r in 0..reader_thread_count {
            let remaining_thread_count = reader_thread_count - r;
            let remaining_streams      = stream_paths.len() - next_stream;
            let streams_in_thread      = (remaining_streams + remaining_thread_count - 1) / remaining_thread_count;
            log::info!(target:"stream_view", "reading thread {r} reading {streams_in_thread} streams starting at {next_stream}" );
            let paths: Vec<String> = stream_paths[next_stream..next_stream+streams_in_thread].to_vec();
            stream_readers.push(ThreadStreamReader::new( StreamID(next_stream.try_into().unwrap()), paths ));
            next_stream += streams_in_thread;
        };
        Self {
            stream_readers,
            current_stream_id:      StreamID(0),
            time_window_center:     StreamTime::MIN,
            earliest_update_time:   StreamTime::INF,
            auto_pan:               true,
            //                      10ms per cell by default
            time_per_cell:          StreamTime::from_picoseconds(10 * 1000 * 1000 * 1000),
            colors:                 color::generate_scope_colors(),
            vertical_pan:           0,
            time_window_cells:      0,
            select_coords:          None,
            focus_coords:           None,
            selected_scope:         None,
            selected_scope_stream:  None,
            focus_stream_id:        None,
            synchronized:           false,
            cell_counters:          DrawCellCounters::new(),
            debug_mode:             false
        }
    }

    // synchronized is a crutch meaning 'when not showing all streams' - TODO write in a better way
    pub fn showing_all(&self) -> bool {
        self.synchronized
    }

    /// Reset input with deferred processing (input set by `command_XXX` that is processed later
    /// during rendering because it needs to know e.g. cell coords of a scope or widget).
    pub fn reset_input(&mut self) {
        self.select_coords = None;
        self.focus_coords  = None;
    }

    /// Start synchronized view (multiple/all streams) - changes handling of e.g. max panning bounds.
    pub fn command_synchronize(&mut self) {
        self.synchronized = true;
    }

    /// Stop synchronized view (multiple/all streams) - changes handling of e.g. max panning bounds.
    pub fn command_desynchronize(&mut self) {
        self.synchronized = false;
    }

    /// Toggle debug mode (display of information for debugging timedilator-2 itself)>
    pub fn command_toggle_debug_mode(&mut self) {
        self.debug_mode = !self.debug_mode;
    }

    /// Set the stream to view. `stream_idx` is an index that wraps around stream count.
    pub fn command_set_stream(&mut self, stream_idx: usize) {
        let stream_count = self.stream_count();
        assert!(stream_count != 0, "there must be at least a single stream");
        self.current_stream_id = StreamID((stream_idx % stream_count).try_into().unwrap());
    }

    /// Input command to switch to viewing the next stream, cycling in a round-robin fashion.
    pub fn command_next_stream(&mut self) {
        self.command_set_stream(self.current_stream_id.0 as usize + 1);
    }

    /// Input command to switch to viewing the previous stream, cycling in a round-robin fashion.
    pub fn command_previous_stream(&mut self) {
        let stream_count = self.stream_count();
        self.command_set_stream(self.current_stream_id.0 as usize + stream_count - 1);
    }

    /// Input command to zoom in by specified number of resolution levels.
    pub fn command_zoom_in(&mut self, steps: usize) {
        // from outermost to innermost levels
        for l in (0..Self::RESOLUTION_LEVELS.len()).rev() {
            let resolution = Self::RESOLUTION_LEVELS[l];
            if resolution == self.time_per_cell {
                self.set_zoom(Self::RESOLUTION_LEVELS[l - cmp::min(steps, l)]);
                return;
            } else if resolution < self.time_per_cell {
                // if the resolution has a value *between* levels, jump to the next level first
                self.set_zoom(Self::RESOLUTION_LEVELS[l - cmp::min(steps - 1, l)]);
                return;
            }
        }
        self.set_zoom(Self::RESOLUTION_LEVELS[0]);
    }

    /// Input command to zoom out by specified number of resolution levels.
    pub fn command_zoom_out(&mut self, steps: usize) {
        // from innermost to outermost levels
        for l in 0..Self::RESOLUTION_LEVELS.len() {
            let resolution = Self::RESOLUTION_LEVELS[l];
            if resolution == self.time_per_cell {
                self.set_zoom(Self::RESOLUTION_LEVELS[cmp::min(l + steps, Self::RESOLUTION_LEVELS.len() - 1)]);
                return;
            } else if resolution > self.time_per_cell {
                // if the resolution has a value *between* levels, jump to the next level first
                self.set_zoom(Self::RESOLUTION_LEVELS[cmp::min(l + steps - 1, Self::RESOLUTION_LEVELS.len() - 1)]);
                return;
            } 
        }
        self.set_zoom( Self::RESOLUTION_LEVELS[Self::RESOLUTION_LEVELS.len() - 1] );
    }

    /// Input command to pan horizontally by specified number of cells. Disables autopan.
    pub fn command_pan(&mut self, cells: i16 ) {
        self.auto_pan = false;
        let cells_abs = u64::try_from(cells.abs()).unwrap();
        // StreamTime is non-negative so it can't be multiplied by a negative value.
        if cells < 0 {
            self.time_window_center -= self.time_per_cell * cells_abs;
        } else {
            self.time_window_center += self.time_per_cell * cells_abs;
        }
        self.correct_pan_horizontal();
    }

    /// Input command to pan horizontally by half a screenful.
    pub fn command_pan_halfscreens(&mut self, halfscreens: i16 ) {
        self.command_pan(halfscreens * i16::try_from(self.half_cells_right()).unwrap());
    }

    /// Input command to pan vertically by specified number of cells.
    pub fn command_vertical_pan(&mut self, cells: i32 ) {
        self.vertical_pan += cells;
        // See also: StreamView::correct_pan_vertical() - ensures we don't pan too far from potentially visible tracks.
    }

    /// Input command to disable/enable automatic panning (tracking newest recorded data).
    pub fn command_toggle_autopan(&mut self) {
        self.auto_pan = !self.auto_pan;
    }

    /// Input command to select (click) cell at specified coords, if any.
    pub fn command_select(&mut self, col: u16, row: u16) {
        self.select_coords = Some((col, row));
    }

    /// Input command to focus a stream view by clicking into it.
    ///
    /// As multiple stream views may use the same `StreamViewState` with alternating stream ID,
    /// this will be checked by all render() calls of such views, and focus the view that contains
    /// specified mouse coords.
    pub fn command_focus(&mut self, col: u16, row: u16) {
        self.focus_coords = Some((col, row));
    }

    /// If focusing on any stream, stop focusing.
    pub fn command_unfocus(&mut self) {
        self.focus_stream_id = None;
    }

    /// Get ID of currently focused stream, if any (so we can e.g. make it take more screen space).
    pub fn focused_stream(&self) -> Option<StreamID> {
        self.focus_stream_id 
    }

    /// Set zoom by setting the time interval represented by each cell, and correct view position so we don't go out of bounds.
    fn set_zoom(&mut self, new_time_per_cell: StreamTime ) {
        self.auto_pan = false;
        self.time_per_cell = new_time_per_cell;
        self.correct_pan_horizontal();
    }

    /// Correct horizontal position so we don't view past the last recorded event or before the first.
    ///
    /// If we cannot avoid both, we prioritize not seeing past the end.
    fn correct_pan_horizontal(&mut self) {
        self.set_time_window_start( cmp::max( self.time_window_start(), self.earliest_update_time ) );
        let most_recent_update_time = {
            let stream        = self.get_current_stream();
            let locked_stream = stream.lock().unwrap();
            locked_stream.most_recent_update_time()
        };

        let max_time = if self.synchronized { 
            // If we're viewing multiple synchronized streams, use the overall max update time as the bound
            self.all_streams_most_recent_update_time()
        } else {
            most_recent_update_time
        };

        if let Some( time ) = max_time {
            self.set_time_window_end( cmp::min( self.time_window_end(), time ) );
        }
    }

    /// Make sure vertical panning isn't too far from visible tracks, and return row to draw nest level 0 at.
    /// 
    /// area: area of the stream view (not including any statistics subwindow/split).
    fn correct_pan_vertical(&mut self, area: Rect) -> i32 {
        let center_row           = i32::from(area.top() + area.height / 2);
        let nestlevel_0_row      = center_row + self.vertical_pan;
        let nestlevel_top_row    = nestlevel_0_row - i32::from(NestLevel::MAX.0);
        let nestlevel_bottom_row = nestlevel_0_row - i32::from(NestLevel::MIN.0);

        if nestlevel_top_row >= i32::from(area.bottom()) {
            // If we've panned so much up that even the topmost nest level is at/below the bottom
            self.vertical_pan -= nestlevel_top_row - i32::from(area.bottom());
        }
        if nestlevel_bottom_row < i32::from(area.top()) {
            // If we've panned so much up that even the topmost nest level is at/below the bottom
            self.vertical_pan += i32::from(area.top()) - nestlevel_bottom_row;
        }
        center_row + self.vertical_pan
    }

    /// Width of the 'left half of view' for purposes of determining time window start relative to center.
    fn half_cells_left(&self) -> u16 {
        self.time_window_cells - self.time_window_cells / 2
    }

    /// Width of the 'right half of view' for purposes of determining time window start relative to center.
    fn half_cells_right(&self) -> u16 {
        self.time_window_cells / 2
    }

    /// Get time corresponding to the right end of the widget.
    fn time_window_end(&self) -> StreamTime {
        let end_half = self.time_per_cell * u64::from(self.half_cells_right());
        self.time_window_center + end_half
    }

    /// Time corresponding to the left edge of the widget.
    fn time_window_start(&self) -> StreamTime {
        let start_half = self.time_per_cell * u64::from(self.half_cells_left());
        self.time_window_center - start_half
    }

    /// Get overall most recent update time from among all streams.
    fn all_streams_most_recent_update_time(&mut self) -> Option<StreamTime> {
        let mut result = None;
        for thread in &mut self.stream_readers {
            for stream in thread.iter_streams() {
                let locked_stream = stream.lock().unwrap();
                if let Some(most_recent_update_time) = locked_stream.most_recent_update_time() {
                    match result {
                        None           => result = Some(most_recent_update_time),
                        Some(last_max) => result = Some(cmp::max(last_max, most_recent_update_time))
                    }
                }
            }
        }
        result
    }

    /// If autopan is enabled, update panning position to follow most recent profiling data.
    fn auto_pan_update(&mut self) {
        if !self.auto_pan {
            return
        }
        if self.synchronized {
            if let Some(time) = self.all_streams_most_recent_update_time() {
                self.set_time_window_end(time);
            }
        } else {
            let stream = self.get_current_stream();
            let locked_stream = stream.lock().unwrap();
            if let Some(most_recent_update_time) = locked_stream.most_recent_update_time() {
                self.set_time_window_end(most_recent_update_time);
            }
        }
    }

    /// Move the view horizontally by setting the last point in time visible in the view.
    fn set_time_window_end(&mut self, end: StreamTime) {
        let end_half = self.time_per_cell * u64::from(self.half_cells_right());
        self.time_window_center = end - end_half;
    }

    /// Move the view horizontally by setting the first point in time visible in the view.
    fn set_time_window_start(&mut self, start: StreamTime) {
        let start_half = self.time_per_cell * u64::from(self.half_cells_left());
        self.time_window_center = start + start_half;
    }

    /// Returns true if specified scope is currently selected by the user.
    fn is_scope_selected(&self, scope: &Scope) -> bool {
        self.selected_scope.map_or(
                false,
                // These fields are always the same for the same scope extracted from a Stream,
                // other fields may change as they're view-dependent.
                |selected| scope.start == selected.start && scope.duration == selected.duration && scope.id == selected.id )
    }

    /// Check if specified scope at specified coords should be selected, and if so, select it.
    ///
    /// Determines selection based on select coords that are not None if user has clicked this frame.
    fn update_selection(&mut self, col: u16, row: u16, scope: &Scope ) {
        if let Some((x, y)) = self.select_coords {
            // If we have a selection click, check if it's on current cell and if the cell only
            // intersects with one scope (not multiple merged scopes) - if so, select that scope.
            if x == col && y == row && !scope.id.is_merged() {
                self.selected_scope        = Some(*scope);
                self.selected_scope_stream = Some(self.current_stream_id);
            }
        }
    }

    /// Check if currently drawn view interscts any focus click coords, and if so, focus current stream.
    fn update_focus(&mut self, area: Rect) {
        if let Some((x, y)) = self.focus_coords {
            if x >= area.left() && x <= area.right() && y >= area.top() && y <= area.bottom() {
                self.focus_stream_id = Some(self.current_stream_id);
            }
        }
    }

    /// Get the color to use to draw scope with specified ID.
    fn scope_color_base(&self, scope: &Scope, is_selected: bool) -> (Color, Color) {
        let id = scope.id;
        // Merged scopes use grayscale
        let base = if id.is_merged() { // gray to avoid confusion with any real scope
            let level = u8::try_from((id.0 - TimepointID::MERGED_MIN.0) >> 13).unwrap();
            // Only use 8 levels of grayness for merged scopes, to make it easier to distinguish
            // high-level differences.
            gray(level * 32)
        } else if id.is_internal() {
            RED // red for easy visibility
        } else {
            self.colors[id.0 as usize % self.colors.len()]
        };
        if is_selected {
            if let Color::Rgb(r, g, b) = base {
                (Color::Rgb(255 - r, 255 - g, 255 - b), WHITE)
            } else {
                panic!("base is rgb");
            }
        } else {
            (base, BLACK)
        }
    }

    fn scope_color(&self, scope: &Scope ) -> (Color, Color) {
        self.scope_color_base(scope, self.is_scope_selected(scope))
    }

    fn scope_color_unselected(&self, scope: &Scope ) -> (Color, Color) {
        self.scope_color_base(scope, false)
    }

    /// Get the number of streams we currently have.
    fn stream_count(&self) -> usize {
        self.stream_readers.iter().map(ThreadStreamReader::stream_count).sum()
    }

    /// Get access to currently viewed stream.
    fn get_current_stream(&mut self) -> Arc<Mutex<Stream>> {
        for thread in &mut self.stream_readers {
            if let Some(stream) = thread.get_stream(self.current_stream_id) {
                return stream;
            }
        }
        panic!("current stream ... does not exist in any thread?");
    }

    /// Get access to the ID registry of the currently viewed stream.
    fn get_current_timepoint_registry(&mut self) -> Arc<Mutex<IDRegistry>> {
        for thread in &mut self.stream_readers {
            if let Some(registry) = thread.get_id_registry(self.current_stream_id) {
                return registry;
            }
        }
        panic!("current stream ... does not exist in any thread?");
    }
}

/// Data generated for each viewed scope on each frame.
struct TransientScopeData {
    /// Text to draw on the scope.
    label: [char; 63],
    /// Number of cells from `label` we've drawn so far.
    cells_drawn: u32
}

impl TransientScopeData {
    /// Get a character from the label for drawing, and move to the next character.
    fn eat_char(&mut self) -> char {
        if self.cells_drawn == 0 {
            self.cells_drawn+=1;
            return '🢒';
        }
        let char_idx = self.cells_drawn as usize - 1;
        if char_idx < self.label.len() {
            self.cells_drawn+=1;
            self.label[char_idx]
        } else {
            StreamView::GAP_MARKER.chars().next().unwrap()
        }
    }
}

/// Rational number-like promile type, with one promile precision.
#[derive(Copy, Clone)]
struct Promile(u64);

/// we can represent 18 gigaseconds at most with picoseconds, so this range of units is sufficient.
const UNITS: &[&str] = &["ns", "μs", "ms", "s", "ks", "Ms", "Gs"];

/// Convert a duration from picoseconds to specified unit.
fn to_time_unit( picoseconds: i128, unit: &str ) -> i128 {
    let mut value = picoseconds;
    for u in UNITS {
        value /= 1000;
        if *u == unit {
            break;
        }
    }
    value
}

/// Convert a duration to a time unit that's good for readable output, and return that duration
/// as thousandths of selected unit.
///
/// Repeatedly divides the picosecond value by `1000` to get below `1000_000` so we can represent
/// 'thousandths' of a unit (e.g. nanoseconds). At most 6 digits of precision. Can have less than 6
/// - e.g. 4 digits when we only have 'tens of thousands of thousandths of a unit'.
fn to_readable_time_unit( duration: StreamTime ) -> (&'static str, u32){
    let mut thousandths = duration.as_picoseconds(); // picoseconds are thousandths of a nanosecond
    let mut unit_index = 0;
    while thousandths >= 1_000_000 {
        thousandths /= 1000;
        unit_index += 1;
    }
    (UNITS[unit_index], thousandths.try_into().unwrap())
}

/// Write `duration` as a human-readable string up to `width` characters long, into `buffer`,
/// and return the number of characters written.
///
/// Uses `to_readable_time_unit`, so it has 3-6 digits of precision.
fn write_fixed_width( duration: StreamTime, width: usize, buffer: &mut [char] ) -> usize {
    assert!(width>=5, "write_fixed_width needs at least 5 characters of space");
    if duration.is_inf() {
        buffer[0] = '∞';
        return 1;
    }

    // Duration in thousandths of unit - always <1000000
    let (unit, thousandths) = to_readable_time_unit( duration );
    let unit_width   = unit.len();
    let value_width  = width - unit_width;
    let before_radix = thousandths / 1000; // <1000

    let mut used_chars = 0;
    if before_radix >= 100 {
        buffer[used_chars] = char::from_digit(before_radix / 100, 10).unwrap();
        used_chars += 1;
    }
    if before_radix >= 10 {
        buffer[used_chars] = char::from_digit((before_radix / 10) % 10, 10).unwrap();
        used_chars += 1;
    }
    buffer[used_chars] = char::from_digit(before_radix % 10, 10).unwrap();
    used_chars += 1;

    // only continue if we have enough space for at least '.' and one more digit
    if used_chars + 1 < value_width {
        buffer[used_chars] = '.';
        used_chars += 1;
        let after_radix = thousandths % 1000; // <1000
        buffer[used_chars] = char::from_digit(after_radix / 100, 10).unwrap();
        used_chars += 1;
        let remaining_chars = value_width - used_chars;
        if remaining_chars >= 1 {
            buffer[used_chars] = char::from_digit((after_radix / 10) % 10, 10).unwrap();
            used_chars += 1;
            if remaining_chars >= 2 {
                buffer[used_chars] = char::from_digit(after_radix % 10, 10).unwrap();
                used_chars += 1;
            }
        } 
    }

    // add the unit to the end of the buffer
    for char in unit.chars() {
        buffer[used_chars] = char;
        used_chars += 1;
    }

    used_chars
}

/// Stores info about a scope interseting a cell - used for subcell rendering.
#[derive(Copy, Clone)]
struct CellIntersection {
    /// Index of intersecting scope in the vector of scopes intersecting current line.
    scope_idx: usize, 
    /// Start of the scope intersection within the cell.
    start:     Promile,
    /// End of the scope intersection within the cell.
    end:       Promile,
}
impl CellIntersection { 
    const fn coverage(&self) -> Promile {
        Promile(self.end.0 - self.start.0)
    }
}

/// 'Rasterize' a cell into a 8-'pixel'(octant) line.
///
/// Works similarly to rasterization in computer graphics - we determine the scope intersecting the
/// center of each octant, and use its color.
///
/// * `state`:         state of the stream view widget, to determine scope colors.
/// * `scopes`:        all scopes intersecting current *line*, not just the current cell.
/// * `intersections`: info about all scopes intersecting current *cell*, including their indices
///                    pointing into `scopes`
/// Returns an array of 8 colors which we can then approximate by a glyph on the termnal.
fn rasterize_cell_into_octants(
    state:         &StreamViewState,
    scopes:        &[Scope],
    intersections: &[CellIntersection]) 
    -> [color::Wrapper; 8] 
{
    let mut octant_colors: [color::Wrapper; 8] = [color::Wrapper(BLACK); 8];
    let mut i = 0;
    for (o, octant_color) in octant_colors.iter_mut().enumerate() {
        // Just like rasterization in 3D graphics, this will result in some artifacts if there are
        // many short scopes within an octant.
        let octant_center = (125/2) + 125 * o as u64;
        // Find an intersection/scope, if any, that intersects the center of the octant.
        while i < intersections.len() && intersections[i].start.0 <= octant_center {
            if intersections[i].end.0 >= octant_center {
                let octant_scope     = &scopes[intersections[i].scope_idx];
                let (color, _ignore) = state.scope_color(octant_scope);
                *octant_color        = color::Wrapper(color);
                break;
            }

            i += 1;
        }
    }
    octant_colors
}


struct StatusbarState {
    rendered_scope_count:  usize,
    total_timepoint_count: u64,
    total_gap_count:       u64,
    render_time_millis:    u128,
    stream_view_stats:     StreamViewStats
}

impl StatusbarState {
    const fn new() -> Self {
        Self {
            rendered_scope_count:  0,
            total_timepoint_count: 0,
            total_gap_count:       0,
            render_time_millis:    0,
            stream_view_stats:     StreamViewStats::new()
        }
    }
}

/// Data used to render a track in `render_tracks()`
struct TrackRenderData {
    /// Stores visible scopes of the track being drawn.
    scopes:      Vec<Scope>,
    /// Data events in the visible part (a row of cells) of the current Track.
    data_events: Vec<(TrackDataEvent, bool)>,
    /// Stores generated data (e.g. label) of each scope in `scopes` when drawing a track.
    scope_data:  Vec<TransientScopeData>
}

impl TrackRenderData {
    fn new() -> Self {
        Self {
            scopes:      Vec::with_capacity(2000),
            data_events: Vec::with_capacity(2000),
            scope_data:  Vec::with_capacity(2000),
        }
    }

    fn clear(&mut self) {
        self.scopes.clear();
        self.data_events.clear();
        self.scope_data.clear();
    }
}

/// Widget used to view scopes in a stream.
pub struct StreamView {
    background_style: Style,
}

impl Default for StreamView {
    /// Default-initializes a `StreamView` with black background.
    fn default() -> Self {
        Self {
            background_style: Style::default().bg(Self::COLOR_BG)
        }
    }
}

impl StreamView {
    const COLOR_BG: Color = BLACK;
    const COLOR_GAP_MARKER: Color = Color::Rgb(48,48,96);
    const GAP_MARKER: &'static str = "·";
    /// Background color for the scope data/stats bar.
    const COLOR_SCOPEDATA_BG: Color = Color::Rgb(16,16,32);

    /// Width of the labels showing nest level at the left edge of each track.
    const NEST_LEVEL_LABEL_CELLS: u16 = 4;
    /// Generate (into `scope_data`) labels and any other transient data for visible `scopes` in a track.
    fn init_scope_data(state: &mut StreamViewState, scopes: &[Scope], scope_data: &mut Vec<TransientScopeData>) {
        let registry_unlocked = state.get_current_timepoint_registry();
        let registry = registry_unlocked.lock().unwrap();
        let gap_marker = Self::GAP_MARKER.chars().next().unwrap();
        for scope in scopes {
            let mut data  = TransientScopeData{ label: [gap_marker; 63], cells_drawn: 0 };
            let max_chars = cmp::min(data.label.len(), (scope.duration / state.time_per_cell).try_into().unwrap());
            let name = if scope.id.is_merged() {
                "…"
            } else {
                let meta = registry.get_meta( scope.id );
                match meta {
                    Some( m ) => &m.name,
                    None => "<?>"
                }
            };

             // 7 because: to draw time + name we need 5 + 1 + >=1 (time, ' ', name), and
             // we add 1 more since the scope start may not be aligned to a cell and 2
             // halves may be 'lost' this way at either end of the scope.
            let name_offset = if max_chars <= 7 || scope.id.is_merged() { 
                0
            } else {
                // duration
                let number_chars = write_fixed_width(scope.duration, 5, &mut data.label[..]);
                assert!(max_chars > 5, "write_fixed_width must fit inside specified width");
                data.label[number_chars] = ' ';
                number_chars + 1
            };
            // name
            for (i, char) in name.chars().enumerate() {
                if i + name_offset > max_chars {
                    break;
                }
                data.label[i + name_offset] = char;
            }
            scope_data.push( data );
        }
    }

    /// Draw a single cell of a track, which may have multiple `intersections` with scopes.
    ///
    /// May use multiple different methods to draw the cell, depending on how many and how large
    /// intersections there are:
    ///
    /// * if a single scope fully intersects the cell we try to render a character from the scope's
    ///   name or a 'gap marker' for a space.
    /// * if 1-2 scopes mostly or fully intersect the cell, we approximate their coverage of the
    ///   cell with a block element or similar unicode symbol.
    /// * if many scopes intersect the cell, we rasterize them into 8 octant colors, and find a
    ///   block element/other unicode symbol to best approximate the rasterized colors.
    ///
    /// Params:
    ///
    /// * `cell_window_end`: Time corresponding to the end/right end of the cell. 
    /// * `intersections`:   Intersections of the cell with scopes.
    /// * `cell`:            Cell to draw on.
    fn draw_cell(
        state:           &mut StreamViewState,
        cell_window_end: StreamTime,
        intersections:   &[CellIntersection],
        cell:            &mut Cell,
        render_data:     &mut TrackRenderData)
    {
        let draw_cell_2color = |cell: &mut Cell, invert: bool, data: bool, symbol: &str, color_1: Color, color_2: Color| {
            let style_base = Style::default()
                .bg(if invert{color_1} else {color_2})
                .fg(if invert{color_2} else {color_1});
            // If there's a data event on exectly the scope on this cell, highlight the cell.
            let style = if data {
                // style_base
                if let Some( Color::Rgb(r, g, b) ) = style_base.bg {
                    // Darken the color to make data events visible
                    style_base.add_modifier(Modifier::UNDERLINED)
                              .bg(Color::Rgb(r / 5 * 3, g / 5 * 3, b / 5 * 3))
                } else {
                    panic!("bg is RGB");
                }
            } else {
                style_base
            };
            cell.set_symbol(symbol)
                .set_style(style);
        };

        if intersections.is_empty() {
            state.cell_counters.gap += 1;
            draw_cell_2color(cell, false, false, Self::GAP_MARKER, Self::COLOR_GAP_MARKER, Self::COLOR_BG);
            return;
        }

        // Find the 2 scopes that intersect current cell the most.
        let mut max_intersection_indices: [usize; 2]   = [0, 0];
        let mut max_coverages:            [Promile; 2] = [Promile(0), Promile(0)];
        for (i, intersection) in intersections.iter().enumerate() {
            let coverage = intersection.coverage();
            if coverage.0 >= max_coverages[0].0 {
                max_coverages[1].0          = max_coverages[0].0;
                max_intersection_indices[1] = max_intersection_indices[0];
                max_coverages[0].0          = coverage.0;
                max_intersection_indices[0] = i;
            } else if coverage.0 >= max_coverages[1].0 {
                max_coverages[1].0          = coverage.0;
                max_intersection_indices[1] = i;
            }
        }
        let max_intersection = &intersections[max_intersection_indices[0]];
        let gap_marker = Self::GAP_MARKER.chars().next().unwrap();

        // one intersection taking up the entire cell - trivial to draw
        if max_coverages[0].0 >= 990 {
            let scope_idx         = max_intersection.scope_idx;
            let scope             = &render_data.scopes[scope_idx];
            let glyph             = render_data.scope_data[scope_idx].eat_char();
            let (color, fg_color) = state.scope_color(scope);
            let fg_color          = if glyph == gap_marker {Self::COLOR_GAP_MARKER} else {fg_color};
            let mut glyph_buffer  = [0u8; 4];
            state.cell_counters.full += 1;

            // Draw a marker indicating a data event?
            let draw_data_event = (||{
                let scope = render_data.scopes[scope_idx];
                // Find a data event in this scope that has no marker yet.
                for (data, was_drawn) in &mut render_data.data_events[scope.data_event_first .. scope.data_event_first + scope.data_event_count] { 
                    // Draws the data event only once for a multi-cell scope.
                    if !*was_drawn && data.time >= scope.start && data.time <= cell_window_end {
                        *was_drawn = true;
                        return true;
                    }
                }
                false
            })();

            draw_cell_2color(cell, false, draw_data_event, glyph.encode_utf8(&mut glyph_buffer), fg_color, color);
            return;
        } 

        // used to draw a cell with 2 intersections (or 1 intersection + lots of empty space)
        let draw_cell_2intersections = |cell: &mut Cell, intersection_1: &CellIntersection, color_1: Color, color_2: Color| {
            // necessary for end_octant to always be greater than start_octant
            assert!(intersection_1.coverage().0 >= 125,
                "the intersection to draw must cover at least 1/8th of the cell");
            // the +/- corrections ensure the octant is only drawn if more than 50% covered.
            // disadvantage: if we intersect 49% of both first and last octant, we lose 98% worth of
            // octants drawn.
            let start_octant = (intersection_1.start.0 + 125/2) / 125;
            let end_octant   = (intersection_1.end.0 - cmp::min(125/2, intersection_1.end.0)) / 125;
            let (symbol, invert) = subcells_symbol(start_octant.try_into().unwrap(), end_octant.try_into().unwrap());
            draw_cell_2color(cell, invert, false, symbol, color_1, color_2);
        };

        // one intersection taking up a part of the cell - use subcell drawing
        if intersections.len() == 1 && max_coverages[0].0 >= 125 {
            let scope_idx        = max_intersection.scope_idx;
            let scope            = &render_data.scopes[scope_idx];
            let (color, _ignore) = state.scope_color(scope);
            state.cell_counters.partial_1 += 1;
            draw_cell_2intersections(cell, max_intersection, color, Self::COLOR_BG);
            return;
        }

        if intersections.len() == 2 {
            let second_intersection = &intersections[max_intersection_indices[1]];
            let total_coverage = max_coverages[0].0 + second_intersection.coverage().0;
            // two intersections taking up the entire cell - use subcells
            if total_coverage >= 990 {
                assert!(total_coverage <= 1000, "cannot cover more than 1000‰ of a cell, ({}+{} aka ({},{})+({},{}) idx {},{})",
                    max_coverages[0].0, second_intersection.coverage().0,
                    max_intersection.start.0, max_intersection.end.0, second_intersection.start.0, second_intersection.end.0,
                    max_intersection.scope_idx, second_intersection.scope_idx);
                let scope_1            = &render_data.scopes[max_intersection.scope_idx];
                let scope_2            = &render_data.scopes[second_intersection.scope_idx];
                let (color_1, _ignore) = state.scope_color(scope_1);
                let (color_2, _ignore) = state.scope_color(scope_2);
                state.cell_counters.partial_2 += 1;
                draw_cell_2intersections(cell, max_intersection, color_1, color_2);
                return;
            }
            // two different scopes not taking up the entire cell - fall back to rasterization
        }

        // everything else failed - try rasterization.
        let octant_colors: [color::Wrapper; 8] = rasterize_cell_into_octants(state, &render_data.scopes[..], intersections);
        let (color_fg, color_bg) = top2_octant_colors(octant_colors);
        let (symbol, invert)     = subcells_symbol_match_octants(color_fg, color_bg, &octant_colors);
        state.cell_counters.rasterized += 1;
        draw_cell_2color(cell, invert, false, symbol, color_fg, color_bg);
    }

    /// Get an array used as a table mapping index (nest_level-NestLevel::MIN) to row to draw that nest level at.
    ///
    /// Used to skip drawing empty rows for tracks/nest levels with no scopes.
    fn map_nest_levels_to_rows(nestlevel_0_row: i32, state: &mut StreamViewState) -> [i32; 256]
    {
        let mut level_to_track_row: [i32; 256] = [i32::MIN; 256];
        let mut row = nestlevel_0_row;
        let nest_level_min = NestLevel::MIN.0 as isize;
        let stream         = state.get_current_stream();
        let locked_stream  = stream.lock().unwrap();
        for level_up in 0..=NestLevel::MAX.0 as isize {
            if locked_stream.is_track_empty(NestLevel(level_up.try_into().unwrap())) {
                continue;
            }
            level_to_track_row[usize::try_from(level_up - nest_level_min).unwrap()] = row;
            row-=1;
        }
        row = nestlevel_0_row + 1;
        for level_down in (nest_level_min..=-1).rev() {
            if locked_stream.is_track_empty(NestLevel(level_down.try_into().unwrap())) {
                continue;
            }
            level_to_track_row[usize::try_from(level_down - nest_level_min).unwrap()] = row;
            row+=1;
        }
        level_to_track_row
    }

    /// Render the label showing nest level at the left edge of the track at `track_row`.
    fn render_level_label(area: Rect, buf: &mut Buffer, level: isize, track_row: u16) {
        let level_label = label_right(format!("{}", level), Color::White);
        let level_area  = Rect::new(area.x, track_row, Self::NEST_LEVEL_LABEL_CELLS, 1);
        level_label.render(level_area, buf);
    }

    /// Render tracks with all scopes currently visible in the view.
    fn render_tracks(area: Rect, buf: &mut Buffer, state: &mut StreamViewState) -> usize {
        // Clear selection if we had a click into the current view. If this click doesn't fall on
        // any scope, this will result in a deselect, otherwise it will select that scope in
        // `update_selection()`.
        if let Some((x, y)) = state.select_coords {
            if x >= area.left() && x <= area.right() && y >= area.top() && y <= area.bottom() {
                state.selected_scope        = None;
                state.selected_scope_stream = None;
            }
        }

        let mut render_data = TrackRenderData::new();
        // Stores info about scopes intersecting with currently drawn cell.
        let mut intersections: Vec<CellIntersection> = vec![];
        intersections.reserve(SUBCELLS_PER_CELL + 1);
        let nestlevel_0_row = state.correct_pan_vertical(area);
        // Maps nest levels to rows they should be drawn on, indexed by nest_level - NestLevel::MIN.0
        let level_to_track_row = Self::map_nest_levels_to_rows(nestlevel_0_row, state);

        let mut scope_count = 0;

        let nest_level_min = NestLevel::MIN.0 as isize;
        // Draw tracks of individual nest levels.
        for level in nest_level_min..=NestLevel::MAX.0 as isize {
            // Signed because the track row may be moved anywhere by panning, even outside of the screen.
            let track_row_signed = level_to_track_row[usize::try_from(level - nest_level_min).unwrap()];
            if track_row_signed < 0 || track_row_signed < i32::from(area.top()) || track_row_signed >= i32::from(area.bottom()) {
                continue;
            }

            let track_row = u16::try_from(track_row_signed).unwrap();

            // Scopes with durations lower than this are too small to be renderable.
            let time_resolution = state.time_per_cell / (SUBCELLS_PER_CELL as u64);

            render_data.clear();
            // Get currently visible scopes into `scopes`.
            state.get_current_stream().lock().unwrap().track_scope_view(
                &mut render_data.scopes,
                &mut render_data.data_events,
                NestLevel(level.try_into().unwrap()),
                state.time_window_start(),
                state.time_window_end(),
                time_resolution );
            scope_count += render_data.scopes.len();

            if render_data.scopes.is_empty() {
                continue;
            }

            Self::init_scope_data(state, &render_data.scopes[..], &mut render_data.scope_data);
            // Index of the current scope being drawn.
            let mut scope_idx = 0;
            // Skip any scopes that end before the time window (and therefore the first column) starts.
            while scope_idx < render_data.scopes.len() && render_data.scopes[scope_idx].end() < state.time_window_start() {
                scope_idx += 1;
            }

            Self::render_level_label(area, buf, level, track_row);
            // Draw visible cells of the track.
            for cell in 0 ..area.width {
                intersections.clear();
                let cell_window_start = state.time_window_start() + state.time_per_cell * u64::from(cell);
                let cell_window_end   = cell_window_start + state.time_per_cell;

                // get scopes intersecting the cell into `intersections`
                while scope_idx < render_data.scopes.len() && render_data.scopes[scope_idx].start < cell_window_end {
                    let scope            = &render_data.scopes[scope_idx];
                    let intersect_start  = cmp::max(cell_window_start, scope.start);
                    let intersect_end    = cmp::min(cell_window_end, scope.end());
                    let start_promile    = Promile((intersect_start - cell_window_start) * 1000 / state.time_per_cell);
                    let end_promile      = Promile((intersect_end   - cell_window_start) * 1000 / state.time_per_cell);
                    intersections.push(CellIntersection{
                        scope_idx,
                        start:     start_promile,
                        end:       end_promile
                    });

                    // Reached scope interseting the end of the cell.
                    if scope.end() >= cell_window_end {
                        // The scope continues into the next column - do not increase
                        // scopeIdx as it will be needed for the next column.
                        break;
                    } 
                    scope_idx += 1;
                }
                assert!(intersections.len() <= SUBCELLS_PER_CELL + 1, 
                    "since we're viewing scopes at resulution state.time_per_cell / SUBCELLS_PER_CELL, at most SUBCELLS_PER_CELL + 1 scopes can intersect with a cell. Got {}", intersections.len());

                let col = area.x + cell;
                if intersections.len() == 1 {
                    state.update_selection(col, track_row, &render_data.scopes[intersections[0].scope_idx]);
                }
                if col >= Self::NEST_LEVEL_LABEL_CELLS
                {
                    Self::draw_cell(
                        state,
                        cell_window_end,
                        &intersections[..],
                        buf.get_mut(col, track_row),
                        &mut render_data);
                }
            }
        }

        scope_count
    }

    /// Create a label rendering the total time taken by instances of scope with stats `stats`
    fn create_total_duration_label<'a>(stats: &ScopeStats, profiling_time: StreamTime) -> Paragraph<'a>{
        let total_duration = stats.duration_mean * stats.instance_count;
        let total_percent  = {
            if !profiling_time.is_zero() { 
                total_duration.raw() / (profiling_time.raw() / 100)
            } else {
                100
            }
        };
        let (unit, thousandths) = to_readable_time_unit(total_duration);
        label_right(format!("total: {}%/{}.{}{}", total_percent, thousandths / 1000, thousandths % 1000, unit), YELLOW)
    }

    /// Render the histogram/visual stats part of the scope data bottom pane. Includes non-percentile stats like max/min.
    fn render_scope_data_histogram(area: Rect, buf: &mut Buffer, state: &mut StreamViewState, scope_color: Color, stats: ScopeStats, profiling_time: StreamTime) {
        // Layout of the percentile/histogram view.
        let chunks_percentiles = Layout::default()
            .direction(Direction::Horizontal)
            .margin(0)
            .constraints(
                [
                    Constraint::Length(1), // right margin
                    Constraint::Length(6), // margin for stat
                    Constraint::Length(8), // stat name
                    Constraint::Length(9), // stat value
                    Constraint::Length(18),// CV and other secondary stats
                    Constraint::Min(3)     // the rest
                ]
                .as_ref(),
            )
            .split(area);

        // Left part with the percentile/stat names.
        let left  = chunks_percentiles[1];
        // Middle part with the percentile/stat values as numbers.
        let mid_left  = chunks_percentiles[2];
        let mid_right = chunks_percentiles[3];
        // Secondary stats such as coefficient of variation
        let right = chunks_percentiles[4];

        // Draw the percentile/stat bars - main body of the scope data view.

        // Function to draw name & value of a percentile/stat.
        let draw_timestat = |buf: &mut Buffer, name: &str, value: StreamTime, row: u16| {
            let name_label = label_left(name, Color::White);
            let (unit, thousandths) = to_readable_time_unit( value );
            let value_label =
                label_right(format!("{}.{}{}", thousandths / 1000, thousandths % 1000, unit), Color::White);
            name_label .render(Rect::new(mid_left.x,  mid_left.y + row,  mid_left.width,  1), buf);
            value_label.render(Rect::new(mid_right.x, mid_right.y + row, mid_right.width, 1), buf);
        };

        // Function to draw a visual bar representing the value of a percentile/stat.
        let draw_timestat_bar = |buf: &mut Buffer, is_percentile: bool, value: StreamTime, row: u16| {
            // Remaining 'part of the value' to draw.
            let mut value_remaining = value;
            // -2 for 1col margin on both right and left.
            let max_width = area.width - 2;

            // percentiles are drawn brighter, non-percentile stats dimmer, to differentiate
            let color = if is_percentile {
                scope_color
            } else if let Color::Rgb(r,g,b) = scope_color {
                Color::Rgb(r / 5 * 3, g / 5 * 3, b / 5 * 3)
            } else {
                // scope color not RGB - can't do anything smart
                Color::White
            };
            let style = Style::default().bg(color).fg(BLACK).add_modifier(Modifier::UNDERLINED);
            // go through the line cell by cell, and draw the time statistic using time_per_cell as unit
            for col in 0 .. max_width {
                let x = col + left.x;
                // last cell of the value
                if value_remaining < state.time_per_cell {
                    // use subcells to get a bit of extra precision, but only if there's no
                    // character in the cell already, so we don't overdraw any stat names/numbers.
                    if buf.get_mut(x, area.y + row).symbol != " " {
                        break;
                    }
                    let octants_full = u8::try_from(value_remaining * 8 / (state.time_per_cell)).unwrap();
                    if octants_full > 0 {
                        let (symbol, invert) = subcells_symbol(0, octants_full - 1);
                        assert!(!invert, "inverting should never be needed when starting at octant 0");
                        buf.get_mut(x, area.y + row)
                           .set_symbol(symbol)
                           .set_style(style.bg(Self::COLOR_SCOPEDATA_BG).fg(color));
                    }

                    break;
                }
                value_remaining -= state.time_per_cell;

                // draw the bar by just setting the style - keep any existing character in the cell
                // to draw the bar over the numeric value
                buf.get_mut(x, area.y + row)
                   .set_style(style);
                // if we don't have enough space to draw the full value, end the bar by a red arrow
                if col == max_width - 1 && value_remaining >= state.time_per_cell {
                    buf.get_mut(x, area.y + row)
                       .set_style(style.fg(RED))
                       .set_symbol("⯈");
                }
            }
        };

        let mut timestat_row = 0;
        let mut draw_timestat_and_bar = |buf: &mut Buffer, name: &str, is_percentile: bool, value: StreamTime, count: u64| {
            draw_timestat(buf, name, value, timestat_row);
            if is_percentile && !profiling_time.is_zero() {
                let freq_label = label_right(Self::freq_str(count, profiling_time), Color::White);
                freq_label.render(Rect::new(right.x, right.y + timestat_row, right.width, 1), buf);
            }
            draw_timestat_bar(buf, is_percentile, value, timestat_row);
            timestat_row += 1;
        };

        draw_timestat_and_bar(buf, "max",     false, stats.duration_max,             0);
        draw_timestat_and_bar(buf, "pct99.9", true,  stats.duration_percentile_99_9, stats.count_percentile_99_9);
        draw_timestat_and_bar(buf, "pct99",   true,  stats.duration_percentile_99,   stats.count_percentile_99);
        draw_timestat_and_bar(buf, "pct90",   true,  stats.duration_percentile_90,   stats.count_percentile_90);
        draw_timestat_and_bar(buf, "pct75",   true,  stats.duration_percentile_75,   stats.count_percentile_75);
        draw_timestat_and_bar(buf, "median",  true,  stats.duration_median,          stats.count_median);
        draw_timestat_and_bar(buf, "pct25",   true,  stats.duration_percentile_25,   stats.count_percentile_25);
        draw_timestat_and_bar(buf, "min",     false, stats.duration_min,             0);
        draw_timestat_and_bar(buf, "mean",    false, stats.duration_mean,            0);
        draw_timestat_and_bar(buf, "σ/CV",    false, stats.duration_stdev,           0);

        Self::render_coefficient_of_variation(Rect::new(right.x, right.y + 9, right.width, 1), buf, &stats);
    }

    #[allow(clippy::cast_precision_loss)]      // If the ratio between stddev and mean is so large that it loses precision, it's not useful anyway.
    #[allow(clippy::cast_sign_loss)]           // The color calculations here are purely unsigned.
    #[allow(clippy::cast_possible_truncation)] // We do truncate, and that's what we need here.
    fn render_coefficient_of_variation(area: Rect, buf: &mut Buffer, stats: &ScopeStats) {
        // any variance over this value is 'bad' - rendered as fully red
        const BAD_CV: f64 = 4.0;

        let coefficient_of_variation = ((stats.duration_stdev * 1000) / stats.duration_mean) as f64 / 1000.0;
        let badness      = coefficient_of_variation / BAD_CV;
        let badness_byte = f64::min(badness * 255.0, 255.0);
        let red_base     = badness_byte;
        let green_base   = 255.0 - badness_byte;
        // correct up to keep full brightness (this is far from proportional... doing this in HSB would be better)
        let correction   = 255.0 / f64::max(red_base, green_base);
        let color        = Color::Rgb((red_base * correction) as u8, (green_base * correction) as u8, 0);
        let cv_string    = format!("/{:.2}", coefficient_of_variation);
        let cv_len       = u16::try_from(cv_string.len()).unwrap();
        let cv_label     =
            Paragraph::new(cv_string)
                .style(Style::default().fg(color).add_modifier(Modifier::BOLD))
                .alignment(Alignment::Left);
        // drawing with exactly cv_len to avoid artifacts due to changing style of empty cells.
        cv_label.render(Rect::new(area.x, area.y, cv_len, 1), buf);
    }

    fn freq_str(instance_count: u64, time: StreamTime) -> String {
        const NANOSECONDS_IN_SECOND: u64 = 1000 * 1000 * 1000;
        let count_normalized = instance_count * NANOSECONDS_IN_SECOND;
        let time_ns          = time.as_nanoseconds();
        if count_normalized >= time_ns {
            if time_ns > 0 {format!("{}inst/s", count_normalized / time_ns)} else {String::from("∞inst/s")}
        } else if count_normalized > 0 {
            format!("{}s/inst", time_ns / count_normalized)
        } else {
            String::from("∞s/inst")
        }
    }

    /// Render a bottom pane with detailed information about selected scope.
    ///
    /// * area:  area of the bottom pane
    /// * scope: selected scope
    fn render_scope_data(area: Rect, buf: &mut Buffer, state: &mut StreamViewState, scope: Scope) {
        // Get data/stats about the scope from ID registry.
        let (id, name, level, stats) = {
            let registry_unlocked = state.get_current_timepoint_registry();
            let registry          = registry_unlocked.lock().unwrap();
            let meta              = registry.get_meta( scope.id );
            match meta {
                Some(m) => (m.id, m.name.clone(), m.nest_level, m.get_scope_stats()),
                None    => (scope.id, String::from("<?>"), NestLevel::MAX, ScopeStats::default())
            }
        };

        // Main layout of the bottom pane.
        let chunks_main = Layout::default()
            .direction(Direction::Vertical)
            .margin(0)
            .constraints(
                [
                    Constraint::Length(1),  // top line with scope info and some stats
                    Constraint::Length(1),  // second line for data events
                    Constraint::Min(8),     // main part with histogram data
                ]
                .as_ref(),
            )
            .split(area);

        let area_top_line   = chunks_main[0];
        let data_event_line = chunks_main[1];
        let area_histogram  = chunks_main[2];
        let chunks_top_line = Layout::default()
            .direction(Direction::Horizontal)
            .margin(0)
            .constraints(
                [
                    Constraint::Length(1),  // right margin
                    Constraint::Length(32), // scope name
                    Constraint::Length(10), // scope duration
                    Constraint::Length(16), // scope position
                    Constraint::Length(30), // instances
                    Constraint::Length(22), // total duration/percent
                    Constraint::Length(12), // nest level
                    Constraint::Min(3),     // the rest
                ]
                .as_ref(),
            )
            .split(area_top_line);

        // Draw a background to visually separate the bottom pane from the main scope view.
        let background = Block::default().style(Style::default().bg(Self::COLOR_SCOPEDATA_BG));
        background.render(area, buf);

        // Color to draw the scope name/histogram with.
        let (scope_color, _ignore) = state.scope_color_unselected(&scope);

        // Create and render contents of the top line; name, duration, etc.
        let name_label = label_left(format!("{}/{}", id.0, name), scope_color);
        let duration_label  = {
            let (unit, thousandths) = to_readable_time_unit(scope.duration);
            // Need to draw infinite duration as infinite instead of u64::MAX picoseconds
            let text = if scope.duration.is_inf() {
                String::from("∞")
            } else {
                format!("{}.{}{}", thousandths / 1000, thousandths % 1000, unit)
            };
            Paragraph::new(text)
                .style(Style::default().fg(YELLOW).add_modifier(Modifier::BOLD))
                .alignment(Alignment::Right)
        };
        let position_label  = {
            let (unit, _thousandths) = to_readable_time_unit(state.time_per_cell);
            let position = i128::try_from(scope.start.as_picoseconds()).unwrap() -
                           i128::try_from(state.earliest_update_time.as_picoseconds()).unwrap();
            label_right(format!("@{}{}", to_time_unit( position, unit ), unit), Color::Green)
        };

        let profiling_time  = {
            let stream_unlocked = state.get_current_stream();
            let mut stream_locked = stream_unlocked.lock().unwrap();
            // Calling here since to avoid extra lock on stream.
            Self::render_data_events( buf, state, stream_locked.get_scope_data_events( scope, level ), data_event_line );
            stream_locked.total_profiling_duration()
        };
        let instances_label = {
            label_right(format!("instances: {}, {}", stats.instance_count, Self::freq_str(stats.instance_count, profiling_time)), Color::Green)
        };
        let total_label     = Self::create_total_duration_label(&stats, profiling_time);
        let level_label     = label_right(format!("level: {}", level.0), Color::Green);

        name_label     .render(chunks_top_line[1], buf);
        duration_label .render(chunks_top_line[2], buf);
        position_label .render(chunks_top_line[3], buf);
        instances_label.render(chunks_top_line[4], buf);
        total_label    .render(chunks_top_line[5], buf);
        level_label    .render(chunks_top_line[6], buf);

        // If there are no instances of this scope there's no percentile/statistical data to draw.
        if stats.instance_count > 0 {
            Self::render_scope_data_histogram(area_histogram, buf, state, scope_color, stats, profiling_time);
        }
    }

    /// Render the line with information about data events in currently selected scope.
    ///
    /// Params:
    ///
    /// * `buf`:             TUI 'framebuffer' to render into.
    /// * `state`            State of the stream view widget.
    /// * `data_events`      Data events in currently selected scope.
    /// * `data_event_line`: Area in `buf` to render the line in.
    fn render_data_events(buf:             &mut Buffer,
                          state:           &mut StreamViewState,
                          data_events:     &[TrackDataEvent],
                          data_event_line: Rect)
    {
        if data_events.is_empty() {
            return
        }
        let registry_unlocked = state.get_current_timepoint_registry();
        let registry          = registry_unlocked.lock().unwrap();

        let data_str = data_events.iter()
            // TODO print stream time as well, but in a readable way - either scaled based on
            // zoom level or relative to scope (may even be percentage of the scope)
            .map(|e|{ let meta = registry.get_meta_data_event( &e.data );
                match meta {
                    Some(m) => format!(" {}: {}", m.name, e.data.value_str()),
                    None    => "<UNREGISTERED DATA EVENT>".to_string()
                }})
            .collect::<String>();

        let data_label = label_left(data_str, Color::Yellow);
        data_label.render(data_event_line, buf);
    }

    fn render_statusbar(area:  Rect,
                        buf:   &mut Buffer,
                        state: &mut StreamViewState,
                        bar:   &StatusbarState)
    {
        // Statusbar layout.
        let chunks_status = Layout::default()
            .direction(Direction::Horizontal)
            .margin(0)
            .constraints(
                [
                    Constraint::Length(12), // stream ID/count
                    Constraint::Length(10), // auto vs manual panning
                    Constraint::Length(8),  // resolution legend
                    Constraint::Length(36), // panning position
                    Constraint::Min(10),    // center gap
                    Constraint::Length(14), // frame time
                    Constraint::Length(34), // visible/total scope count
                    Constraint::Length(44), // stream view stats
                ]
                .as_ref(),
            )
            .split(area);

        let (tpc_unit, tpc_thousandths) = to_readable_time_unit(state.time_per_cell);

        // Statusbar text.
        let stream_status = {
            let stream_count = state.stream_count();
            label_left(format!("stream {}/{}", state.current_stream_id.0 + 1, stream_count), Color::Green)
        };
        let pan_status    = {
            let (pan_str, pan_color) = if state.auto_pan { ("auto", Color::Red) } else { ("manual",Color::Yellow) };
            label_left(format!("pan:{}", pan_str), pan_color)
        };
        let legend        = label_right(format!("{}{}/█", tpc_thousandths / 1000, tpc_unit), Color::Green);
        let window_end    = {
            let position = i128::try_from(state.time_window_center.as_picoseconds()).unwrap() -
                           i128::try_from(state.earliest_update_time.as_picoseconds()).unwrap();
            let stream_unlocked = state.get_current_stream();
            let profiling_time = i128::try_from(stream_unlocked.lock().unwrap().total_profiling_duration().as_picoseconds()).unwrap();
            label_right(format!("@{}/{}{}", to_time_unit(position, tpc_unit),
                        to_time_unit(profiling_time, tpc_unit), tpc_unit), Color::Green)
        };

        stream_status.render(chunks_status[0], buf);
        pan_status.render(   chunks_status[1], buf);
        legend.render(       chunks_status[2], buf);
        window_end.render(   chunks_status[3], buf);

        if state.debug_mode {
            let drawcells   = label_right(state.cell_counters.as_string(), Color::Green);
            let frametime   = label_right(format!("{}ms/frame", bar.render_time_millis), Color::Green);
            let scope_count = label_right(format!("{}sc/{}tp/{}gaps", bar.rendered_scope_count, bar.total_timepoint_count, bar.total_gap_count), Color::Green);
            let view_stats  = label_right(bar.stream_view_stats.to_string(), Color::White);
            drawcells.render(    chunks_status[4], buf);
            frametime.render(    chunks_status[5], buf);
            scope_count.render(  chunks_status[6], buf);
            view_stats.render(   chunks_status[7], buf);
            state.cell_counters.clear();
        }
    }
}

impl StatefulWidget for StreamView {
    type State = StreamViewState;

    /// Main render function
    fn render(self, area: Rect, buf: &mut Buffer, state: &mut Self::State) {
        // Time the render for perf diagnostics.
        let render_start_time = time::Instant::now();
        state.time_window_cells = area.width;
        state.update_focus(area);

        // Draw the line in the center of the screen.
        let center_col = state.half_cells_left();
        for row in area.top()..area.bottom() {
            buf.get_mut(center_col, row)
               .set_symbol("│")
               .set_style( Style::default().fg(gray(64)) );
        }

        let show_scope_data = if let Some(_scope) = state.selected_scope {
            // if we're showing views for all streams, only show scope data if viewing stream containing
            // selected scope.
            !state.showing_all() || state.current_stream_id.0 == state.selected_scope_stream.unwrap().0
        } else {
            false
        };
        // if we have a selected scope, draw a scope data bottom pane with this heaight
        let scope_data_height = if show_scope_data {12} else {0};
        // Main layout.
        let chunks = Layout::default()
            .direction(Direction::Vertical)
            .margin(0)
            .constraints(
                [
                    Constraint::Min(3),
                    Constraint::Length(scope_data_height), // scope data bar, if any
                    Constraint::Length(1), // statusbar
                ]
                .as_ref(),
            )
            .split(area);
        // Layout for the area that renders the scopes themselves.
        // All of this area is used to render scopes, but some chunks may be used to draw data on
        // top or below scopes (e.g. the help info bar).
        let chunks_scopes = Layout::default()
            .direction(Direction::Vertical)
            .margin(0)
            .constraints(
                [
                    Constraint::Length(1), // help bar
                    Constraint::Min(2),
                ]
                .as_ref(),
            )
            .split(chunks[0]);

        // If we're showing all streams, only render the help bar once.
        if !state.showing_all() || state.current_stream_id.0 == 0 {
            // Help bar at the top of the view. Can get obscured by scopes.
            let help_info =
                label("(shift-)tab:change stream, <space>:pause, (ctrl-)j/k/up/down/wheel:zoom, (ctrl-)h/l/left/right/w/s/pgup/pgdn:move, left-click:stats",
                      Color::White, Alignment::Center);
            help_info.render(chunks_scopes[0], buf);
        }

        buf.set_style(area, self.background_style);
        let mut bar = StatusbarState::new();
        {
            // Update time for auto-panning to most recent data, and total scope count for stats.
            let stream = state.get_current_stream();
            let mut locked_stream = stream.lock().unwrap();
            if let Some(time) = locked_stream.earliest_update_time() {
                state.earliest_update_time = cmp::min(state.earliest_update_time, time);
            }

            bar.total_timepoint_count = locked_stream.timepoint_count();
            bar.total_gap_count       = locked_stream.gap_count(); 
            bar.stream_view_stats     = locked_stream.get_and_reset_view_stats();
        }

        // Only draw tracks if there is any data.
        bar.rendered_scope_count = if bar.total_timepoint_count > 0 {
            state.auto_pan_update();
            if area.height >= 1 {
                // correct pan to prevent scope data bar from vertically moving the viewed tracks
                state.vertical_pan += i32::try_from(scope_data_height).unwrap() / 2;
                let count = Self::render_tracks(chunks[0], buf, state);
                state.vertical_pan -= i32::try_from(scope_data_height).unwrap() / 2;
                count
            } else {
                0
            }
        } else {
            0
        };

        if let Some(scope) = state.selected_scope {
            // need to check if this is true because selection could have changed this frame
            if scope_data_height != 0 {
                Self::render_scope_data(chunks[1], buf, state, scope);
            }
        }

        bar.render_time_millis = render_start_time.elapsed().as_millis();
        Self::render_statusbar(chunks[2], buf, state, &bar);
    }
}

impl Widget for StreamView {
    fn render(self, _area: Rect, _buf: &mut Buffer) {
        unreachable!("StreamView can only be used in a stateful manner");
    }
}
