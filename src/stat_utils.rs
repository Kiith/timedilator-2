use humansize::{FileSize, file_size_opts::BINARY as BIN, file_size_opts::DECIMAL as DEC};


/// Format a non-byte-size value with an SI suffix assuming decimal (1000) scaling.
pub fn dec(stat: usize) -> String {
    // This is a hack, `humansize` is for filesizes and always adds 'B' to the end, but we can use
    // it like this for general case.
    let mut bytes = stat.file_size(DEC).unwrap();
    bytes.pop();
    bytes
}

/// Format a byte-size value with an SI suffix assuming binary (1024) scaling.
pub fn bin(stat: usize) -> String {
    stat.file_size(BIN).unwrap()
}

