// generates useless advice so allow this
#![allow(clippy::comparison_chain)]
#![warn(clippy::nursery)]
#![warn(clippy::pedantic)]
// recommends to 'simplify' code into equivalently complex code
#![allow(clippy::option_if_let_else)]
// doesn't make sense without splitting some modules first
#![allow(clippy::module_name_repetitions)]
// recommends putting less common case first just to get rid of a negation.
#![allow(clippy::if_not_else)]
// recommends making long if let/else pairs less readable
#![allow(clippy::option_if_let_else)]
// too much useless advice, e.g. `bg/fg` being too similar
#![allow(clippy::similar_names)]
// purely stylistic and not in a good way
#![allow(clippy::unseparated_literal_suffix)]
// actual unicode glyph is more readable than its codepoint value
#![allow(clippy::non_ascii_literal)]

use std::{
    io,
    sync::mpsc,
    thread,
    time::{Duration, Instant},
    convert::TryFrom
};
use crossterm::{
    event::{self, Event as CEvent, KeyCode, KeyModifiers, MouseEventKind, MouseButton, EnableMouseCapture, DisableMouseCapture},
    terminal::{disable_raw_mode, enable_raw_mode},
    style::{SetBackgroundColor},
};
use tui::{
    backend::CrosstermBackend,
    layout::{Constraint, Direction, Layout},
    style::{Color, Modifier, Style},
    text::{Span, Spans},
    widgets::{Tabs},
};
use clap::{
    app_from_crate,
    crate_name,
    crate_version,
    crate_authors,
    crate_description,
    App,
    AppSettings,
    Arg,
};
extern crate log;

mod stream_reader;
mod stream_view;
mod stream;
mod color;
mod integer_storage;
mod stat_utils;
mod event_id;
use stream_view::{
    StreamView,
    StreamViewState
};


enum Event {
    Input(CEvent),
    Tick,
}

#[derive(Copy, Clone, Debug)]
enum MenuItem {
    StreamView,
    StreamViewAll,
    Log,
}

impl From<MenuItem> for usize {
    fn from(input: MenuItem) -> Self {
        match input {
            MenuItem::StreamView    => 0,
            MenuItem::StreamViewAll => 1,
            MenuItem::Log           => 2,
        }
    }
}


/// Process CLI input and return stream paths, framerate and thread count.
fn handle_cli() -> Result<(Vec<String>, u64, usize), Box<dyn std::error::Error>> {
    // CLI arg parsing
    let matches = app_from_crate!()
        .setting(AppSettings::SubcommandRequiredElseHelp) // --help if incorrect subcommand
        .subcommand(
            App::new("view")
                .about("view nanoprof streams; either pre-recorded or in real time as they're being written by profiled program")
                .arg( Arg::with_name("paths")
                    .index(1)
                    .multiple(true)
                    .required(true)
                    .help("paths to .nanoprof stream files to view"))
                .arg( Arg::with_name("framerate")
                    .short("f")
                    .long("framerate")
                    .value_name("FPS")
                    .takes_value(true)
                    .default_value("24")
                    .help("rendering rate in redraws per second; decrease for huge inputs / slow SSH connections"))
                .arg( Arg::with_name("reader-thread-count")
                    .short("t")
                    .long("reader-thread-count")
                    .value_name("THREADS")
                    .takes_value(true)
                    .default_value("1")
                    .help("read nanoprof streams will be evenly divided amont THREADS reader threads")),
        )
        .get_matches();
    // TODO refactor this when we have multiple subcommands
    if let Some(view) = matches.subcommand_matches("view") {
        let paths: Vec<String> = view.values_of("paths").unwrap_or_default().map(String::from).collect();
        let framerate: u64 = match view.value_of("framerate").unwrap().parse() {
            Ok(rate) => {
                if rate == 0 {
                    eprintln!("Invalid --framerate/-f value: 0");
                    return Err(Box::new(clap::Error::with_description("Invalid --framerate/-f value: 0", clap::ErrorKind::InvalidValue)));
                }
                rate
            }
            Err(why) => {
                eprintln!("Invalid --framerate/-f value: {why}");
                return Err(Box::new(why));
            }
        };
        let reader_thread_count: usize = match view.value_of("reader-thread-count").unwrap().parse() {
            Ok(rate) => rate,
            Err(why) => {
                eprintln!("Invalid --reader-thread-count/-t value: {why}");
                return Err(Box::new(why));
            }
        };
        Ok((paths, framerate, reader_thread_count))
    } else {
        unreachable!("seems like clap prevents this");
    }
}

/// Create the tabs widget to draw on top of the screen.
fn init_tabs<'a>(menu_titles: &'a[&str], active_menu_item: MenuItem) -> Tabs<'a> {
    // Prepare spans for the tab bar
    let tab_spans = menu_titles
        .iter()
        .map(|t| {
            // first char is styled differently, to indicate the char to type to enter each tab
            if *t == "Log" {
                let (rest, hint) = t.split_at(2);
                Spans::from(vec![
                    Span::styled(rest, Style::default().bg(Color::Rgb(0,0,0)).fg(Color::White)),
                    Span::styled(hint,Style::default().fg(Color::Yellow).add_modifier(Modifier::UNDERLINED)),
                ])
            } else {
                let (hint, rest) = t.split_at(1);
                Spans::from(vec![
                    Span::styled(hint, Style::default().fg(Color::Yellow).add_modifier(Modifier::UNDERLINED)),
                    Span::styled(rest, Style::default().bg(Color::Rgb(0,0,0)).fg(Color::White)),
                ])
            }
        })
        .collect();
    Tabs::new(tab_spans)
        // make the active menu item 'selected', so it gets highlighted
        .select(active_menu_item.into())
        .style(Style::default().bg(Color::Rgb(0,0,0)).fg(Color::White))
        .highlight_style(Style::default().fg(Color::Yellow))
        .divider(Span::raw("|"))
}

/// Initialize the logging widget used as a separate tab.
fn init_log_widget(log_state: &mut tui_logger::TuiWidgetState) -> tui_logger::TuiLoggerSmartWidget {
    tui_logger::TuiLoggerSmartWidget::default()
        .style      (Style::default().bg(Color::Rgb(0,0,0)))
        .style_error(Style::default().fg(Color::Red))
        .style_debug(Style::default().fg(Color::Green))
        .style_warn (Style::default().fg(Color::Yellow))
        .style_trace(Style::default().fg(Color::Magenta))
        .style_info (Style::default().fg(Color::Cyan))
        .state(log_state)
}

/// Tell all threads to log any debugging stats they can right now.
fn log_debug_stats(stream_view_state: &mut StreamViewState) {
    for reader in &mut stream_view_state.stream_readers {
        reader.log_debug_stats();
    }
}

/// Input handling for the log widget.
fn process_logger_input_event(
    event: &CEvent,
    log_state: &mut tui_logger::TuiWidgetState,
    stream_view_state: &mut StreamViewState) -> bool
{
    if let CEvent::Key(key) = event {
        match key.code {
            KeyCode::Char(' ') => log_state.transition(&tui_logger::TuiWidgetEvent::SpaceKey),
            KeyCode::Esc       => log_state.transition(&tui_logger::TuiWidgetEvent::EscapeKey),
            KeyCode::PageUp    => log_state.transition(&tui_logger::TuiWidgetEvent::PrevPageKey),
            KeyCode::PageDown  => log_state.transition(&tui_logger::TuiWidgetEvent::NextPageKey),
            KeyCode::Up        => log_state.transition(&tui_logger::TuiWidgetEvent::UpKey),
            KeyCode::Down      => log_state.transition(&tui_logger::TuiWidgetEvent::DownKey),
            KeyCode::Left      => log_state.transition(&tui_logger::TuiWidgetEvent::LeftKey),
            KeyCode::Right     => log_state.transition(&tui_logger::TuiWidgetEvent::RightKey),
            KeyCode::Char('+') => log_state.transition(&tui_logger::TuiWidgetEvent::PlusKey),
            KeyCode::Char('-') => log_state.transition(&tui_logger::TuiWidgetEvent::MinusKey),
            KeyCode::Char('F') => log_state.transition(&tui_logger::TuiWidgetEvent::HideKey),
            KeyCode::Char('f') => log_state.transition(&tui_logger::TuiWidgetEvent::FocusKey),
            KeyCode::Char('D') => log_debug_stats(stream_view_state),
            _                  => return false,
        }
    }
    true
}

// Input handling for the stream view.
fn process_stream_view_input_event(event: &CEvent, stream_view_state: &mut StreamViewState, all: bool) -> bool {
    match event {
        CEvent::Key(key) => {
            let ctrl = key.modifiers.contains(KeyModifiers::CONTROL);
            match key.code {
                KeyCode::Tab       => if !all {stream_view_state.command_next_stream();},
                KeyCode::BackTab   => if !all {stream_view_state.command_previous_stream();},
                KeyCode::Char(' ') => stream_view_state.command_toggle_autopan(),
                KeyCode::Char('j') | KeyCode::Down => 
                    stream_view_state.command_zoom_in(if ctrl{3} else {1}),
                KeyCode::Char('k') | KeyCode::Up =>
                    stream_view_state.command_zoom_out(if ctrl{3} else {1}),
                KeyCode::Char('h') | KeyCode::Left => 
                    stream_view_state.command_pan(if ctrl{-30} else {-1}),
                KeyCode::Char('l') | KeyCode::Right =>
                    stream_view_state.command_pan(if ctrl{30} else {1}),
                KeyCode::Char('w') => stream_view_state.command_vertical_pan(-1),
                KeyCode::Char('s') => stream_view_state.command_vertical_pan(1),
                KeyCode::Char('D') => stream_view_state.command_toggle_debug_mode(),
                KeyCode::PageUp    => stream_view_state.command_pan_halfscreens(if ctrl{-2}else{-1}),
                KeyCode::PageDown  => stream_view_state.command_pan_halfscreens(if ctrl{2}else{1}),
                _                  => return false,
            }
        },
        CEvent::Mouse(mouse) => {
            match mouse.kind {
                MouseEventKind::Down(button) => {
                    match button {
                        MouseButton::Left => {
                            log::info!(target:"main", "command_select: x{} y{}", mouse.column, mouse.row);
                            stream_view_state.command_select(mouse.column, mouse.row);
                        },
                        MouseButton::Right => {
                            log::info!(target:"main", "command_focus: x{} y{}", mouse.column, mouse.row);
                            stream_view_state.command_focus(mouse.column, mouse.row);
                        },
                        MouseButton::Middle => {
                            log::info!(target:"main", "middle click: x{} y{}", mouse.column, mouse.row);
                        }
                    }
                },
                MouseEventKind::Up(_button) => { },
                MouseEventKind::Drag(_button) => { },
                MouseEventKind::Moved => { },
                MouseEventKind::ScrollUp =>   stream_view_state.command_zoom_in(1),
                MouseEventKind::ScrollDown => stream_view_state.command_zoom_out(1),
            }
        },
        CEvent::Resize(_x, _y) => {
        }
    }
    true
}

/// Global input handling, present in all tabs.
fn process_global_input_event(event: &CEvent, active_menu_item: &mut MenuItem, stream_view_state: &mut StreamViewState) -> bool {
    if let CEvent::Key(key) = event {
        match key.code {
            KeyCode::Char('Q') => return false,
            KeyCode::Char('S') => {
                stream_view_state.command_unfocus();
                stream_view_state.command_desynchronize();
                *active_menu_item = MenuItem::StreamView;
            }
            KeyCode::Char('A') => {
                stream_view_state.command_unfocus();
                stream_view_state.command_synchronize();
                *active_menu_item = MenuItem::StreamViewAll;
            },
            KeyCode::Char('g') => *active_menu_item = MenuItem::Log,
            _                  => {}
        }
    }
    true
}

/// Start the input thread - polls for input events and sends them to the rendering (main) thread.
///
/// Returns the receiver for events sent by the input thread.
fn start_input_thread(framerate: u64) -> mpsc::Receiver<Event> {
    // Input thread init
    let (tx, rx) = mpsc::channel();
    let tick_rate = Duration::from_millis(1000 / framerate);
    // Input thread - sends input keys and regular ticks to the rendering thread
    thread::spawn(move || {
        let mut last_tick = Instant::now();
        loop {
            let timeout = tick_rate
                .checked_sub(last_tick.elapsed())
                .unwrap_or_else(|| Duration::from_secs(0));
            if event::poll(timeout).expect("poll works") {
                tx.send(Event::Input(event::read().expect("can read events"))).expect("can send events");
            }
            if last_tick.elapsed() >= tick_rate && tx.send(Event::Tick).is_ok() {
                last_tick = Instant::now();
            }
        }
    });
    rx
}

/// Start stream reading threads and return `StreamViewState` containing them.
fn start_stream_reading_threads(stream_paths: &[String], reader_thread_count: usize) -> Result<StreamViewState, Box<dyn std::error::Error>> {
    // Init stream view state and start stream reader thread/s
    let mut stream_view_state = StreamViewState::new(stream_paths, reader_thread_count);
    for thread in &mut stream_view_state.stream_readers {
        if let Err(why) = thread.start() {
            eprintln!("Error starting a stream reader thread: {why}");
            return Err(Box::new(why));
        }
    }
    Ok(stream_view_state)
}

/// Stop stream reading threads, deinitializing a `StreamViewState`.
fn stop_stream_reading_threads(mut stream_view_state: StreamViewState) -> Result<(), Box<dyn std::error::Error>> {
    for reader in &mut stream_view_state.stream_readers {
        reader.stop();
    }
    for reader in stream_view_state.stream_readers {
        reader.join()?;
    }
    Ok(())
}

/// Initialze the terminal for TUI rendering.
fn init_terminal() -> Result<tui::Terminal<CrosstermBackend<io::Stdout>>, Box<dyn std::error::Error>> {
    // Panic hook to have readable output when we crash while rendering TUI.
    std::panic::set_hook(Box::new(|info| {
        disable_raw_mode().unwrap();
        crossterm::execute!(io::stdout(), DisableMouseCapture).unwrap();
        let msg = match info.payload().downcast_ref::<&'static str>() {
            Some(s) => *s,
            None => match info.payload().downcast_ref::<String>() {
                Some(s) => &s[..],
                None => "Box<Any>",
            }
        };
        if let Some(location) = info.location() {
            eprintln!("panic in file '{}' at line {}", location.file(), location.line() );
        } 
        eprintln!("{msg:?}");
    }));

    enable_raw_mode().expect("can run in raw mode");
    crossterm::execute!(
        io::stdout(),
        SetBackgroundColor(crossterm::style::Color::Rgb{r:0,g:0,b:0}),
        EnableMouseCapture
    )?;
    let backend = CrosstermBackend::new(io::stdout());
    let mut terminal = tui::Terminal::new(backend)?;
    terminal.clear()?;
    Ok(terminal)
}

/// Deinitialize the terminal to end TUI rendering.
fn deinit_terminal(mut terminal: tui::Terminal<CrosstermBackend<io::Stdout>>) -> Result<(), Box<dyn std::error::Error>>{
    disable_raw_mode()?;
    crossterm::execute!(io::stdout(), DisableMouseCapture)?;
    terminal.show_cursor()?;
    Ok(())
}

fn main() -> Result<(), Box<dyn std::error::Error>> {
    // Logging init
    tui_logger::init_logger(log::LevelFilter::Trace).unwrap();
    tui_logger::set_default_level(log::LevelFilter::Trace);
    log::info!(target:"main", "timedilator start");

    let (stream_paths, framerate, reader_thread_count) = handle_cli()?;
    let rx                    = start_input_thread(framerate);
    let mut stream_view_state = start_stream_reading_threads(&stream_paths[..], reader_thread_count)?;

    // Start drawing the TUI.
    let mut terminal = init_terminal()?;

    // State for the menu/tab bar and log
    let menu_titles          = ["Stream", "All", "Log", "Quit"];
    let mut active_menu_item = MenuItem::StreamView;
    let mut log_state        = tui_logger::TuiWidgetState::new();

    let mut process_frame = || -> Result<bool, Box<dyn std::error::Error>> {
        // Rendering
        terminal.draw(|frame| {
            // Main layout with a tab bar on top and a large widget taking up the entire screen.
            let chunks = Layout::default()
                .direction(Direction::Vertical)
                .margin(0)
                .constraints(
                    [
                        Constraint::Length(1), // menu/tab bar, top of screen
                        Constraint::Min(2),    // 2+ line central area
                    ]
                    .as_ref(),
                )
                .split(frame.size());

            let tabs = init_tabs(&menu_titles, active_menu_item);
            // Draw the tabs
            frame.render_widget(tabs, chunks[0]);

            // Draw current main widget
            match active_menu_item {
                // View a single stream.
                MenuItem::StreamView => {
                    frame.render_stateful_widget(StreamView::default(), chunks[1], &mut stream_view_state);
                    stream_view_state.reset_input();
                },
                // View all streams (use the same stream_view_state, alternating stream ID).
                MenuItem::StreamViewAll => {
                    let stream_count = u16::try_from(stream_paths.len()).unwrap();
                    let constraints = if let Some(focused_stream_id) = stream_view_state.focused_stream() {
                        let focused_height = chunks[1].height / 2;
                        // Height of all views except the focused one (avoiding div by zero).
                        let default_height = if stream_count > 1 {
                            (chunks[1].height - focused_height) / (stream_count-1)
                        } else {
                            0
                        };

                        let mut constraints: Vec<Constraint> = (0..stream_count-1)
                            .map(|i| {
                                Constraint::Length(if i == focused_stream_id.0 {focused_height} else {default_height})
                            })
                            .collect();
                        constraints.push(Constraint::Min(1)); // use up the leftover space for the last chunk 
                        constraints
                    } else {
                        let mut constraints: Vec<Constraint> = (0..stream_count-1)
                            .map(|_i| Constraint::Length(chunks[1].height / stream_count as u16))
                            .collect();
                        constraints.push(Constraint::Min(1)); // use up the leftover space for the last chunk
                        constraints
                    };

                    let stream_chunks = Layout::default()
                        .direction(Direction::Vertical)
                        .margin(0)
                        .constraints(&*constraints)
                        .split(chunks[1]);
                    for s in 0..stream_count {
                        stream_view_state.command_set_stream(s.into());
                        frame.render_stateful_widget(StreamView::default(), stream_chunks[s as usize], &mut stream_view_state);
                    }

                    stream_view_state.reset_input();
                },
                MenuItem::Log => {
                    let log = init_log_widget(&mut log_state);
                    frame.render_widget(log, chunks[1]);
                }
            }
        }).expect("failed to draw a frame");

        // Keep a copy of the menu item for the rest of the frame in case we change it
        let current_active_menu_item = active_menu_item;
        // Input processing
        match rx.recv()? {
            Event::Input(event) => {
                let event_consumed = match current_active_menu_item {
                    MenuItem::StreamView    => process_stream_view_input_event(&event, &mut stream_view_state, false),
                    MenuItem::StreamViewAll => process_stream_view_input_event(&event, &mut stream_view_state, true),
                    MenuItem::Log           => process_logger_input_event(&event, &mut log_state, &mut stream_view_state)
                };
                if !event_consumed && !process_global_input_event(&event, &mut active_menu_item, &mut stream_view_state) {
                    return Ok(false);
                }
            },
            Event::Tick => {}
        }
        Ok(true)
    };

    let mut main_loop = || -> Result<(), Box<dyn std::error::Error>> 
    {
        while process_frame()? {
        }
        Ok(())
    };

    let result = main_loop();
    // Shutdown
    stop_stream_reading_threads(stream_view_state)?;
    deinit_terminal(terminal)?;
    result
}
