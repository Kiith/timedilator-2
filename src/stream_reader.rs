use std::fmt;
use std::io;
use std::io::Read;
use std::fs::File;
use std::collections::VecDeque;
use std::convert::{TryInto, TryFrom};
use std::sync::{Arc, Mutex};
use std::sync::mpsc;
use std::thread;
use std::cmp;
use std::mem;
use std::time;

use hdrhistogram::Histogram;

use crate::event_id::{
    NestLevel,
    DataID,
    IDBase,
    TimepointID,
};
use crate::stream::{
    Stream,
    StreamID,
    Scope,
    StreamTime,
};
use crate::stat_utils::{
    bin,
};

/// Offset into a file stream.
#[derive(Copy, Clone)]
struct StreamOffset(usize);
/// Time value measured by fast relatively imprecise clock like RDTSC, for timepoint measurements.
#[derive(Copy, Clone)]
struct Ticks(u64);
/// Time value measured by a higher-overhead high precision clock, used to correct timepoint measurements.
#[derive(Copy, Clone)]
struct HighPrecTime(u64);

/// Timepoint nanoprof event.
#[derive(Copy, Clone)]
struct TimepointEvent {
    id:         TimepointID,
    time_delta: Ticks,
}

/// High precision nanoprof event.
#[derive(Copy, Clone)]
struct HighPrecEvent {
    nsecs:  HighPrecTime,
}

/// Identifies type of an ID, since IDs of different types may have the same value.
#[derive(Copy, Clone, Eq, PartialEq, PartialOrd)]
pub enum IDType {
    Timepoint = 0,
    DataU8    = 1,
    DataU16   = 2,
    DataU32   = 3,
    DataU64   = 4,
    DataF32   = 5,
    DataF64   = 6,
    DataStr   = 7,
}

impl IDType {
    /// Number of data event ID types (same as number of data event types).
    const DATA_EVENT_TYPE_COUNT: usize = 7;

    /// Convert an ID register event ID type byte into an `IDType`.
    pub const fn from_byte(byte: u8) -> Option<Self> {
        match byte {
            0b0000_0001 => Some(Self::Timepoint),
            0b0000_0010 => Some(Self::DataU8), 
            0b0000_0011 => Some(Self::DataU16),
            0b0000_0100 => Some(Self::DataU32),
            0b0000_0101 => Some(Self::DataU64),
            0b0000_0110 => Some(Self::DataF32),
            0b0000_0111 => Some(Self::DataF64),
            0b0000_1000 => Some(Self::DataStr),
            _ => None
        }
    }
}

/// Nanoprof event (timepoint or data) ID register event.
#[derive(Clone)]
struct IDRegisterEvent {
    /// ID of the event associated with the name/nest level.
    id:         IDBase,
    /// For timepoint events only - nest level of the timepoint.
    nest_level: NestLevel,
    /// Type of event we're registering.
    id_type:    IDType,
    /// Name of the event.
    name:       String
}

/// Data nanoprof event. May have different data types.
#[derive(Clone)]
pub enum DataEvent {
    U8     { id: DataID, value: u8     },
    U16    { id: DataID, value: u16    },
    U32    { id: DataID, value: u32    },
    U64    { id: DataID, value: u64    },
    F32    { id: DataID, value: f32    },
    F64    { id: DataID, value: f64    },
    String { id: DataID, value: String },
}

impl DataEvent {
    /// Convert the *value* of a data event to a human-readable string.
    pub fn value_str(&self) -> String {
        match self {
            Self::U8{id: _,     value} => { format!("{}", value) }
            Self::U16{id: _,    value} => { format!("{}", value) }
            Self::U32{id: _,    value} => { format!("{}", value) }
            Self::U64{id: _,    value} => { format!("{}", value) }
            Self::F32{id: _,    value} => { format!("{}", value) }
            Self::F64{id: _,    value} => { format!("{}", value) }
            Self::String{id: _, value} => { value.clone() }
        }
    }
}

/// A data event recorded after timepoint with specified index in the timepoint buffer.
#[derive(Clone)]
pub struct PositionedDataEvent {
    /// Index of the timepoint in StreamReaderFile::timepoint_buffer (updated to sync as items are
    /// moved in timepoint bufer)
    timepoint_index: usize,
    /// The data event itself.
    data:            DataEvent
}

/// Statistics aggregated for scopes with the same ID.
#[derive(Copy, Clone)]
pub struct ScopeStats {
    /// Number of scopes with this ID.
    pub instance_count:           u64,
    /// Duration of the shortest scope with this ID.
    pub duration_max:             StreamTime,
    /// Duration of the longest scope with this ID.
    pub duration_min:             StreamTime,
    /// Average duration of a scope with this ID.
    pub duration_mean:            StreamTime,
    /// 99.9th percentile of duration of scopes with this ID.
    pub duration_percentile_99_9: StreamTime,
    /// 99th percentile of duration of scopes with this ID.
    pub duration_percentile_99:   StreamTime,
    /// 90th percentile of duration of scopes with this ID.
    pub duration_percentile_90:   StreamTime,
    /// 75th percentile of duration of scopes with this ID.
    pub duration_percentile_75:   StreamTime,
    /// Duration of the median scope with this ID.
    pub duration_median:          StreamTime,
    /// 25th percentile of duration of scopes with this ID.
    pub duration_percentile_25:   StreamTime,
    /// Standard deviation of scopes with this ID.
    pub duration_stdev:           StreamTime,
    pub count_percentile_99_9:    u64,
    pub count_percentile_99:      u64,
    pub count_percentile_90:      u64,
    pub count_percentile_75:      u64,
    pub count_median:             u64,
    pub count_percentile_25:      u64,
}

impl ScopeStats {
    pub const fn default() -> Self {
        Self {
            instance_count:           0,
            duration_max:             StreamTime::MIN,
            duration_min:             StreamTime::MIN,
            duration_mean:            StreamTime::MIN,
            duration_percentile_99_9: StreamTime::MIN,
            duration_percentile_99:   StreamTime::MIN,
            duration_percentile_90:   StreamTime::MIN,
            duration_percentile_75:   StreamTime::MIN,
            duration_median:          StreamTime::MIN,
            duration_percentile_25:   StreamTime::MIN,
            duration_stdev:           StreamTime::MIN,
            count_percentile_99_9:    0,
            count_percentile_99:      0,
            count_percentile_90:      0,
            count_percentile_75:      0,
            count_median:             0,
            count_percentile_25:      0,
        }
    }
}

/// Any duration longer than this is clamped to this max. We need a fixed max to limit histogram memory usage.
const MAX_HISTOGRAM_DURATION: StreamTime = StreamTime::from_nanoseconds(10 * 1000 * 1000 * 1000);

/// Metadata stored in ID registry about timepoint events with a given ID.
#[derive(Clone)]
pub struct TimepointMetadata {
    /// Name of scopes with this ID.
    pub name:       String,
    /// ID this metadata corresponds to.
    pub id:         TimepointID,
    /// Nest level of scopes with this ID.
    pub nest_level: NestLevel,
    /// Histogram of durations of scopes with this ID.
    histogram: Histogram<u64>
}

impl TimepointMetadata {
    /// Get statistics for scopes with timepoint ID of this `TimepointMetadata`.
    #[allow(clippy::cast_sign_loss)] // Values in the histogram are always non-negative.
    #[allow(clippy::cast_possible_truncation)] // Values should never get large enough for cast to u64 to lose precision
    pub fn get_scope_stats(&self) -> ScopeStats {
        let pc999 = self.histogram.value_at_quantile(0.999);
        let pc99  = self.histogram.value_at_quantile(0.99);
        let pc90  = self.histogram.value_at_quantile(0.9);
        let pc75  = self.histogram.value_at_quantile(0.75);
        let pc50  = self.histogram.value_at_quantile(0.5);
        let pc25  = self.histogram.value_at_quantile(0.25);

        ScopeStats {
            instance_count:           self.histogram.len(),
            duration_max:             StreamTime::from_raw(self.histogram.max()),
            duration_min:             StreamTime::from_raw(self.histogram.min()),
            duration_mean:            StreamTime::from_raw(self.histogram.mean() as u64),
            duration_percentile_99_9: StreamTime::from_raw(pc999),
            duration_percentile_99:   StreamTime::from_raw(pc99),
            duration_percentile_90:   StreamTime::from_raw(pc90),
            duration_percentile_75:   StreamTime::from_raw(pc75),
            duration_median:          StreamTime::from_raw(pc50),
            duration_percentile_25:   StreamTime::from_raw(pc25),
            duration_stdev:           StreamTime::from_raw(self.histogram.stdev().round() as u64),
            count_percentile_99_9:    self.histogram.count_between(pc999, MAX_HISTOGRAM_DURATION.raw()),
            count_percentile_99:      self.histogram.count_between(pc99,  MAX_HISTOGRAM_DURATION.raw()),
            count_percentile_90:      self.histogram.count_between(pc90,  MAX_HISTOGRAM_DURATION.raw()),
            count_percentile_75:      self.histogram.count_between(pc75,  MAX_HISTOGRAM_DURATION.raw()),
            count_median:             self.histogram.count_between(pc50,  MAX_HISTOGRAM_DURATION.raw()),
            count_percentile_25:      self.histogram.count_between(pc25,  MAX_HISTOGRAM_DURATION.raw()),
        }
    }
}

/// Metadata stored in ID registry about data events with a given ID.
#[derive(Clone)]
pub struct DataEventMetadata {
    /// Name of scopes with this ID.
    pub name:       String,
    /// ID this metadata corresponds to.
    pub id:         DataID,
}

/// Struct for temp timepoint storage after decoding, but before adding to a Stream.
struct ProcessedTimepoint {
    /// Timepoint absolute time (not delta.
    time:       StreamTime,
    /// Timepoint ID.
    id:         TimepointID,
}

/// Debugging statistics about the ID registry.
pub struct IDRegistryStats {
    /// Estimated memory used by stats histograms.
    memory_histograms:  usize,
    /// Memory used by timepoint registration table.
    memory_timepoints:  usize,
    /// Memory used by data event registration tables.
    memory_data_events: usize
}

impl IDRegistryStats {
    const fn new() -> Self {
        Self {
            memory_histograms:  0,
            memory_timepoints:  0,
            memory_data_events: 0,
        }
    }

    /// Get total memory allocated by the registry in bytes.
    pub const fn total_memory_used(&self) -> usize {
        self.memory_histograms + self.memory_timepoints + self.memory_data_events
    }
}
impl fmt::Display for IDRegistryStats {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "histograms:  {}\n\
                   timepoints:  {}\n\
                   data events: {}",
                   bin(self.memory_histograms),
                   bin(self.memory_timepoints),
                   bin(self.memory_data_events))
    }
}

/// Timepoint IDs from `IDRegisterEvents` are stored here together with their metadata such as name and nest level.
pub struct IDRegistry {
    /// Vector storing metadata for each timepoint ID at index equal to that ID.
    id_to_metadata: Vec<Option<TimepointMetadata>>,
    /// Vectors storing metadata for data events of each type, similar to `id_to_metadata`.
    data_id_to_metadata: [Vec<Option<DataEventMetadata>>; IDType::DATA_EVENT_TYPE_COUNT]
}

impl IDRegistry {
    /// Precision of the histogram in significant values - '2' is 1% precision. Keeping this low limits histogram memory usage.
    const HISTOGRAM_SIGNIFICANT_FIGURES: u8 = 2;

    fn new() -> Self {
        let data_id_count: usize = (DataID::MAX.0 + 1).try_into().unwrap();
        Self {
            // These vectors are used as maps - non-mapped values are None.
            id_to_metadata: vec![None; (TimepointID::MAX.0 + 1).try_into().unwrap()],
            data_id_to_metadata: 
                [vec![None; data_id_count],
                 vec![None; data_id_count],
                 vec![None; data_id_count],
                 vec![None; data_id_count],
                 vec![None; data_id_count],
                 vec![None; data_id_count],
                 vec![None; data_id_count]]
        }
    }

    /// Collect and return debugging stats about the ID registry.
    fn collect_debug_stats(&self) -> IDRegistryStats {
        let mut stats = IDRegistryStats::new();
        for event in self.id_to_metadata.iter().flatten() {
            stats.memory_histograms += event.histogram.distinct_values() * mem::size_of::<u64>();
        }
        stats.memory_timepoints  = self.id_to_metadata.len() * mem::size_of::<Option<TimepointMetadata>>();
        stats.memory_data_events =
            self.data_id_to_metadata.iter().map(|table|table.len() * mem::size_of::<Option<DataEventMetadata>>()).sum();
        stats
    }

    /// Register timepoint from event `id_register_event`.
    fn register_timepoint_id(&mut self, id_register_event: IDRegisterEvent) -> io::Result<()> {
        let id = id_register_event.id;
        log::info!(target:"stream", "registering timepoint ID {id} with name {} and nest level {}",
            id_register_event.name, id_register_event.nest_level.0 );
        assert!(id_register_event.id_type == IDType::Timepoint,
                "register_timepoint_id must be called with a timepoint ID register event");
        assert!(id <= TimepointID::MAX.0,
                "timepoint ID must be guaranteed to be in range by stream reader code");
        if let Some( _meta ) = &self.id_to_metadata[id as usize] {
            return Err( io::Error::new(io::ErrorKind::InvalidData, "timedilator does not support re-registering of timepoint event with the same ID" ) )
        }
        self.id_to_metadata[id as usize] =
            Some( TimepointMetadata{
                name:       id_register_event.name,
                id:         TimepointID(id),
                nest_level: id_register_event.nest_level,
                histogram:  Histogram::<u64>::new_with_max(MAX_HISTOGRAM_DURATION.raw(), Self::HISTOGRAM_SIGNIFICANT_FIGURES).unwrap()
                });
        Ok(())
    }

    /// Register data event from event `id_register_event.`
    fn register_data_id(&mut self, id_register_event: IDRegisterEvent) -> io::Result<()> {
        let id = id_register_event.id;
        // Data event registrations don't include a nest level.
        log::info!(target:"stream", "registering data event ID {id} with type {} and name {}",
            id_register_event.id_type as i32, id_register_event.name );
        assert!(id_register_event.id_type != IDType::Timepoint,
                "register_data_id must not be called with a timepoint ID register event");
        assert!(id <= DataID::MAX.0,
                "Data event ID must be guaranteed to be in range by stream reader code");

        let id_to_metadata = match id_register_event.id_type {
            IDType::Timepoint => panic!("Timepoint ID must be handled in register_timepoint_id"),
            _ => &mut self.data_id_to_metadata[(id_register_event.id_type as usize) - 1]
        };

        if let Some( _meta ) = &id_to_metadata[id as usize] {
            return Err( io::Error::new(io::ErrorKind::InvalidData, "timedilator does not support re-registering of data event with the same ID" ) )
        }

        id_to_metadata[id as usize] = Some(
            DataEventMetadata{
                name: id_register_event.name,
                id:   DataID(id)});
        Ok(())
    }

    /// Get metadata for specified timepoint ID (or None if no such ID is registered).
    pub fn get_meta(&self, id: TimepointID ) -> &Option<TimepointMetadata> {
        &self.id_to_metadata[id.0 as usize]
    }

    /// Get metadata for specified data event (we get ID and trype from the event).
    pub fn get_meta_data_event(&self, event: &DataEvent ) -> &Option<DataEventMetadata> {
        match event {
            DataEvent::U8{id,     value: _} => { self.get_meta_data_event_and_type( *id, IDType::DataU8  ) }
            DataEvent::U16{id,    value: _} => { self.get_meta_data_event_and_type( *id, IDType::DataU16 ) }
            DataEvent::U32{id,    value: _} => { self.get_meta_data_event_and_type( *id, IDType::DataU32 ) }
            DataEvent::U64{id,    value: _} => { self.get_meta_data_event_and_type( *id, IDType::DataU64 ) }
            DataEvent::F32{id,    value: _} => { self.get_meta_data_event_and_type( *id, IDType::DataF32 ) }
            DataEvent::F64{id,    value: _} => { self.get_meta_data_event_and_type( *id, IDType::DataF64 ) }
            DataEvent::String{id, value: _} => { self.get_meta_data_event_and_type( *id, IDType::DataStr ) }
        }
    }

    /// Get metadata for specified data event ID/type (or None if no such ID is registered).
    pub fn get_meta_data_event_and_type(&self, id: DataID, id_type: IDType) -> &Option<DataEventMetadata> {
        match id_type {
            IDType::Timepoint => panic!("Timepoint ID must be handled in register_timepoint_id"),
            _ => &self.data_id_to_metadata[(id_type as usize) - 1][id.0 as usize]
        }
    }

    /// Gather stats for all scopes in all nest levels since the last call to this function.
    fn gather_stats(&mut self, stream: &mut Arc<Mutex<Stream>>) {
        let mut locked_stream = stream.lock().unwrap();
        for level in NestLevel::MIN.0..=NestLevel::MAX.0 {
            locked_stream.collect_stats(NestLevel(level), |scope: Scope| {
                let meta = &mut self.id_to_metadata[scope.id.0 as usize];
                if let Some(metadata) = meta {
                    metadata.histogram.saturating_record(scope.duration.raw());
                }
            });
        }
    }
}

/// Debugging statistics about stream reader implementations..
pub struct StreamReaderStats {
    /// Memory used by the file read buffer.
    memory_read_buffer:        usize,
    /// Memory used by the main timepoint buffer.
    memory_timepoint_buffer:   usize,
    /// Memory used by the high-precision event buffer.
    memory_highprec_buffer:    usize,
    /// Memory used by the data event buffer.
    memory_data_buffer:        usize,
    /// Memory used by a second-stage timepoint event bufffer, if any.
    memory_timepoint_buffer_2: usize,
    /// Memory reserved for the file read buffer.
    memory_read_buffer_reserved:        usize,
    /// Memory reserved by the main timepoint buffer.
    memory_timepoint_buffer_reserved:   usize,
    /// Memory reserved by the high-precision event buffer.
    memory_highprec_buffer_reserved:    usize,
    /// Memory reserved by the data event buffer.
    memory_data_buffer_reserved:        usize,
    /// Memory used by a second-stage timepoint event bufffer, if any.
    memory_timepoint_buffer_2_reserved: usize,
}

impl StreamReaderStats {
    const fn new() -> Self {
        Self {
            memory_read_buffer:        0,
            memory_timepoint_buffer:   0,
            memory_highprec_buffer:    0,
            memory_data_buffer:        0,
            memory_timepoint_buffer_2: 0,
            memory_read_buffer_reserved:        0,
            memory_timepoint_buffer_reserved:   0,
            memory_highprec_buffer_reserved:    0,
            memory_data_buffer_reserved:        0,
            memory_timepoint_buffer_2_reserved: 0,
        }
    }

    /// Get total memory currently used by all buffers in bytes.
    pub const fn total_memory_used(&self) -> usize {
        self.memory_read_buffer +
        self.memory_timepoint_buffer +
        self.memory_highprec_buffer +
        self.memory_data_buffer +
        self.memory_timepoint_buffer_2
    }

    /// Get total memory reserved by all buffers in bytes.
    #[allow(dead_code)]
    pub const fn total_reserved(&self) -> usize {
        self.memory_read_buffer_reserved +
        self.memory_timepoint_buffer_reserved +
        self.memory_highprec_buffer_reserved +
        self.memory_data_buffer_reserved +
        self.memory_timepoint_buffer_2_reserved
    }
}
impl fmt::Display for StreamReaderStats {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "read_buffer:          used {} reserved {}\n\
                   timepoints:           used {} reserved {}\n\
                   highprec:             used {} reserved {}\n\
                   data events:          used {} reserved {}\n\
                   processed timepoints: used {} reserved {}",
                   bin(self.memory_read_buffer),        bin(self.memory_read_buffer_reserved),
                   bin(self.memory_timepoint_buffer),   bin(self.memory_timepoint_buffer_reserved),
                   bin(self.memory_highprec_buffer),    bin(self.memory_highprec_buffer_reserved),
                   bin(self.memory_data_buffer),        bin(self.memory_data_buffer_reserved),
                   bin(self.memory_timepoint_buffer_2), bin(self.memory_timepoint_buffer_2_reserved)) 
    }
}


/// Types implementing this trait should be able to read a nanoprof stream from some source,
/// e.g. a file or a network connection.
trait StreamReader {
    /// Read more stream data into a buffer, and return number of bytes buffered.
    fn buffer_more_data(&mut self) -> io::Result<usize>;

    /// Parse data buffered  by `buffer_more_data` into nanoprof events, and buffer timepoint and
    /// related events.
    ///
    /// Any ID register events will register timepoint IDs/names/etc. with `timepoint_registry`.
    ///
    /// Returns parsed (event count, 2B timepoint count, 4B timepoint count, 8B timepoint count)
    fn parse_data(&mut self, timepoint_registry: &mut Arc<Mutex<IDRegistry>> ) -> io::Result<(usize,usize,usize,usize)>;

    /// Process timepoint events buffered by `parse_data` into scopes, and buffer those scopes.
    ///
    /// Returns the number of scopes generated.
    fn process_timepoints(&mut self) -> io::Result<usize>;

    /// Add processed timepoints and data events recorded between those timepoints to `stream`.
    ///
    /// Returns the number of timepoints addded.
    ///
    /// * `stream`:               Stream to add to
    /// * `timepoint_registry`:   Registry with metadata about timepoint/data events.
    /// * `timepoints_processed`: Number of timepoints processed since the last call. Used to 
    ///                           update indices from data events to timepoints as timepoints are
    ///                           removed from the beginning of an internal timepoint buffer.
    fn add_timepoints_and_data_events_to_stream(
        &mut self,
        stream:               &mut Arc<Mutex<Stream>>,
        timepoint_registry:   &Mutex<IDRegistry>,
        timepoints_processed: usize) -> io::Result<usize>;

    /// Collect and return debugging statistics about the stream reader.
    fn collect_debug_stats(&self) -> StreamReaderStats;
}

/// `StreamReader` implementation reading a stream from a file.
struct StreamReaderFile {
    #[allow(dead_code)]
    /// ID of the stream being read.
    id:                     StreamID,
    /// Path to the file.
    path:                   String,
    /// Buffer for bytes read from the file.
    buffer:                 Vec<u8>,
    /// Current position to continue parsing at in `buffer`.
    buffer_position:        usize,
    /// File we're reading from.
    file:                   File,
    /// Current position in the file.
    file_offset:            StreamOffset,
    /// Timepoint events are buffered here by `parse_data()`.
    timepoint_buffer:       Vec<TimepointEvent>,
    /// HighPrec events are buffered here by `parse_data()`.
    highprec_buffer:        Vec<(HighPrecEvent, usize)>,
    /// Data events are buffered here by `parse_data()`, containing timepoint indices into `timepoint_buffer`.
    ///
    /// Using a deque so we can remove processed data events one by one from the front.
    data_buffer:            VecDeque<PositionedDataEvent>,
    /// Processed timepoints are buffered here by `process_timepoints()`
    processed_timepoint_buffer: Vec<ProcessedTimepoint>,
}

impl StreamReaderFile {
    /// Construct a `StreamReaderFile`, opening the file.
    fn new( id: StreamID, path: String ) -> io::Result<Self> {
        eprintln!("Trying to open stream file {path}");
        let file = File::open(path.clone())?;
        Ok(Self{
            id,
            path,
            buffer:                     vec![],
            buffer_position:            0,
            file,
            file_offset:                StreamOffset(0),
            timepoint_buffer:           vec![],
            highprec_buffer:            vec![],
            data_buffer:                VecDeque::new(),
            processed_timepoint_buffer: vec![],
        })
    }
}

impl StreamReaderFile {
    /// Parse a timepoint event starting at start of `bytes`, and return number of bytes read and the event.
    fn parse_event_timepoint(bytes: &[u8]) -> (usize, TimepointEvent) {
        if (bytes[1] & 0b1000_0000) == 0 {
            // 2B timepoint event 
            (2, TimepointEvent{ id: TimepointID(u32::from(bytes[0])), time_delta: Ticks(u64::from(bytes[1])) })
        } else if (bytes[2] & 0b1000_0000) == 0 {
            // If 1st bit of 3rd byte is 0, that byte is the start of a 2B
            // timedelta (4B format), otherwise the timedelta is 6B (8B format)
            let id         = (u32::from(bytes[0]) << 7) | (u32::from(bytes[1]) & 0b0111_1111);
            let time_delta = u64::from(u16::from_be_bytes( bytes[2..4].try_into().unwrap()));
            (4, TimepointEvent{ id: TimepointID(id), time_delta: Ticks(time_delta) })
        } else {
            let id         = (u32::from(bytes[0]) << 7) | (u32::from(bytes[1]) & 0b0111_1111);
            let bytes_23   = [bytes[2] & 0b0111_1111, bytes[3]];
            let time_delta = (u64::from(u16::from_be_bytes(bytes_23)) << 32) |
                              u64::from(u32::from_be_bytes(bytes[4..8].try_into().unwrap()));
            (8, TimepointEvent{ id: TimepointID(id), time_delta: Ticks(time_delta) })
        }
    }

    /// Parse a string starting at `base_offset` in buffer, and return the string and offset beyond it.
    fn parse_string(&self, base_offset: usize) -> io::Result<Option<(String, usize)>> {
        let buffer_remaining = &self.buffer[self.buffer_position + base_offset..];
        // Slice of bytes until terminating 0 byte.
        let raw_slice = match buffer_remaining.iter().position(|&x| x == 0) {
            Some(length) => &buffer_remaining[0..length],
            None         => return Ok(None) // didn't find a zero terminator in buffer; give up so we can buffer more data
        };
        // Parse the string as UTF-8.
        let utf8_string = match String::from_utf8(raw_slice.to_vec()) {
            Err(why)   => return Err(io::Error::new(io::ErrorKind::InvalidData, format!("invalid string (must be utf-8:) {}", why))),
            Ok(string) => string
        };
        // String and offset to the first byte beyond it.
        Ok(Some((utf8_string, base_offset + raw_slice.len() + 1)))
    }

    /// Parse a data event in buffer starting at current position, and return number of bytes read and the event.
    fn parse_event_data(&self, bytes: &[u8]) -> io::Result<Option<(usize, DataEvent)>> {
        let data_id = DataID((u32::from(bytes[1] & 0b0011_1111) << 8) | u32::from(bytes[2]));
        match bytes[0] & 0xF {
            0x1 => Ok(Some((4,  DataEvent::U8 {id: data_id, value: bytes[3]}))),
            0x2 => Ok(Some((5,  DataEvent::U16{id: data_id, value: u16::from_ne_bytes(bytes[3..5].try_into().unwrap())}))),
            0x3 => Ok(Some((7,  DataEvent::U32{id: data_id, value: u32::from_ne_bytes(bytes[3..7].try_into().unwrap())}))),
            0x4 => Ok(Some((11, DataEvent::U64{id: data_id, value: u64::from_ne_bytes(bytes[3..11].try_into().unwrap())}))),
            0x5 => Ok(Some((7,  DataEvent::F32{id: data_id, value: f32::from_ne_bytes(bytes[3..7].try_into().unwrap())}))),
            0x6 => Ok(Some((11, DataEvent::F64{id: data_id, value: f64::from_ne_bytes(bytes[3..11].try_into().unwrap())}))),
            0xF => {
                // 3 byte offset - data event / type byte, 2 ID bytes, then the data (string).
                match self.parse_string(3)? {
                    Some((string, offset)) => Ok(Some((offset, DataEvent::String{id: data_id, value: string}))),
                    None => Ok(None)
                }
            },
            _   => Err(io::Error::new(io::ErrorKind::InvalidData, format!("invalid event type: {}", bytes[0])))
        }
    }
}

impl StreamReader for StreamReaderFile {
    fn buffer_more_data(&mut self) -> io::Result<usize> {
        const READ_LENGTH: usize = 64 * 1024;
        let buffer_len      = self.buffer.len();
        let remaining_bytes = buffer_len - self.buffer_position;
        // Move unprocessed buffer data to the start of the buffer.
        self.buffer.copy_within( self.buffer_position .. buffer_len, 0 );
        self.buffer.resize( remaining_bytes + READ_LENGTH, 0 );
        self.buffer_position = 0;
        // Read more data.
        let bytes_read = self.file.read( &mut self.buffer[remaining_bytes .. remaining_bytes + READ_LENGTH])?;
        // Truncate the buffer to the actual size read.
        self.buffer.truncate( remaining_bytes + bytes_read );
        self.file_offset.0 += bytes_read;
        Ok(bytes_read)
    }

    // We do such casts here, intentionally.
    //#[allow(clippy::cast_possible_wrap)]
    fn parse_data(&mut self, timepoint_registry: &mut Arc<Mutex<IDRegistry>> ) -> io::Result<(usize,usize,usize,usize)> {
        // Must be enough for any fixed-size event: timepoint, non-string data, highprec, ID register
        let max_fixed_event_bytes: usize = cmp::max( 8, cmp::max( 11, cmp::max( 8, 6 ) ) );
        // Number of events read is tracked here
        let mut event_count = 0;
        let mut timepoint_count_2b = 0;
        let mut timepoint_count_4b = 0;
        let mut timepoint_count_8b = 0;

        // Parse events as long as we have enough data for any fixed-size event (so we don't need bounds-checking everywhere)
        while self.buffer_position + max_fixed_event_bytes <= self.buffer.len() {
            // Raw event bytes.
            let bytes = &self.buffer[self.buffer_position..self.buffer_position + max_fixed_event_bytes];
            // Match on event ID.
            match bytes[0] {
                // timepoint event
                0b0000_0000..=0b0111_1111 => {
                    let (bytes_eaten, timepoint) = Self::parse_event_timepoint(bytes);
                    self.buffer_position += bytes_eaten;
                    match bytes_eaten {
                        2 => timepoint_count_2b += 1,
                        4 => timepoint_count_4b += 1,
                        8 => timepoint_count_8b += 1,
                        _ => panic!("timepoint size must be 2,4 or 8")
                    };
                    self.timepoint_buffer.push( timepoint );
                }
                // data event
                0b1010_0000..=0b1010_1111 => {
                    let (bytes_eaten, data) = match self.parse_event_data(bytes) {
                        Ok(Some((b, event))) => (b, event),
                        Ok(None)             => return Ok((event_count, timepoint_count_2b, timepoint_count_4b, timepoint_count_8b)),
                        Err(why)             => return Err(why)
                    };
                    self.buffer_position += bytes_eaten;
                    // Push the data event.
                    self.data_buffer.push_back(
                        PositionedDataEvent {
                            timepoint_index: self.timepoint_buffer.len() - 1,
                            data
                        });
                }
                // highprec event
                0b1111_1101 => {
                    let nsecs = (u64::from(bytes[1]) << 48) |
                                (u64::from(u16::from_be_bytes(bytes[2..4].try_into().unwrap())) << 32) |
                                 u64::from(u32::from_be_bytes(bytes[4..8].try_into().unwrap()));
                    let highprec = HighPrecEvent{ nsecs: HighPrecTime( nsecs ) };
                    self.highprec_buffer.push((highprec, self.timepoint_buffer.len()));
                    self.buffer_position += 8;
                }
                // ID register event
                0b1111_1110 => {
                    // Only relevant when registering a timepoint event.
                    let nest_level = NestLevel(i8::from_be_bytes(bytes[1 .. 2].try_into().unwrap()));
                    // ID type - timepoint or some data event type.
                    let id_type = match IDType::from_byte(bytes[2]) {
                        Some(t) => t,
                        None    => return Err( io::Error::new( io::ErrorKind::InvalidData,
                            format!("unknown ID type {} in an ID register event", bytes[2])))
                    };
                    // ID of given type to register with this metadata.
                    let id = (u32::from(bytes[3] & 0b0011_1111) << 8 | u32::from(bytes[4]) ) as IDBase;
                    // Name of the events with this type/ID.
                    let name = match self.parse_string(5)? {
                        Some((string, offset)) => {
                            self.buffer_position += offset;
                            string
                        },
                        None => {
                            // didn't find a zero terminator in buffer; give up so we can buffer more data
                            return Ok((event_count, timepoint_count_2b, timepoint_count_4b, timepoint_count_8b))
                        }
                    };

                    let register_event = IDRegisterEvent{ id, nest_level, id_type, name };
                    match id_type {
                        IDType::Timepoint => {
                            timepoint_registry.lock().unwrap().register_timepoint_id( register_event )?;
                        },
                        IDType::DataU8 | IDType::DataU16 | IDType::DataU32 | IDType::DataU64 | IDType::DataF32 | IDType::DataF64 | IDType::DataStr => {
                            timepoint_registry.lock().unwrap().register_data_id( register_event )?;
                        }
                    }
                }
                // padding 'event'
                0b1111_1111 => {
                    self.buffer_position += 1;
                }
                _ => {
                    return Err( io::Error::new(io::ErrorKind::InvalidData, format!( "invalid event type: {}", bytes[0] )) ); 
                }
            }

            event_count += 1;
        }
        Ok((event_count, timepoint_count_2b, timepoint_count_4b, timepoint_count_8b))
    }

    fn process_timepoints(&mut self) -> io::Result<usize> {
        // We need at least one timepoint per highprec event 
        if self.highprec_buffer.len() < 2 {
            return Ok(0)
        }

        let mut highprec_consumed  = 0;
        let mut timepoint_consumed = 0;
        let mut timepoints_added   = 0;
        for i in 0 .. self.highprec_buffer.len() - 1 {
            let (start_highprec, start_timepoint_idx) = self.highprec_buffer[i];
            let (end_highprec,   end_timepoint_idx)   = self.highprec_buffer[i + 1];

            if end_timepoint_idx >= self.timepoint_buffer.len() {
                // don't have a timepoint for the last highprec event, so don't consume that
                // event or corresponding timepoints.
                break;
            }

            // start_highprec sets the time of first timepoint in this slice (not last timepoint of
            // previous iteration's slice), and so the first timepoint has no relative time delta.
            // end_highprec sends the time for the last timepoint in this slice.
            if end_highprec.nsecs.0 < start_highprec.nsecs.0 {
                return Err(io::Error::new(io::ErrorKind::InvalidData, 
                    format!("a highprec event has value lower than previous highprec event ({} vs {})",
                            start_highprec.nsecs.0, end_highprec.nsecs.0)))
            }

            let highprec_delta_hpsecs = 10 * (end_highprec.nsecs.0 - start_highprec.nsecs.0);
            // includes the last time delta, does not include the first, since the first timepoint
            // has time delta 0 relative to first highprec event (its delta is relative to previous
            // timepoint, before the interval between the highprec events; on the other hand, the
            // last timepoint's delta is *in* that interval)
            let delta_ticks: u64 = self.timepoint_buffer[start_timepoint_idx + 1 ..=end_timepoint_idx]
                                       .iter()
                                       .map(|&x| x.time_delta.0)
                                       .sum();
            let hpsecs_per_tick = if delta_ticks != 0 {
                highprec_delta_hpsecs / delta_ticks
            } else {
                // if there was zero time delta in Ticks between two highprec events, the time
                // difference must be so small we may as well assume each timepoint to take no time.
                0
            };


            // start at position of the first timepoint/highprec
            let mut absolute_hpsecs = 10 * start_highprec.nsecs.0;

            // calculate time values for all timepoints *except* end_timepoint, which will be
            // processed with the next iteration.slice
            for t in start_timepoint_idx..end_timepoint_idx {
                let timepoint  = &self.timepoint_buffer[t];
                self.processed_timepoint_buffer.push(ProcessedTimepoint {
                    time:       StreamTime::from_hpsecs(absolute_hpsecs),
                    id:         timepoint.id
                });
                timepoints_added += 1;

                // A timepoint stores time delta *to get to that timepoint* from previous timepoint.
                // We're adding that at the end of the iteration, since in the first iteration, we
                // get the time value of the highprec event; time delta of first timepoint is
                // relative to the timepoint *before* that highprec event.
                let ticks_to_next = self.timepoint_buffer[t+1].time_delta;
                absolute_hpsecs    += ticks_to_next.0 * hpsecs_per_tick;
                timepoint_consumed += 1;
            }

            highprec_consumed += 1;
        }

        // remove processed events
        let timepoint_count = self.timepoint_buffer.len();
        let highprec_count  = self.highprec_buffer.len();
        self.timepoint_buffer.copy_within( timepoint_consumed..timepoint_count, 0 );
        self.timepoint_buffer.truncate( timepoint_count - timepoint_consumed);
        self.highprec_buffer.copy_within( highprec_consumed..highprec_count, 0 );
        self.highprec_buffer.truncate( highprec_count - highprec_consumed);
        for h in 0 .. self.highprec_buffer.len() {
            self.highprec_buffer[h].1 -= timepoint_consumed;
        }
        assert!( !self.highprec_buffer.is_empty(), "at least the last highprec event should be left in the buffer" );

        Ok(timepoints_added)
    }

    fn add_timepoints_and_data_events_to_stream(
        &mut self,
        stream:               &mut Arc<Mutex<Stream>>,
        timepoint_registry:   &Mutex<IDRegistry>,
        timepoints_processed: usize) -> io::Result<usize>
    {
        let mut locked_stream         = stream.lock().unwrap();
        let locked_timepoint_registry = &timepoint_registry.lock().unwrap();
        // Next timepoint where we need to also add data events.
        let mut next_data_event_timepoint_index =
            if self.data_buffer.is_empty() {
                usize::MAX
            } else {
                self.data_buffer[0].timepoint_index
            };

        // Add timepoints, and for each timepoint, add following data events.
        for t in 0..self.processed_timepoint_buffer.len() {
            let timepoint = &self.processed_timepoint_buffer[t];
            let meta = locked_timepoint_registry.get_meta(timepoint.id);
            let nest_level = match meta {
                Some(meta) => meta.nest_level,
                None       => return Err( io::Error::new(io::ErrorKind::InvalidData, format!( "unregistered timepoint ID {}", locked_stream.get_id().0 )) )
            };
            locked_stream.add_timepoint(timepoint.id, timepoint.time, nest_level);

            assert!(t <= next_data_event_timepoint_index, "somehow, a timepoint with a data event has been skipped");
            // This timepoint has data events.
            // Keep adding data events while they are for this timepoint.
            while next_data_event_timepoint_index == t {
                // Get and remove the first data event from the data buffer.
                if let Some(data) = self.data_buffer.pop_front() {
                    // Add the data event.
                    locked_stream.add_data_event_after_last_timepoint(nest_level, data.data);
                    // Next timepoint with a data event is at this idx (may still be the current timepoint).
                    next_data_event_timepoint_index = if !self.data_buffer.is_empty() {
                        self.data_buffer[0].timepoint_index
                    } else {
                        usize::MAX
                    };
                } else {
                    // Processed all data events.
                    next_data_event_timepoint_index = usize::MAX;
                    break;
                }
            }
        }

        // We've added all processed timepoints - clear them and update timepoint indices of
        // remaining data events to reflect the fact that processed timepoints have been removed.
        let timepoint_count = self.processed_timepoint_buffer.len();
        self.processed_timepoint_buffer.clear();
        for d in 0 .. self.data_buffer.len() {
            self.data_buffer[d].timepoint_index -= timepoints_processed;
        }
        Ok(timepoint_count)
    }

    fn collect_debug_stats(&self) -> StreamReaderStats {
        let mut stats = StreamReaderStats::new();
        stats.memory_read_buffer        = self.buffer.len();
        stats.memory_timepoint_buffer   = self.timepoint_buffer.len()   * mem::size_of::<TimepointEvent>();
        stats.memory_highprec_buffer    = self.highprec_buffer.len()    * mem::size_of::<HighPrecEvent>();
        stats.memory_data_buffer        = self.data_buffer.len()        * mem::size_of::<PositionedDataEvent>();
        stats.memory_timepoint_buffer_2 = self.processed_timepoint_buffer.len() * mem::size_of::<ProcessedTimepoint>();
        stats.memory_read_buffer_reserved        = self.buffer.capacity();
        stats.memory_timepoint_buffer_reserved   = self.timepoint_buffer.capacity()   * mem::size_of::<TimepointEvent>();
        stats.memory_highprec_buffer_reserved    = self.highprec_buffer.capacity()    * mem::size_of::<HighPrecEvent>();
        stats.memory_data_buffer_reserved        = self.data_buffer.capacity()        * mem::size_of::<PositionedDataEvent>();
        stats.memory_timepoint_buffer_2_reserved = self.processed_timepoint_buffer.capacity() * mem::size_of::<ProcessedTimepoint>();
        stats
    }
}

/// Events used for communication with `ThreadStreamReader`.
enum Event {
    StopThread,
    LogDebugStats
}

/// Thread that reads streams and makes them available (through a mutex) to the UI.
///
/// The thread reads multiple streams, reading files in bufferfuls in a round-robin fashion
/// (fill buffer of stream 1 from file 1, then a buffer of stream 2 from file 2, etc. cyclically)
pub struct ThreadStreamReader {
    /// Used to send mesages to the thread>
    tx_to_streamreader:   Option<mpsc::Sender<Event>>,
    /// Handle used to join the thread at the end.
    handle:               Option<thread::JoinHandle<io::Result<()>>>,
    /// ID of the first stream in `streams`, other streams' IDs are consecutively increasing.
    first_stream_id:      StreamID,
    /// Paths to files we're reading `streams` from, in the same order.
    stream_paths:         Vec<String>,
    /// Streams being read.
    streams:              Vec<Arc<Mutex<Stream>>>,
    /// Metadata of timepoint events in each stream in `streams`, in the same order.
    timepoint_registries: Vec<Arc<Mutex<IDRegistry>>>,
}

impl ThreadStreamReader {

    /// Construct a `ThreadStreamReader` reading specified files, but *don't* start it.
    pub fn new( first_stream_id: StreamID, stream_paths: Vec<String> ) -> Self {
        Self {
            tx_to_streamreader:   None,
            handle:               None,
            first_stream_id,
            stream_paths,
            streams:              vec![],
            timepoint_registries: vec![],
        }
    }

    /// Tell the thread to stop, but don't join it yet.
    pub fn stop(&mut self) {
        if let Some(sender) = &self.tx_to_streamreader {
            sender.send(Event::StopThread).expect("stream reader thread hung up (failed silently?)");
        }
    }

    /// Tell the thread to immediately log any debugging stats it can.
    pub fn log_debug_stats(&mut self) {
        if let Some(sender) = &self.tx_to_streamreader {
            sender.send(Event::LogDebugStats).expect("stream reader thread hung up (failed silently?)");
        }
    }

    /// Join the thread, blocking until it finishes joining.
    pub fn join(self) -> io::Result<()> {
        if let Some(handle) = self.handle {
            handle.join().expect("failed to join a stream reader thread")?;
        }
        Ok(())
    }

    /// Number of streams being read by the thread.
    pub fn stream_count(&self) -> usize {
        self.stream_paths.len()
    }

    /// Access stream with specified ID *if present in the thread*, return `None` otherwise.
    pub fn get_stream(&mut self, id: StreamID) -> Option<Arc<Mutex<Stream>>> {
        let stream_id_upper_bound = self.first_stream_id.0 + u16::try_from(self.streams.len()).unwrap();
        if id.0 >= self.first_stream_id.0 && id.0 < stream_id_upper_bound {
            Some(self.streams[(id.0 - self.first_stream_id.0) as usize].clone())
        } else {
            None
        }
    }

    /// Get an iterator over all streams processed by the thread
    pub fn iter_streams(& mut self) -> impl Iterator<Item = &mut Arc<Mutex<Stream>>> {
        self.streams.iter_mut()
    }

    /// Access ID registry for stream with specified ID *if present in the thread*, return `None` otherwise.
    pub fn get_id_registry(&mut self, id: StreamID) -> Option<Arc<Mutex<IDRegistry>>> {
        let stream_id_upper_bound = self.first_stream_id.0 + u16::try_from(self.streams.len()).unwrap();
        if id.0 >= self.first_stream_id.0 && id.0 < stream_id_upper_bound {
            Some(self.timepoint_registries[(id.0 - self.first_stream_id.0) as usize].clone())
        } else {
            None
        }
    }

    fn update_stream(
        reader:   &mut StreamReaderFile,
        stream:   &mut Arc<Mutex<Stream>>,
        registry: &mut Arc<Mutex<IDRegistry>>) -> io::Result<usize>
    {
        let read_start_time = time::Instant::now();
        let recv_duration = read_start_time.elapsed();

        let bytes_read = match reader.buffer_more_data() {
            Ok(bytes) => {
                // no new data from this stream, may as well skip to the next stream
                if bytes == 0 { return Ok(0); }
                bytes
            },
            Err(why) => {
                log::error!(target:"stream_reader", "failed while buffering more data from file {}: {why}", reader.path );
                return Err(why);
            }
        };
        let read_duration = read_start_time.elapsed() - recv_duration;

        let (events_parsed, tpoints_2b, tpoints_4b, tpoints_8b) = match reader.parse_data( registry ) {
            Ok((events_parsed, t2, t4, t8)) => {
                // no new events, skip to the next stream
                if events_parsed == 0 { return Ok(bytes_read); }
                (events_parsed, t2, t4, t8)
            },
            Err(why) => {
                log::error!(target:"stream_reader", "failed while parsing events from file {}: {why}", reader.path );
                return Err(why);
            }
        };
        let parse_duration = read_start_time.elapsed() - recv_duration - read_duration;

        let timepoints_processed = match reader.process_timepoints() {
            Ok(timepoints_processed) => {
                // no new scopes, skip to the next stream
                if timepoints_processed == 0 { return Ok(bytes_read); }
                timepoints_processed
            },
            Err(why) => {
                log::error!(target:"stream_reader", "failed while processing timepoints from file {}: {why}", reader.path );
                return Err(why);
            }
        };
        let timepoints_duration = read_start_time.elapsed() - recv_duration - read_duration - parse_duration;

        let timepoints_added = match reader.add_timepoints_and_data_events_to_stream( stream, registry, timepoints_processed ) {
            Ok(timepoints_added) =>  timepoints_added,
            Err(why) => {
                log::error!(target:"stream_reader", "failed while adding processed timepoints from file {}: {why}", reader.path );
                return Err(why);
            }
        };
        let tp_add_duration = read_start_time.elapsed() - recv_duration - read_duration - parse_duration - timepoints_duration;

        registry.lock().unwrap().gather_stats(stream);

        let stats_duration = read_start_time.elapsed() - recv_duration - read_duration - parse_duration - timepoints_duration - tp_add_duration;
        log::info!(target:"stream_reader",
           "stream {}: {bytes_read}B/{}us, {events_parsed} events/{}us \
            (T2/T4/T8 {tpoints_2b}/{tpoints_4b}/{tpoints_8b}), \
            {timepoints_processed} timepoints/{}us, {timepoints_added} tp_added/{}us stats:{}us", 
            stream.lock().unwrap().get_id().0,
            read_duration.as_micros(),
            parse_duration.as_micros(),
            timepoints_duration.as_micros(),
            tp_add_duration.as_micros(), 
            stats_duration.as_micros() );
        Ok(bytes_read)
    }

    /// Start the thread.
    pub fn start(&mut self) -> io::Result<()> {
        log::info!(target:"stream_reader", "{} start with {} streams", self.first_stream_id.0, self.stream_count() );
        let (tx, rx)            = mpsc::channel();
        self.tx_to_streamreader = Some(tx);
        log::info!(target:"stream_reader", "{} init streams", self.first_stream_id.0 );

        // Init streams/ID registries
        for i in 0..self.stream_paths.len() {
            let id = StreamID(u16::try_from(i).unwrap() + self.first_stream_id.0);
            self.streams.push(Arc::new(Mutex::new(Stream::new(id))));
            self.timepoint_registries.push(Arc::new(Mutex::new(IDRegistry::new())));
        }
        // all following variables are moved into the thread
        let mut streams                        = self.streams.clone();
        let mut timepoint_registries           = self.timepoint_registries.clone();
        let stream_paths                       = self.stream_paths.clone();
        let first_stream_id                    = self.first_stream_id;
        // Stream readers in same order as streams they read.
        let mut readers: Vec<StreamReaderFile> = vec![];

        log::info!(target:"stream_reader", "{} open files", self.first_stream_id.0 );
        for (i, path) in stream_paths.iter().enumerate() {
            let id = StreamID(u16::try_from(i).unwrap() + self.first_stream_id.0 );
            readers.push(StreamReaderFile::new(id, path.clone())?);
        }
        log::info!(target:"stream_reader", "{} spawn thread", self.first_stream_id.0 );

        // Spawn the thread itself.
        self.handle = Some(thread::spawn(move || -> io::Result<()> {
            log::info!(target:"stream_reader", "{} thread spawned", first_stream_id.0 );

            let mut total_batch_bytes = 1; // 1 to prevent sleep on startum

            // reading N streams, alternating between them
            loop {
                // If we're getting no new data, sleep to free up the CPU.
                if total_batch_bytes == 0 {
                    thread::sleep(time::Duration::from_millis(200));
                } 
                total_batch_bytes = 0;

                // Iterate stream readers, streams and registries in lockstep.
                for ((reader, stream), registry) in readers.iter_mut().zip(streams.iter_mut()).zip(timepoint_registries.iter_mut()) {
                    let print_stats = match rx.try_recv() {
                        Ok(event) => match event {
                            Event::StopThread    => return Ok(()),
                            Event::LogDebugStats => true,
                        },
                        Err(error) => match error {
                            // no events from main thread - continue
                            mpsc::TryRecvError::Empty        => false,
                            mpsc::TryRecvError::Disconnected => panic!("stream reader thread got disconnected from the main thread's side"),
                        }
                    };
                    total_batch_bytes += Self::update_stream(reader, stream, registry)?;
                    if print_stats {
                        let reader_stats   = reader.collect_debug_stats();
                        let stream_stats   = stream.lock().unwrap().collect_debug_stats();
                        let registry_stats = registry.lock().unwrap().collect_debug_stats();
                        log::info!(target:"stream_reader", 
                            "memory usage stats:\n#reader:\n{reader_stats}\n\n\
                             #stream:\n{stream_stats}\n\n#id registry:\n{registry_stats}\n\n\
                            TOTAL: {}\n", 
                            bin(reader_stats.total_memory_used() + stream_stats.total_memory_used() + registry_stats.total_memory_used()));
                    }
                }
            }
        }));
        Ok(())
    }
}
