use std::cmp;
use std::fmt;
use std::mem;

use bitpacking::{BitPacker1x, BitPacker4x, BitPacker};
use byteorder::{ByteOrder, NativeEndian};

use std::convert::{TryFrom, TryInto};
use std::collections::hash_map::DefaultHasher;
use std::hash::Hasher;

use crate::event_id::{
    TimepointID
};
use crate::stat_utils::{
    bin,
    dec,
};

/// Debugging statistics about `U16Array` implementations.
pub struct U16ArrayStats {
    /// Total memory allocated from the US.
    pub memory_total_reserved:   usize,
    /// Total memory currently used.
    pub memory_total_used:       usize,
    /// Metadata - blocks.
    pub memory_blocks:           usize,
    /// Metadata - tables (e.g. hash table).
    pub memory_tables:           usize,
    /// Size of compressed integer data itself.
    pub memory_data:             usize,
    /// Number of u16s stored in the array/s.
    pub stored_items_total:      usize,
    /// Number of entries in the largest table.
    pub tables_entries_max:      usize,
    /// Number of values stored in the largest table.
    pub tables_entries_used_max: usize,
    /// Number of blocks compaced with DedupSimple or pointing to table with DedupHash
    pub blocks_deduped:          usize,
    /// Total number of blocks.
    pub blocks_total:            usize,
}

impl U16ArrayStats {
    #[allow(dead_code)]
    pub const fn new() -> Self {
        Self {
            memory_total_reserved:   0,
            memory_total_used:       0,
            memory_blocks:           0,
            memory_tables:           0,
            memory_data:             0,
            stored_items_total:      0,
            tables_entries_max:      0,
            tables_entries_used_max: 0,
            blocks_deduped:          0,
            blocks_total:            0
        }
    }
}

impl fmt::Display for U16ArrayStats {
    #[allow(clippy::cast_precision_loss)]
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let mpi = |stat: usize| -> f64 { (stat * 8) as f64 / self.stored_items_total as f64 };
        write!(f, "mem:          used {} blocks {} tables {} data {}\n\
                   mem-per-item: used {:.2}b blocks {:.2}b tables {:.2}b data {:.2}b\n\
                   uncompressed: count {} mem {}\n\
                   reserved:     total {} per-item {:.2}b\n\
                   table:        entries-max {} used-max {}\n\
                   blocks:       deduped {} total {}" ,
            bin(self.memory_total_used), bin(self.memory_blocks), bin(self.memory_tables), bin(self.memory_data),
            mpi(self.memory_total_used), mpi(self.memory_blocks), mpi(self.memory_tables), mpi(self.memory_data),
            dec(self.stored_items_total), dec(self.stored_items_total * mem::size_of::<u16>()), 
            bin(self.memory_total_reserved), mpi(self.memory_total_reserved),
            dec(self.tables_entries_max), dec(self.tables_entries_used_max),
            dec(self.blocks_deduped), dec(self.blocks_total) )
    }
}

/// Implemented by (possibly compressed) arrays storing 16-bit values (timepoint IDs, time deltas) in a Track.
pub trait U16Array {
    /// Get value at specified index.
    fn at(&mut self, index: usize) -> u16;

    /// Get the last value, or None.
    fn last(&mut self) -> Option<u16> {
        if !self.is_empty() {
            Some(self.at(self.len() - 1))
        } else {
            None
        }
    }

    /// Add a value to the end of the array.
    fn push(&mut self, rhs: u16);

    /// Get number of items in the array.
    fn len(&self) -> usize;

    /// True if there are no values in the array.
    fn is_empty(&self) -> bool {
        self.len() == 0
    }

    /// Add statistics for this array to aggregate statistics for multiple such arrays.
    fn add_stats(&self, stats: &mut U16ArrayStats);
}

/// Stores 16-bit values in a plain vector.
#[derive(Clone)]
pub struct U16ArrayVec {
    data: Vec<u16>
}

impl U16ArrayVec {
    #[allow(dead_code)]
    pub const fn new(_is_id: bool) -> Self {
        Self {
            data: vec![],
        }
    }
}

impl U16Array for U16ArrayVec {
    fn at(&mut self, index: usize) -> u16 {
        self.data[index]
    }

    fn push(&mut self, rhs: u16) {
        self.data.push(rhs);
    }

    fn len(&self) -> usize {
        self.data.len()
    }

    fn add_stats(&self, stats: &mut U16ArrayStats) {
        stats.memory_total_reserved   += self.data.capacity() * mem::size_of::<u16>();  
        stats.memory_total_used       += self.data.len() * mem::size_of::<u16>(); 
        stats.memory_blocks           += 0;
        stats.memory_tables           += 0;
        stats.memory_data             += self.data.len() * mem::size_of::<u16>();
        stats.stored_items_total      += self.len();
        stats.tables_entries_max      += 0;
        stats.tables_entries_used_max += 0;
        stats.blocks_deduped          += 0;
        stats.blocks_total            += 0;
    }
}


/// Info about a block of u16s compressed by `BitPacker`.
///
/// May contain multiple actual 32-int `BitPacker1x` or 128-int `BitPacker4x` blocks. - see
/// `BLOCK_SIZE_EXP` and `USE_BITPACKER4` in `struct U16ArrayBitPacker`
#[derive(Clone, Copy)]
struct BitPackerBlock {
    start_offset: u32, // start offset in `block_data`

    // Bits per int values for bitpacker1x (32-int) blocks within the block
    // Stores one value per nibble for up to 4 values. Each nibble stores a value from 1 (0) to 16
    // (15) bits per int - we can't compress to 0 bits per int but this allows us to fit 4
    // bits_per_int values into 16 bits.
    //
    // The values are assigned to bitpacker1x blocks as follows:
    //
    // ---------- --------------------- ------- ----------------------------------
    // block size bp1x blocks per block nibbles                 nibble:bp1x-blocks
    // ---------- --------------------- ------- ----------------------------------
    //         32                     1       1 0:0
    //         64                     2       2 0:0,1:1
    //        128                     4       4 0:0,1:1,2:2,3:3
    //        256                     8       4 0:{0-1},1:{2-3},2:{4-5},3:{6-7}
    //        512                    16       4 0:{0-3},1:{4-7},2:{8-11},3:{12-15}
    // ---------- --------------------- ------- ----------------------------------
    //
    // See also `BITPACKER_BLOCKS_PER_BPI_NIBBLE`
    bits_per_int: u16,  // bits per int in compressed data - up to 4 bits
    base_value:   u16  // value to add to each compressed int.
}

// Ranges of timepoint ID values we want to be more compressible - not necessarily the full ranges of 
// gaps/end event IDs.
const MAX_USER:     u32 = TimepointID::MAX_USER.0;
const REAL_MIN_END: u32 = TimepointID::MIN_END.0;
const MIN_END:      u32 = REAL_MIN_END + 130;
const MAX_END:      u32 = MIN_END + 32;
const REAL_MAX_GAP: u32 = TimepointID::GAP_48.0;
const MAX_GAP:      u32 = REAL_MAX_GAP - 32;
const MIN_GAP:      u32 = MAX_GAP - 16;
const GAP_COUNT:    u32 = MAX_GAP - MIN_GAP + 1;
const END_COUNT:    u32 = MAX_END - MIN_END + 1;

/// max() usable in const computations.
const fn const_min(a: usize, b: usize) -> usize {
    [b, a][(a < b) as usize]
}

/// Stores u16s compressed with `BitPacker` blocks.
#[derive(Clone)]
pub struct U16ArrayBitPacker {
    /// Metadata 'block' structures storing info about raw blocks in `block_data`.
    block_meta: Vec<BitPackerBlock>,
    /// Compressed integers are tightly packed here.
    block_data: Vec<u8>,

    /// Last decompressed block is always decompressed into this array
    ///
    /// This allow us to avoid decompressing on each accessed integer as long as access is somewhat
    /// non-random.
    /// 32bit values used for BitPacker compatibility and speed.
    decompressed_block: [u32; Self::BLOCK_SIZE],
    /// Index of the decompressed block.
    decompressed_block_index: usize,

    /// Newly added integers are added to (and accessed from) this array.
    ///
    /// This effectively stores the last modulo `BLOCK_SIZE` items of the array.
    ///
    /// When it gets filled, it's compressed by BitPacker into a new block, which is 
    /// appended to `block_data`, and this array is reset.
    /// 32bit values used for BitPacker compatibility and speed.
    next_block:      [u32; Self::BLOCK_SIZE],
    /// Number of integers in 'next_block'
    next_block_used: usize,
    /// True if this array stores (14-bit) timepoint IDs.
    is_id: bool
}
impl U16ArrayBitPacker {
    /// log2 of the block size. `BitPacker1x` works with 32-int blocks so this must be at least 5.
    ///
    /// Note that there are no memory advantages to using less than 7 here (we store 4 bits-per-int 
    /// values per block) - there may be processing advantages as the next/decompressed buffer
    /// would be smaller.
    const BLOCK_SIZE_EXP:             usize = 7;
    /// Mask used to get the in-block part of an index.
    const IN_BLOCK_INDEX_MASK:        usize = (1 << Self::BLOCK_SIZE_EXP) - 1;
    /// Size of one block.
    const BLOCK_SIZE:                 usize = 1 << Self::BLOCK_SIZE_EXP;
    /// Should we compress indices with `BitPacker4x` instead of `BitPacker1x`?
    const USE_BITPACKER4:             bool  = Self::BLOCK_SIZE >= 256;

    #[allow(dead_code)]
    pub fn new(is_id: bool) -> Self {
        Self {
            block_meta:               Vec::with_capacity(512),
            block_data:               Vec::with_capacity(512),
            decompressed_block:       [0; Self::BLOCK_SIZE],
            decompressed_block_index: usize::MAX,
            next_block:               [0; Self::BLOCK_SIZE],
            next_block_used:          0,
            is_id,
        }
    }

    /// Shuffle integer values in a block to make timepoint ID values more compressible.
    ///
    /// Moves gap/end timepoint IDs to low values (likely close to most commonly used timepoint IDs)
    /// and moves everything else to avoid losing any values.
    fn shuffle_id_for_compressibility(block: &mut [u32; Self::BLOCK_SIZE]) {
        for id in block {
            const END_NORMAL:       u32 =  MAX_USER - GAP_COUNT - END_COUNT;
            const START_END_FILLER: u32 =  END_NORMAL + 1;
            const END_END_FILLER:   u32 =  MAX_USER - GAP_COUNT;
            const START_GAP_FILLER: u32 =  END_END_FILLER + 1;
            *id = match *id {
                MIN_END ..= MAX_END => {
                    *id - MIN_END
                },
                MIN_GAP ..= MAX_GAP => {
                    *id - MIN_GAP + END_COUNT
                },
                START_END_FILLER ..= END_END_FILLER => {
                    let offset = *id - START_END_FILLER;
                    MIN_END + offset
                },
                START_GAP_FILLER ..= MAX_USER => {
                    let offset = *id - START_GAP_FILLER;
                    MIN_GAP + offset
                },
                0 ..= END_NORMAL => {
                    *id + GAP_COUNT + END_COUNT
                }
                _ => {
                    *id
                }
            }
        }
    }

    /// Revert block of values shuffled by `shuffle_id_for_compressibility` to original values.
    fn revert_shuffle_id_for_compressibility(block: &mut [u32; Self::BLOCK_SIZE]) {
        for id in block {
            const LAST_TRANSFORMED_END:     u32 =  END_COUNT -1;
            const LAST_TRANSFORMED_GAP:     u32 =  END_COUNT + GAP_COUNT - 1;
            const FIRST_TRANSFORMED_NORMAL: u32 =  LAST_TRANSFORMED_GAP + 1;
            *id = match *id {
                0 ..= LAST_TRANSFORMED_END => {
                    MIN_END + *id
                },
                END_COUNT ..= LAST_TRANSFORMED_GAP => {
                    MIN_GAP + (*id - END_COUNT)
                },
                MIN_END ..= MAX_END => {
                    let offset = *id - MIN_END;
                    MAX_USER - GAP_COUNT - END_COUNT + 1 + offset
                },
                MIN_GAP ..= MAX_GAP => {
                    let offset = *id - MIN_GAP;
                    MAX_USER - GAP_COUNT + 1 + offset
                },
                FIRST_TRANSFORMED_NORMAL ..= MAX_USER => {
                    *id - FIRST_TRANSFORMED_NORMAL
                },
                _ => {
                    *id
                }
            }
        }
    }

    /// Assert that `shuffle_id_for_compressibility` can be reverted by `revert_shuffle_id_for_compressibility` for all values.
    #[allow(dead_code)]
    fn assert_shuffle_bidirectional() {
        for i in 0..(1 << 14) {
            let mut block = [0; Self::BLOCK_SIZE];
            block[0] = i;
            Self::shuffle_id_for_compressibility(&mut block);
            Self::revert_shuffle_id_for_compressibility(&mut block);
            assert!(block[0] == i, "{} didn't revert to the same value, instead reverting to {}", i, block[0]);
        }
    }

    /// Decompress indivudual `BitPacker` blocks within the array blocks specified by `meta`.
    fn decompress_base<Packer: BitPacker>(&mut self, meta: BitPackerBlock) {
        // This is basically zero-overhead
        let bitpacker                   = Packer::new();
        // Number of BitPacker blocks that fit into a single block.
        let subblock_count              = Self::BLOCK_SIZE / Packer::BLOCK_LEN;
        let nibbles                     = const_min(subblock_count, 4);
        // `bits_per_int` has 4 nibbles - up to 4 subblocks can have use one nibble each; if we
        // have more than 4 subblocks, this number of subblocks will share each nibble.
        let bitpacker_blocks_per_nibble = subblock_count / nibbles;
        let mut start_offset_src        = meta.start_offset as usize;

        for nibble_idx in 0..nibbles {
            // BitPacker blocks sharing a nibble (usually this is 1 block)
            let bb_start     = nibble_idx * bitpacker_blocks_per_nibble;
            let bb_end       = (nibble_idx + 1) * bitpacker_blocks_per_nibble;
            let bits_per_int = u8::try_from(1 + ((meta.bits_per_int >> (4 * nibble_idx)) & 0xF)).unwrap();
            let compressed_bitpacker_block_size = (bits_per_int as usize) * Packer::BLOCK_LEN / 8;
            for bb in bb_start..bb_end {
                // Decompress a bitpacker block
                let start_offset_dst = bb * Packer::BLOCK_LEN;

                bitpacker.decompress(
                     // Compressed BitPacker block.
                     &self.block_data[start_offset_src .. start_offset_src + compressed_bitpacker_block_size],
                     // Part of the decompressed array block corresponding to the BitPacker block.
                     &mut self.decompressed_block[start_offset_dst .. start_offset_dst + Packer::BLOCK_LEN],
                     bits_per_int );

                start_offset_src += compressed_bitpacker_block_size;
            }
        }
    }

    /// Compress `next_block` into a new array block and add that to `block_data`.
    fn compress_base<Packer: BitPacker>(&mut self, base_value: u32) {
        assert!(self.block_data.len() < u32::MAX as usize,
            "Reached max supported track length for BitPacker-compressed storage");
        let start_offset                = u32::try_from(self.block_data.len()).unwrap();
        // This is basically zero-overhead
        let bitpacker                   = Packer::new();
        // 128 works for both 1x and 4x; constants from surrounding class or Packer don't
        // (yet?) work with constants/arrays in generics.
        let mut compress_buf            = [0u8; 2 * 128]; // At most 2 bytes per integer.
        // All bits_per_int nibbles are merged/concatenated into this value.
        let mut bits_per_int: u16       = 0;
        let subblock_count              = Self::BLOCK_SIZE / Packer::BLOCK_LEN;
        let nibbles                     = const_min(subblock_count, 4);
        // `bits_per_int` has 4 nibbles - up to 4 subblocks can have use one nibble each; if we
        // have more than 4 subblocks, this number of subblocks will share each nibble.
        let bitpacker_blocks_per_nibble = subblock_count / nibbles;

        for nibble_idx in 0..nibbles  {
            // Compress bitpacker blocks using this nibble for bits_per_int.
            let bb_start = nibble_idx * bitpacker_blocks_per_nibble;
            let bb_end   = (nibble_idx + 1) * bitpacker_blocks_per_nibble;
            // Get maximum bits_per_int for all BitPacker blocks sharing this nibble,
            // so we can store a single value.
            let bits_per_int_nibble = (bb_start..bb_end).map(|bb| {
                    let start = bb * Packer::BLOCK_LEN;
                    bitpacker.num_bits(&self.next_block[start .. start + Packer::BLOCK_LEN])
                })
                .max().unwrap();
            // We don't support 0 bits per int since we can only fit 1..=16 (16 values) in a single nibble.
            let bits_per_int_nibble = cmp::max(bits_per_int_nibble, 1);

            assert!(bits_per_int_nibble <= 16, "unexpected larger than 16-bit integer in U16Array");

            for bb in bb_start..bb_end {
                // Compress a bitpacker block.
                let start = bb * Packer::BLOCK_LEN;
                let compressed_bytes =
                    bitpacker.compress(
                        &self.next_block[start .. start + Packer::BLOCK_LEN],
                        &mut compress_buf[..], bits_per_int_nibble);
                // Add the compressed block to block_data.
                self.block_data.extend_from_slice(&compress_buf[0..compressed_bytes]);
            }
            bits_per_int |= u16::from(bits_per_int_nibble - 1) << (4 * nibble_idx);
        } 

        // Add compressed block metadata
        self.block_meta.push( BitPackerBlock{
            start_offset,
            bits_per_int,
            base_value:   base_value.try_into().unwrap()
        }); 
    }

    /// Decompress block at specified intex into `decompressed_block` so we can access its values.
    #[cold]
    fn decompress_block(&mut self, block_index: usize) {
        let meta = self.block_meta[block_index];

        if Self::USE_BITPACKER4 {
            self.decompress_base::<BitPacker4x>(meta);
        } else {
            self.decompress_base::<BitPacker1x>(meta);
        }

        // Add minimum value to get the original values.
        for id in &mut self.decompressed_block {
            *id += u32::from(meta.base_value);
        }
        if self.is_id {
            Self::revert_shuffle_id_for_compressibility(&mut self.decompressed_block);
        }
        self.decompressed_block_index = block_index;
    }
}
impl U16Array for U16ArrayBitPacker {
    fn at(&mut self, index: usize) -> u16 {
        let block_index = index >> Self::BLOCK_SIZE_EXP;
        if block_index == self.decompressed_block_index {
            // Usually the index points to current decompressed block (assuming linear access).
            let in_block_index = Self::IN_BLOCK_INDEX_MASK & index;
            return self.decompressed_block[in_block_index].try_into().unwrap()
        }

        if block_index == self.block_meta.len() {
            // Access into not-yet-compressed `next_block`
            let in_block_index = Self::IN_BLOCK_INDEX_MASK & index;
            return self.next_block[in_block_index].try_into().unwrap()
        }

        // Access to a compressed block, decompress and read value from it.
        self.decompress_block(block_index);
        let in_block_index = Self::IN_BLOCK_INDEX_MASK & index;
        self.decompressed_block[in_block_index].try_into().unwrap()
    }

    fn push(&mut self, rhs: u16) {
        self.next_block[self.next_block_used] = u32::from(rhs);
        self.next_block_used += 1;

        if self.next_block_used < Self::BLOCK_SIZE {
            // In most cases we just add an item to `next_block`, which is not yet full, and return.
            return;
        }

        // BLOCK_SIZE new items - compress and reset `next_block`

        // Subtract minimum from all values in the block, to make their bit representations more
        // compact.
        if self.is_id {
            Self::shuffle_id_for_compressibility(&mut self.next_block);
        }
        let base_value: u32 = *self.next_block.iter().min().unwrap();
        for id in &mut self.next_block {
            *id -= base_value;
        }

        if Self::USE_BITPACKER4 {
            self.compress_base::<BitPacker4x>(base_value);
        } else {
            self.compress_base::<BitPacker1x>(base_value);
        }

        // Reset next_block
        self.next_block_used = 0;
    }

    fn len(&self) -> usize {
        (self.block_meta.len() << Self::BLOCK_SIZE_EXP) + self.next_block_used
    }

    fn add_stats(&self, stats: &mut U16ArrayStats) {
        stats.memory_total_reserved   += self.block_data.capacity() * mem::size_of::<u8>() +
                                         self.block_meta.capacity() * mem::size_of::<BitPackerBlock>();
        stats.memory_total_used       += self.block_data.len() * mem::size_of::<u8>() +
                                         self.block_meta.len() * mem::size_of::<BitPackerBlock>();
        stats.memory_blocks           += self.block_meta.len() * mem::size_of::<BitPackerBlock>();
        stats.memory_tables           += 0;
        stats.memory_data             += self.block_data.len() * mem::size_of::<u8>();
        stats.stored_items_total      += self.len();
        stats.tables_entries_max      += 0;
        stats.tables_entries_used_max += 0;
        stats.blocks_deduped          += 0;
        stats.blocks_total            += self.block_meta.len();
    }
}


/// Info about a possibly deduplicated block in `U16ArrayDedupHash`.
#[derive(Clone, Copy)]
struct U16HashBlock {
    /// 31-bit index (into `table` or directly into `data`) and (top bit) a flag true if the index
    /// points into `table`, false if it points into `data`.
    bits: u32
}
impl U16HashBlock {
    const fn new(index: u32, is_in_table: bool) -> Self {
        Self {
            bits: index | ((if is_in_table {1} else {0} ) << 31)
        }
    }

    /// If true, index points to the table (which contains index to data), otherwise it points right into data.
    const fn is_in_table(self) -> bool {
        (self.bits >> 31) == 1
    }

    /// Stored index (`is_in_table` determines what it points into).
    const fn index(self) -> u32 {
        self.bits & 0x7fff_ffff
    }
}

/// Stores u16s in blocks with a deduplication hashtable
///
/// All values are stored in `data`, but whenever we finish a block we check if we already have 
/// an identical block in the table, and if so, we just add a block pointing to that table item
/// and don't store a new blockful of data.
#[derive(Clone)]
pub struct U16ArrayDedupHash {
    /// One per BLOCK_SIZE item, each index stores index to either data, or to a table entry
    /// storing index to data.
    blocks: Vec<U16HashBlock>,
    /// Each entry stores index to data to the first item of a full block with that entry's hash.
    table: Vec<Option<u32>>,
    /// Partially deduplicated array of uint16 values.
    data: Vec<u16>,

    /// Newly added integers are added to (and accessed from) this array.
    ///
    /// This effectively stores the last modulo `next_block_used` items of the array.
    next_block: [u16; Self::BLOCK_SIZE],
    /// Number of integers in 'next_block'
    next_block_used: usize
}

impl U16ArrayDedupHash {
    /// log2 of the block size.
    const BLOCK_SIZE_EXP:      usize = 6;
    /// Mask used to get the in-block part of an index.
    const IN_BLOCK_INDEX_MASK: usize = (1 << Self::BLOCK_SIZE_EXP) - 1;
    /// Size of one block.
    const BLOCK_SIZE:          usize = 1 << Self::BLOCK_SIZE_EXP;

    /// log2 of the table size (max number of unique deduplicated blocks).
    const TABLE_SIZE_EXP:      usize = 14;
    /// Number of table entries.
    const TABLE_SIZE:          usize = 1 << Self::TABLE_SIZE_EXP;
    /// Mask used to cut off bottom bits of a hash to create a table index.
    const TABLE_INDEX_MASK:    u64   = (1 << (Self::TABLE_SIZE_EXP as u64)) - 1;

    #[allow(dead_code)]
    pub fn new(_is_id: bool) -> Self {
        Self {
            blocks:          Vec::with_capacity(512),
            table:           vec![],
            data:            Vec::with_capacity(512),
            next_block:      [0; Self::BLOCK_SIZE],
            next_block_used: 0
        }
    }
}
impl U16Array for U16ArrayDedupHash {
    fn at(&mut self, index: usize) -> u16 {
        let block_index    = index >> Self::BLOCK_SIZE_EXP;
        let in_block_index = Self::IN_BLOCK_INDEX_MASK & index;

        if block_index < self.blocks.len() {
            // Access into a block.
            let block = self.blocks[block_index];
            // Get index to the first block item, whether it's deduplicated or not.
            let start_index: u32 = if block.is_in_table() {
                self.table[block.index() as usize].unwrap()
            } else {
                block.index()
            };

            // Get a value from the block.
            self.data[start_index as usize + in_block_index]
        } else {
            // Access into yet unfinished `next_block`.
            self.next_block[in_block_index]
        }

    }

    // Truncation is intended here
    #[allow(clippy::cast_possible_truncation)]
    fn push(&mut self, rhs: u16) {
        self.next_block[self.next_block_used] = rhs;
        self.next_block_used += 1;

        if self.next_block_used < Self::BLOCK_SIZE {
            // Usually we just add an item to `next_block`, which is not yet full, and return.
            return;
        }

        // BLOCK_SIZE new items - try to find an identical block in the table
        if self.table.is_empty() {
            self.table = vec![None; Self::TABLE_SIZE];
        }

        // Hash the next block.
        let mut hasher = DefaultHasher::new();
        for i in self.next_block {
            hasher.write_u16(i);
        }
        let hash_full  = hasher.finish();

        // Use the lowest bits of the hash as the table index.
        let hash_index = (hash_full & Self::TABLE_INDEX_MASK) as u32;

        if let Some(index) = self.table[hash_index as usize] {
            // There is already a block in the table with the same hash.
            if self.data[index as usize .. index as usize + Self::BLOCK_SIZE] == self.next_block[..] {
                // Same block, so deduplicate - point to the same index in the hash table.
                self.blocks.push(U16HashBlock::new(hash_index, true));
            } else {
                // Different block, add a full non-compressed block.
                let data_index = self.data.len();
                self.data.extend_from_slice(&self.next_block[..]);
                self.blocks.push(U16HashBlock::new(u32::try_from(data_index).unwrap(), false));
            }
        } else {
            // Free table slot - add this block there
            self.table[hash_index as usize] = Some(self.data.len().try_into().unwrap());
            self.data.extend_from_slice(&self.next_block[..]);
            self.blocks.push(U16HashBlock::new(hash_index, true));
        }
        assert!(self.data.len() < (u32::MAX as usize / 2), 
            "Reached max supported track length for dedup-hash-table-compressed storage");

        // reset next_block
        self.next_block_used = 0;
    }

    fn len(&self) -> usize {
        (self.blocks.len() << Self::BLOCK_SIZE_EXP) + self.next_block_used
    }

    fn add_stats(&self, stats: &mut U16ArrayStats) {
        stats.memory_total_reserved   += self.data.capacity() * mem::size_of::<u16>() +
                                         self.table.capacity() * mem::size_of::<Option<u32>>() +
                                         self.blocks.capacity() * mem::size_of::<U16HashBlock>();

        stats.memory_total_used       += self.data.len() * mem::size_of::<u16>() +
                                         self.table.len() * mem::size_of::<Option<u32>>() +
                                         self.blocks.len() * mem::size_of::<U16HashBlock>();
        stats.memory_blocks           += self.blocks.len() * mem::size_of::<U16HashBlock>();
        stats.memory_tables           += self.table.len() * mem::size_of::<Option<u32>>();
        stats.memory_data             += self.data.len() * mem::size_of::<u16>();
        stats.stored_items_total      += self.len();
        stats.tables_entries_max      =  cmp::max(stats.tables_entries_max, self.table.len());
        let entries_used = self.table.iter().filter(|&e| e.is_some()).count();

        stats.tables_entries_used_max =  cmp::max(stats.tables_entries_used_max, entries_used);
        stats.blocks_deduped          += self.blocks.iter().filter(|b| b.is_in_table()).count();
        stats.blocks_total            += self.blocks.len();
    }
}


/// Block of `U16ArrayLUTBlock`.
///
/// Blocks are packed tightly in `block_data`.
///
/// Each block consists of a look-up table of `lut_size` uncompressed `u16` values followed by
/// `BLOCK_SIZE/BitPacker{1x,4x}::BLOCK_LEN` BitPacker{1x,4x} blocks of compressed indices into the LUT.
///
/// If the block is incompressible, `lut_size` is 0 and the block only consists of `BLOCK_SIZE`
/// uncompressed `u16` values.
#[derive(Copy, Clone)]
struct LUTArrayBlock {
    start_offset_main: u32, // start offset in `block_data`
    start_offset_top:   u8, // top 8 bits of start_offset to get a 40-bit value
    lut_size:           u8, // number of values in the LUT for this block.
}
impl LUTArrayBlock {
    fn new(start_offset: usize, lut_size: u8) -> Self {
        Self {
            start_offset_main: u32::try_from(start_offset & 0xFFFF_FFFF).unwrap(),
            start_offset_top:  u8::try_from(start_offset >> 32).unwrap(),
            lut_size
        }
    }

    /// Get offset of the first byte of this block in `block_data`.
    const fn start_offset(self) -> usize {
        (self.start_offset_main as usize) | ((self.start_offset_top as usize) << 32)
    }

    /// If true, the block is compressed and starts with a look-up table.
    const fn use_lut(self) -> bool {
        self.lut_size != 0
    }

    /// Bits per int of the BitPacker-compressed indices.
    const fn bits_per_int(self) -> u8 {
        Self::bits_per_int_lut_size(self.lut_size)
    }

    /// Calculate bits-per-int needed for indices into a LUT of specified size.
    const fn bits_per_int_lut_size(lut_size: u8) -> u8 {
        // Ceil of integer log2. This is surprisingly faster than computational approach:
        // (u8::BITS - lut_size.next_power_of_two().leading_zeros() - 1) as u8
        match lut_size {
            0           => { panic!("should not be reached"); }
            1           => 0,
            2           => 1,
            3   ..=  4  => 2,
            5   ..=  8  => 3,
            9   ..= 16  => 4,
            17  ..= 32  => 5,
            33  ..= 64  => 6,
            65  ..= 128 => 7,
            129 ..= 255 => 8
        }
    }

    /// Get max LUT size that will actually compressed a block based on block size of `U16ArrayLUTBlock`.
    const fn max_useful_lut_size() -> u8 {
        match U16ArrayLUTBlock::BLOCK_SIZE_EXP {
            5 => 21,
            6 => 39,
            7 => 71,
            8 => 128,
            9 => 255,
            _ => panic!("lut size for BLOCK_SIZE_EXP not precomputed, add it!")
        }

        // algorithm used to pre-compute these values:
        //
        // let uncompressed_size = block_size * mem::size_of::<u16>();
        // for lut_size in (1..block_size).rev() {
        //     let lut_bytes  = lut_size * mem::size_of::<u16>();
        //     let bits_per_int = bits_per_int_lut_size(lut_size);
        //     let data_bytes = bits_per_int * block_size / 8;
        //     let total_bytes = data_bytes + lut_bytes;
        //     if total_bytes < uncompressed_size {
        //         return lut_size;
        //     }
        // }
        // 0
    }
}

#[derive(Clone)]
pub struct U16ArrayLUTBlock {
    /// Metadata 'block' structures storing info about raw blocks in `block_data`.
    block_meta: Vec<LUTArrayBlock>,
    /// Tightly packed blocks, consisting of uncompressed LUTS followed by compressed indices
    ///
    /// Incompressible blocks are stored as direct uncompressed values with no LUT.
    block_data: Vec<u8>,

    /// Last decompressed block is always decompressed into this array
    ///
    /// This allow us to avoid decompressing on each accessed integer as long as access is somewhat
    /// non-random.
    /// 32bit values used for BitPacker compatibility and speed.
    decompressed_block: [u32; Self::BLOCK_SIZE],
    /// Index of the decompressed block.
    decompressed_block_index: usize,

    /// Newly added integers are added to (and accessed from) this array.
    ///
    /// This effectively stores the last modulo `BLOCK_SIZE` items of the array.
    ///
    /// When it gets filled, it's compressed by deduplicating into a LUT and storing the
    /// block's values as indices into the LUT, compressed using BitPacker. This (both LUT and 
    /// compressed indices) is appended to `block_data`, and `next_block` is reset.
    next_block:      [u16; Self::BLOCK_SIZE],
    /// Number of integers in 'next_block'
    next_block_used: usize,
}
impl U16ArrayLUTBlock {
    /// log2 of the block size. `BitPacker1x` works with 32-int blocks so this must be at least 5.
    /// Must also be at most 9.
    const BLOCK_SIZE_EXP:      usize = 8;
    /// Mask used to get the in-block part of an index.
    const IN_BLOCK_INDEX_MASK: usize = (1 << Self::BLOCK_SIZE_EXP) - 1;
    /// Size of one block.
    const BLOCK_SIZE:          usize = 1 << Self::BLOCK_SIZE_EXP;
    /// Should we compress indices with `BitPacker4x` instead of `BitPacker1x`?
    const USE_BITPACKER4:      bool  = Self::BLOCK_SIZE >= 128;

    #[allow(dead_code)]
    pub fn new(_is_id: bool) -> Self {
        Self {
            block_meta:               Vec::with_capacity(512),
            block_data:               Vec::with_capacity(512),
            decompressed_block:       [0; Self::BLOCK_SIZE],
            decompressed_block_index: usize::MAX,
            next_block:               [0; Self::BLOCK_SIZE],
            next_block_used:          0,
        }
    }

    /// Decompress indices from `block_data` starting at `start_offset_data` with
    /// `bits_per_int` per index.
    fn decompress_indices<Packer: BitPacker>(&mut self, start_offset_data: usize, bits_per_int: u8) {
        // Should be a constant but those don't work in generics when set from Self/Packer members.
        let subblocks: usize = Self::BLOCK_SIZE / Packer::BLOCK_LEN;
        let compressed_bitpacker_block_size = (bits_per_int as usize) * Packer::BLOCK_LEN / 8;
        let bitpacker = Packer::new();
        for bb in 0..subblocks {
            let start_offset_dst = bb * Packer::BLOCK_LEN;
            let start_offset_src = start_offset_data + bb * compressed_bitpacker_block_size;
            bitpacker.decompress(
                // Compressed bitpacker block.
                &self.block_data[start_offset_src .. start_offset_src + compressed_bitpacker_block_size],
                // Part of the decompressed array block corresponding to the bitpacker block.
                &mut self.decompressed_block[start_offset_dst .. start_offset_dst + Packer::BLOCK_LEN],
                bits_per_int );
        }
    }

    /// Compress indices from `indices` with `bits_per_int` and add them to `self.block_data`.
    fn compress_subblocks<Packer: BitPacker>(&mut self, indices: &[u32], bits_per_int: u8) {
        // This is basically zero-overhead
        let bitpacker = BitPacker4x::new();
        // 128 works for both 1x and 4x; constants from surrounding class or Packer don't
        // (yet?) work with constants/arrays in generics.
        let mut compress_buf = [0u8; 2 * 128]; // At most 2 bytes per integer.
        let subblocks: usize = Self::BLOCK_SIZE / Packer::BLOCK_LEN;

        for bb in 0..subblocks {
            let start = bb * Packer::BLOCK_LEN;
            let compressed_bytes =
                bitpacker.compress(
                    &indices[start .. start + Packer::BLOCK_LEN],
                    &mut compress_buf[..], 
                    bits_per_int);
            // Add compressed index data to block_data.
            self.block_data.extend_from_slice(&compress_buf[0..compressed_bytes]);
        }
    }

    /// Decompress block at specified intex into `decompressed_block` so we can access its values.
    #[cold]
    fn decompress_block(&mut self, block_index: usize) {
        let meta = self.block_meta[block_index];

        if meta.use_lut() {
            // LUT-compressed block
            let start_offset_lut = meta.start_offset();
            let lut_bytes        = (meta.lut_size as usize) * mem::size_of::<u16>();
            let lut = {
                let mut lut: [u16; 256] = [0; 256];
                NativeEndian::read_u16_into(
                    &self.block_data[start_offset_lut..start_offset_lut + lut_bytes],
                    &mut lut[0 .. meta.lut_size as usize]);
                lut
            };

            let start_offset_data = start_offset_lut + lut_bytes;
            let bits_per_int      = meta.bits_per_int();
            if Self::USE_BITPACKER4 {
                self.decompress_indices::<BitPacker4x>(start_offset_data, bits_per_int);
            } else {
                self.decompress_indices::<BitPacker1x>(start_offset_data, bits_per_int);
            }

            // Use LUT to restore original values in the block.
            for val in &mut self.decompressed_block[..] {
                *val = u32::from(lut[*val as usize]);
            }
        } else {
            let u16_block = {
                let mut u16_block = [0; Self::BLOCK_SIZE];
                let start_offset = meta.start_offset();
                NativeEndian::read_u16_into(
                    &self.block_data[start_offset..start_offset + Self::BLOCK_SIZE * mem::size_of::<u16>()],
                    &mut u16_block[..]);
                u16_block
            };
            // Copy into decompressed_block, converting to u32 (used to make bitpacker branch faster).
            for (i, val) in u16_block.iter().enumerate() {
                self.decompressed_block[i] = u32::from(*val);
            }
        }

        self.decompressed_block_index = block_index;
    }
}
impl U16Array for U16ArrayLUTBlock {
    fn at(&mut self, index: usize) -> u16 {
        let block_index = index >> Self::BLOCK_SIZE_EXP;
        if block_index == self.decompressed_block_index {
            // Usually the index points to current decompressed block (assuming linear access).
            let in_block_index = Self::IN_BLOCK_INDEX_MASK & index;
            return u16::try_from(self.decompressed_block[in_block_index]).unwrap();
        }

        if block_index == self.block_meta.len() {
            // Access into not-yet-compressed `next_block`
            let in_block_index = Self::IN_BLOCK_INDEX_MASK & index;
            return self.next_block[in_block_index];
        }

        // Access to a compressed block, decompress and read value from it.
        self.decompress_block(block_index);
        let in_block_index = Self::IN_BLOCK_INDEX_MASK & index;
        u16::try_from(self.decompressed_block[in_block_index]).unwrap()
    }

    fn push(&mut self, rhs: u16) {
        self.next_block[self.next_block_used] = rhs;
        self.next_block_used += 1;

        if self.next_block_used < Self::BLOCK_SIZE {
            // In most cases we just add an item to `next_block`, which is not yet full, and return.
            return;
        }

        // Reset next block.
        self.next_block_used = 0;

        let mut lut: [u16; 256] = [0; 256];
        // Will never go over u8::MAX (255) as long as BLOCK_SIZE_EXP is at most 9.
        // (see `max_useful_lut_size`)
        let mut lut_used: u8 = 0;

        // Next_block translated to indices into `lut`.
        let mut next_block_lut: [u32; Self::BLOCK_SIZE] = [0; Self::BLOCK_SIZE];
        if !(5..=9).contains(&Self::BLOCK_SIZE_EXP) {
            unreachable!("unsupported block size for U16ArrayLUTBlock");
        }
        assert!(self.block_data.len() < (1 << 40), 
            "reached max supported track length for lut-block-compressed storage");

        for (i, val) in self.next_block.iter().enumerate() {
            if let Some(lut_index) = lut[0..lut_used as usize].iter().position(|&v| v == *val as u16) {
                // Value already in LUT, use its index.
                next_block_lut[i] = u32::try_from(lut_index).unwrap();
                continue;
            }
            // Value not in LUT.
            if lut_used < LUTArrayBlock::max_useful_lut_size() {
                // Add value to LUT.
                lut[lut_used as usize] = *val;
                next_block_lut[i]      = u32::from(lut_used);
                lut_used += 1;
                continue;
            }
            let start_offset = self.block_data.len();
            // LUT is no longer advantageous at this size, so don't use it.
            self.block_meta.push(LUTArrayBlock::new(start_offset, 0));
            // Convert next_block to u16 and write that directly into block_data.
            let mut u16_block = [0; Self::BLOCK_SIZE];
            for (i, val) in self.next_block.iter().enumerate() {
                u16_block[i] = *val;
            }
            self.block_data.resize(self.block_data.len() + Self::BLOCK_SIZE * mem::size_of::<u16>(), 0);
            NativeEndian::write_u16_into(&u16_block[..], &mut self.block_data[start_offset..]);
            return;
        }

        // LUT built, add a block with it and the indices.
        let start_offset = self.block_data.len();
        self.block_meta.push(LUTArrayBlock::new(start_offset, lut_used));

        // Copy the LUT to block_Data.
        self.block_data.resize(self.block_data.len() + lut_used as usize * mem::size_of::<u16>(), 0);
        NativeEndian::write_u16_into(&lut[0 .. lut_used as usize], &mut self.block_data[start_offset..]);

        // Compress the indices and add them to block_data.
        let bits_per_int = LUTArrayBlock::bits_per_int_lut_size(lut_used);
        if Self::USE_BITPACKER4 {
            self.compress_subblocks::<BitPacker4x>(&next_block_lut[..], bits_per_int);
        } else {
            self.compress_subblocks::<BitPacker1x>(&next_block_lut[..], bits_per_int);
        }
    }

    fn len(&self) -> usize {
        (self.block_meta.len() << Self::BLOCK_SIZE_EXP) + self.next_block_used
    }

    fn add_stats(&self, stats: &mut U16ArrayStats) {
        stats.memory_total_reserved   += self.block_data.capacity() * mem::size_of::<u8>() +
                                         self.block_meta.capacity() * mem::size_of::<LUTArrayBlock>();
        stats.memory_total_used       += self.block_data.len() * mem::size_of::<u8>() +
                                         self.block_meta.len() * mem::size_of::<LUTArrayBlock>();
        stats.memory_blocks           += self.block_meta.len() * mem::size_of::<LUTArrayBlock>();
        stats.memory_tables           += 0;
        stats.memory_data             += self.block_data.len() * mem::size_of::<u8>();
        stats.stored_items_total      += self.len();
        stats.tables_entries_max      += 0;
        stats.tables_entries_used_max += 0;
        stats.blocks_deduped          += 0;
        stats.blocks_total            += self.block_meta.len();
    } 
}


/// Currently used timepoint ID array implementattion.
// pub type TimepointIDs = U16ArrayVec;
// pub type TimepointIDs = U16ArrayBitPacker;
// pub type TimepointIDs = U16ArrayDedupHash;
pub type TimepointIDs = U16ArrayLUTBlock;
/// Currently used timepoint delta array implementattion.
// pub type TimepointDeltas = U16ArrayVec;
// pub type TimepointDeltas = U16ArrayBitPacker;
// pub type TimepointDeltas = U16ArrayDedupHash;
pub type TimepointDeltas = U16ArrayLUTBlock;

#[allow(dead_code)]
fn assert_equal(lhs: &mut impl U16Array, rhs: &mut impl U16Array, size: usize)
{
    let mut diff = String::new();
    for i in 0 .. size {
        let a = lhs.at(i);
        let b = rhs.at(i);
        diff += format!("{}: {} vs {}\n", i, a, b).as_str();
        assert!( a == b, "difference at index {} (size {}):\n {}", i, size, diff);
    }
}
